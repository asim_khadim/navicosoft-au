import React from 'react';
import { Switch } from 'react-router-dom';

// Route Setting
import HomePage from "../HomePage";
import AboutUs from "../AboutUs";
import Services from "../Services";
import TeamPage from "../TeamPage";
import Portfolio from "../Portfolio";
import Gallery from "../Gallery";
import PortfolioSingle from "../PortfolioSingle";
import BlogLists from "../BlogLists";
import BlogSingle from "../BlogSingle";
import ContactUsPage from "../ContactUsPage";
import DigitalMarketingPage from "../DigitalMarketingPage";
import DomainRegistrationPage from "../DomainRegistrationPage";
import SocialMediaMarketingPage from "../SocialMediaMarketingPage";
import WebHostingPage from "../WebHostingPage";
import WebDevelopmentPage from "../WebDevelopmentPage";
import WebDesignPage from "../WebDesignPage";
import BackorderDomainPage from "../BackorderDomainPage";
import BusinessStationaryPage from "../BusinessStationaryPage";
import DedicatedHostingPage from "../DedicatedHostingPage";
import LinuxHostingPage from "../LinuxHostingPage";
import LinuxServerPage from "../LinuxServerPage";
import LogoDesignPage from "../LogoDesignPage";
import WebsiteAnalyticsPage from "../WebsiteAnalyticsPage";
import WindowsServerPage from "../WindowsServerPage";
import Googleadpage from "../Googleadpage";
import AboutUsPage from "../AboutUsPage";
import VpsHostingPage from "../VpsHostingPage";
import FacebookPage from "../FacebookPage";
import CloudBackUpPage from "../CloudBackUpPage";
import BusinessEmailSolutionPage from "../BusinessEmailSolutionPage";
import WordpressHostingPage from "../WordpressHostingPage";

// Normal Route
import PrivateRoute from "../_PrivateRoute";
import PublicRoute from "../_PublicRoute";

function componentShowRender(copm) {
    switch (copm) {
        case 'HomePage':
            return HomePage;
        case 'AboutUs':
            //     return AboutUs;
            // case 'Services':
            return Services;
        case 'TeamPage':
            return TeamPage;
        case 'Portfolio':
            return Portfolio;
        case 'Gallery':
            return Gallery;
        case 'BlogLists':
            return BlogLists;
        case 'ContactUsPage':
            return ContactUsPage;
        case 'DigitalMarketingPage':
            return DigitalMarketingPage;
        case 'DomainRegistrationPage':
            return DomainRegistrationPage;
        case 'SocialMediaMarketingPage':
            return SocialMediaMarketingPage;
        case 'WebHostingPage':
            return WebHostingPage;
        case 'WebDevelopmentPage':
            return WebDevelopmentPage;
        case 'WebDesignPage':
            return WebDesignPage;
        case 'BackorderDomainPage':
            return BackorderDomainPage;
        case 'BusinessStationaryPage':
            return BusinessStationaryPage;
        case 'DedicatedHostingPage':
            return DedicatedHostingPage;
        case 'LinuxHostingPage':
            return LinuxHostingPage;
        case 'LinuxServerPage':
            return LinuxServerPage;
        case 'LogoDesignPage':
            return LogoDesignPage;
        case 'WebsiteAnalyticsPage':
            return WebsiteAnalyticsPage;
        case 'WindowsServerPage':
            return WindowsServerPage;
        case 'Googleadpage':
            return Googleadpage;
        case 'VpsHostingPage':
            return VpsHostingPage;
        case 'FacebookPage':
            return FacebookPage;
        case 'CloudBackUpPage':
            return CloudBackUpPage;
        case 'BusinessEmailSolutionPage':
            return BusinessEmailSolutionPage;
        case 'WordpressHostingPage':
            return WordpressHostingPage;
        default:
            return HomePage;
    }
}

export default function Routes(props) {

    return (
        <Switch>
            <PrivateRoute
                fixHeader={true}
                exact
                path="/"
                component={HomePage}
            />

            {props.menus !== undefined ? props.menus.map((menu, i) => {
                return (
                    <PrivateRoute
                        key={i}
                        fixHeader={menu.component === 'HomePage' ? true : ''}
                        exact
                        path={`/${menu.slug}`}
                        component={componentShowRender(menu.component)}
                    />
                )
            }) : ''}

            <PrivateRoute
                path="/portfolio-single/:id"
                component={PortfolioSingle}
            />
            <PrivateRoute
                exact
                path="/blog/:id"
                component={BlogLists}
            />
            <PrivateRoute
                path="/blog-details/:id"
                component={BlogSingle}
            />
            <PrivateRoute
                path="/contact-us"
                component={ContactUsPage}
            />
            <PrivateRoute
                path="/digital-marketing"
                component={DigitalMarketingPage}
            />
            <PrivateRoute
                path="/domain-registration"
                component={DomainRegistrationPage}
            />
            <PrivateRoute
                path="/social-media-marketing"
                component={SocialMediaMarketingPage}
            />
            <PrivateRoute
                path="/web-hosting"
                component={WebHostingPage}
            />
            <PrivateRoute
                path="/web-development"
                component={WebDevelopmentPage}
            />
            <PrivateRoute
                path="/web-design"
                component={WebDesignPage}
            />
            <PrivateRoute
                path="/backorder-domain"
                component={BackorderDomainPage}
            />
            <PrivateRoute
                path="/business-stationery"
                component={BusinessStationaryPage}
            />
            <PrivateRoute
                path="/dedicated-hosting"
                component={DedicatedHostingPage}
            />
            <PrivateRoute
                path="/linux-hosting"
                component={LinuxHostingPage}
            />
            <PrivateRoute
                path="/linux-server"
                component={LinuxServerPage}
            />
            <PrivateRoute
                path="/logo-design"
                component={LogoDesignPage}
            />
            <PrivateRoute
                path="/website-analytics"
                component={WebsiteAnalyticsPage}
            />
            <PrivateRoute
                path="/windows-server"
                component={WindowsServerPage}
            />
            <PrivateRoute
                path="/google-ads"
                component={Googleadpage}
            />
            <PrivateRoute
                path="/about-us"
                component={AboutUsPage}
            />
            <PrivateRoute
                path="/vps-hosting"
                component={VpsHostingPage}
            />
            <PrivateRoute
                path="/facebook-ads"
                component={FacebookPage}
            />
            <PrivateRoute
                path="/cloud-backup"
                component={CloudBackUpPage}
            />
            <PrivateRoute
                path="/business-email-solution"
                component={BusinessEmailSolutionPage}
            />
            <PrivateRoute
                path="/wordpress-hosting"
                component={WordpressHostingPage}
            />

            <PrivateRoute
                exact
                fixHeader={true}
                component={HomePage}
            />
        </Switch>

    );
}