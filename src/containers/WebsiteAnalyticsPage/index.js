import React, { Fragment, useEffect } from 'react';
import WebsiteAnalytics from "../../components/WebsiteAnalytics";
import WebsiteAnalyticsFAQ from "../../components/WebsiteAnalyticsFAQ";
import Footer from "../../components/Footer";
import PortfolioSection from '../../components/PortfolioSection';
import { withTranslation } from 'react-i18next';
import './style.scss';
import { Helmet } from "react-helmet";
const WebsiteAnalyticsPage = (props) => {
    useEffect(() => {
        window.scrollTo(0, 0)
    }, [])

    return (
        <Fragment>
            <Helmet>
                <meta charSet="utf-8" />
                <title>Website analytics, google website analytics agency in London </title>
                <meta name="description" content="Let us integrate website analytics to your website to help you track your client's behaviour and turn it into your revenue growth. " />
            </Helmet>
            <WebsiteAnalytics />
            <WebsiteAnalyticsFAQ />
            <PortfolioSection />
            <Footer />
        </Fragment>
    )
};

export default withTranslation('common')(WebsiteAnalyticsPage);