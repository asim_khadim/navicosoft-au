import React, { Fragment, useEffect } from 'react';
import WebHosting from "../../components/WebHosting";
import WebHostingFAQ from "../../components/WebHostingFAQ";
import Footer from "../../components/Footer";
import PortfolioSection from '../../components/PortfolioSection';
import { withTranslation } from 'react-i18next';
import './style.scss';
import { Helmet } from "react-helmet";
const WebHostingPage = (props) => {
    useEffect(() => {
        window.scrollTo(0, 0)
    }, [])

    return (
        <Fragment>
            <Helmet>
                <meta charSet="utf-8" />
                <title>Web hosting, cheap web hosting and domain hosting in London. </title>
                <meta name="description" content="Navicosoft is the only cheap web hosting provider in London, renowned for the most secure web hosting and domain hosting to keep your website running. " />
            </Helmet>
            <WebHosting />
            <WebHostingFAQ />
            <PortfolioSection />
            <Footer />
        </Fragment>
    )
};

export default withTranslation('common')(WebHostingPage);