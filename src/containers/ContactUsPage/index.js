import React, { Fragment, useEffect } from 'react';
import NewsLetter from "../../components/NewsLetter";
import Footer from "../../components/Footer";
import ContactInfoList from "../../components/ContactInfoList";
import PortfolioSection from '../../components/PortfolioSection';
import ContactUs from "../../components/ContactUs";
import BreadCrumb from "../../components/BreadCrumb";
import { Helmet } from "react-helmet";
import { withTranslation } from 'react-i18next';

import './style.scss';

const ContactUsPage = (props) => {
    useEffect(() => {
        window.scrollTo(0, 0)
    }, [])
    return (
        <Fragment>
            {/* <BreadCrumb
                pagename={ props.t('contact.pagename') }
                title={ props.t('contact.banner_title') }
            /> */}
            {/* <ContactInfoList /> */}
            <Helmet>
                <meta charSet="utf-8" />
                <title>NavicoSoft, powered by the digital age with hosting so desirable</title>
                <meta name="description" content="NavicoSoft, powered by digital age has made point of contact easily accessible. Click & know convenient way to get hosting so desirable" />
            </Helmet>
            <ContactUs />
            {/* <NewsLetter
                className="pt-0"
            /> */}
            <PortfolioSection />
            <Footer />
        </Fragment>
    )
};

export default withTranslation('common')(ContactUsPage);