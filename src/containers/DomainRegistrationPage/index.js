import React, { Fragment, useEffect } from 'react';
import DomainRegistration from '../../components/DomainRegistration';
import DomainRegistrationFAQ from '../../components/DomainRegistrationFAQ';
import Footer from "../../components/Footer";
import PortfolioSection from '../../components/PortfolioSection';
import { withTranslation } from 'react-i18next';
import { Helmet } from "react-helmet";
import './style.scss';

const DomainRegistrationPage = (props) => {
    useEffect(() => {
        window.scrollTo(0, 0)
    }, [])

    return (
        <Fragment>
            <Helmet>
                <meta charSet="utf-8" />
                <title>Domain registration, best domain name search in London </title>
                <meta name="description" content="Looking for the best domain name for your business? Check our domain name search list to register the perfect domain for your website in London. " />

            </Helmet>
            <DomainRegistration />
            <DomainRegistrationFAQ />
            <PortfolioSection />
            <Footer />
        </Fragment>
    )
};

export default withTranslation('common')(DomainRegistrationPage);