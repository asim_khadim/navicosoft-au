import React, { Fragment, useEffect } from 'react';
import BusinessEmailSolution from "../../components/BusinessEmailSolution";
import BusinessEmailSolutionFAQ from "../../components/BusinessEmailSolutionFAQ";
import Footer from "../../components/Footer";
import PortfolioSection from '../../components/PortfolioSection';
import { withTranslation } from 'react-i18next';
import { Helmet } from "react-helmet";
const BusinessEmailSolutionPage = (props) => {
    useEffect(() => {
        window.scrollTo(0, 0)
    }, []);
    return (
        <Fragment>
            <Helmet>
                <meta charSet="utf-8" />
                <title>Business email, free professional google business email in London </title>
                <meta name="description" content="Looking for a professional business email for your company? Why not try our free professional google business email to represent your business in London." />
            </Helmet>
            <BusinessEmailSolution />
            <BusinessEmailSolutionFAQ />
            <PortfolioSection />
            <Footer />
        </Fragment>
    )
};

export default withTranslation('common')(BusinessEmailSolutionPage);