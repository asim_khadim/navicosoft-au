import React, { Fragment, useEffect } from 'react';
import WordpressHosting from "../../components/WordpressHosting";
import WordpressHostingFAQ from "../../components/WordpressHostingFAQ";
import Footer from "../../components/Footer";
import PortfolioSection from '../../components/PortfolioSection';
import { withTranslation } from 'react-i18next';
import { Helmet } from "react-helmet";
const WordpressHostingPage = (props) => {
    useEffect(() => {
        window.scrollTo(0, 0)
    }, []);
    return (
        <Fragment>
            <Helmet>
                <meta charSet="utf-8" />
                <title>WordPress hosting, Managed WordPress Hosting UK</title>
                <meta name="description" content="Never let your website down. We are guaranteeing you 99.99% website uptime with our fully managed WordPress hosting in the UK." />
            </Helmet>
            <WordpressHosting />
            <WordpressHostingFAQ />
            <PortfolioSection />
            <Footer />
        </Fragment>
    )
};

export default withTranslation('common')(WordpressHostingPage);