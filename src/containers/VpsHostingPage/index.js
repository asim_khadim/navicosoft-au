import React, { Fragment, useEffect } from 'react';
import VpsHosting from "../../components/VpsHosting";
import VpsHostingFAQ from "../../components/VpsHostingFAQ";
import Footer from "../../components/Footer";
import PortfolioSection from '../../components/PortfolioSection';
import { withTranslation } from 'react-i18next';
import { Helmet } from "react-helmet";
const VpsHostingPage = (props) => {
    useEffect(() => {
        window.scrollTo(0, 0)
    }, [])

    return (
        <Fragment>
            <Helmet>
                <meta charSet="utf-8" />
                <title>VPS hosting, cheap VPS hosting, and VPS server in London</title>
                <meta name="description" content="Looking for VPS hosting? Navicosoft brings cheap VPS hosting in London to secure your website and boost your business sales within days. " />
            </Helmet>
            <VpsHosting />
            <VpsHostingFAQ />
            <PortfolioSection />
            <Footer />
        </Fragment>
    )
};

export default withTranslation('common')(VpsHostingPage);
