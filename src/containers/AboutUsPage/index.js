import React, { Fragment, useEffect } from 'react';
import AboutUs from "../../components/AboutUs";
import AboutUsFAQ from "../../components/AboutUsFAQ";
import Footer from "../../components/Footer";
import PortfolioSection from '../../components/PortfolioSection';
import { withTranslation } from 'react-i18next';
import {Helmet} from "react-helmet";
const AboutUsPage = (props) => {
    useEffect(() => {
        window.scrollTo(0, 0)
    }, [])

    return (
        <Fragment>
            <Helmet>
                    <meta charSet="utf-8" />
                    <title>Cheap website hosting, elite Australian web design company-Navicosoft</title>
                    <meta name="description" content="Looking for modest & cheap website hosting? Sail on Navicosoft’s ship, elite Australian web design company & enjoy the digital heaven!"/>
                    <body className="aboutuspage" />

            </Helmet>
            <AboutUs />
            <AboutUsFAQ />
            <PortfolioSection />
            <Footer />
        </Fragment>
    )
};

export default withTranslation('common')(AboutUsPage);