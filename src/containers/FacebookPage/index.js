import React, { Fragment, useEffect } from 'react';
import Header from "../../components/header";
import Facebook from "../../components/Facebook";
import AboutUsFAQ from "../../components/AboutUsFAQ";
import Footer from "../../components/Footer";
import PortfolioSection from '../../components/PortfolioSection';
import { withTranslation } from 'react-i18next';
import { Helmet } from "react-helmet";
const FacebookPage = (props) => {
    useEffect(() => {
        window.scrollTo(0, 0)
    }, [])
    return (
        <Fragment>
            <Helmet>
                <meta charSet="utf-8" />
                <title>Facebook advertising, facebook ads agency in London </title>
                <meta name="description" content="Get your business acknowledged in the UK through Facebook advertising. As a Facebook ads agency, we will voice out your brand across the globe! " />
                <body className="aboutuspage" />

            </Helmet>
            <Facebook />
            {/* <AboutUs />
            <AboutUsFAQ /> */}
            <PortfolioSection />
            <Footer />
        </Fragment>
    )
};

export default withTranslation('common')(FacebookPage);