import React, { Fragment, useEffect } from 'react';
import LogoDesign from "../../components/LogoDesign";
import LogoDesignFAQ from "../../components/LogoDesignFAQ";
import Footer from "../../components/Footer";
import PortfolioSection from '../../components/PortfolioSection';
import { withTranslation } from 'react-i18next';
import './style.scss';
import { Helmet } from "react-helmet";
const LogoDesignPage = (props) => {
    useEffect(() => {
        window.scrollTo(0, 0)
    }, [])

    return (
        <Fragment>
            <Helmet>
                <meta charSet="utf-8" />
                <title>Logo design, business & brand logo design in UK</title>
                <meta name="description" content="Get a unique and innovative logo design in the UK. We will make your business, brand & company logo outshine with appealing visuals. " />
            </Helmet>
            <LogoDesign />
            <LogoDesignFAQ />
            <PortfolioSection />
            <Footer />
        </Fragment>
    )
};

export default withTranslation('common')(LogoDesignPage);
