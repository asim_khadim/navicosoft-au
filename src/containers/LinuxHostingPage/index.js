import React, { Fragment, useEffect } from 'react';
import LinuxHosting from "../../components/LinuxHosting";
import LinuxHostingFAQ from "../../components/LinuxHostingFAQ";
import Footer from "../../components/Footer";
import PortfolioSection from '../../components/PortfolioSection';
import { withTranslation } from 'react-i18next';
import './style.scss';
import { Helmet } from "react-helmet";
const LinuxHostingPage = (props) => {
    useEffect(() => {
        window.scrollTo(0, 0)
    }, []);

    return (
        <Fragment>
            <Helmet>
                <meta charSet="utf-8" />
                <title>Linux Hosting, best Linux shared hosting with cpanel in London</title>
                <meta name="description" content="Looking for the Linux Hosting? Navicosoft can facilitate you with the most secure yet affordable Linux shared server web hosting with cpanel." />
            </Helmet>
            <LinuxHosting />
            <LinuxHostingFAQ />
            <PortfolioSection />
            <Footer />
        </Fragment>
    )
};

export default withTranslation('common')(LinuxHostingPage);