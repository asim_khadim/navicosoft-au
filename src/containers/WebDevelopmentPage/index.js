import React, { Fragment, useEffect } from 'react';
import WebDevelopment from "../../components/WebDevelopment";
import WebDevelopmentFAQ from "../../components/WebDevelopmentFAQ";
import Footer from "../../components/Footer";
import PortfolioSection from '../../components/PortfolioSection';
import { withTranslation } from 'react-i18next';
import './style.scss';
import { Helmet } from "react-helmet";
const WebDevelopmentPage = (props) => {
    useEffect(() => {
        window.scrollTo(0, 0)
    }, []);

    return (
        <Fragment>
            <Helmet>
                <meta charSet="utf-8" />
                <title>Web development, web development company in London </title>
                <meta name="description" content="Aiming to succeed in the online world? Get a simple, creative and customised website with the most trusted web development company in London. " />
            </Helmet>
            <WebDevelopment />
            <WebDevelopmentFAQ />
            <PortfolioSection />
            <Footer />
        </Fragment>
    )
};

export default withTranslation('common')(WebDevelopmentPage);