import React, { Fragment, useEffect } from 'react';
import LinuxServer from "../../components/LinuxServer";
import LinuxServerFAQ from "../../components/LinuxServerFAQ";
import Footer from "../../components/Footer";
import PortfolioSection from '../../components/PortfolioSection';
import { withTranslation } from 'react-i18next';
import './style.scss';
import { Helmet } from "react-helmet";
const LinuxServerPage = (props) => {
    useEffect(() => {
        window.scrollTo(0, 0)
    }, [])

    return (
        <Fragment>
            <Helmet>
                <meta charSet="utf-8" />
                <title>Linux Server, Linux Ubuntu ssh & ftp web server OS in London </title>
                <meta name="description" content="Difficulty in making a choice? Let's get our best Linux server because Navicosoft offers Linux Ubuntu ssh & ftp web server OS to make you ace the digital world. " />
            </Helmet>
            <LinuxServer />
            <LinuxServerFAQ />
            <PortfolioSection />
            <Footer />
        </Fragment>
    )
};

export default withTranslation('common')(LinuxServerPage);
