import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import HeroArea from "../../components/HeroArea";
import Featured from "../../components/Featured";

import DigitalStudio from "../../components/DigitalStudio";
import Testimonial from "../../components/Testimonial";
import OurExpert from "../../components/OurExpert";
import PricingPlan from "../../components/PricingPlan";
import ContactUs from "../../components/ContactUs";
import Footer from "../../components/Footer";
import BlogSection from "../../components/BlogSection";
import { loadHomeAction } from "../../store/actions/action";
import Particles1 from '../../components/slider';
import StickyStyled from '../../components/SectionTwo';
import Table from '../../components/SectionThree';
import Portfolio from '../../components/SectionFour';
import PortfolioSection from '../../components/PortfolioSection';
import { Helmet } from "react-helmet";

class HomePage extends React.Component {

    componentDidMount() {
        // this.props.loadHomeAction();
        window.scrollTo(0, 0)
    }

    render() {
        return (

            <Fragment>
                <Helmet>
                    <meta charSet="utf-8" />
                    <title>Digital marketing, Web design, Web Hosting & Domain Services in London</title>
                    <meta name="description" content="We are a leading London based digital marketing agency that can generate leads for your business through web design, SEO, Campaigns and Social media services." />
                    <body className="homepage" />
                </Helmet>
                <Particles1 />
                <StickyStyled />
                <Table />
                <Portfolio />
                <PortfolioSection />
                <Footer />

            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        hero: state.home,
        // features: state.home.service_list,
    }
};

export default connect(mapStateToProps, { loadHomeAction })(HomePage);