import React, { Fragment, useEffect } from 'react';
import NewsLetter from "../../components/NewsLetter";
import BusinessStationary from "../../components/BusinessStationary";
import BusinessStationaryFAQ from "../../components/BusinessStationaryFAQ";
import Footer from "../../components/Footer";
import PortfolioSection from '../../components/PortfolioSection';
import { withTranslation } from 'react-i18next';
import './style.scss';
import { Helmet } from "react-helmet";
const BusinessStationaryPage = (props) => {
    useEffect(() => {
        window.scrollTo(0, 0)
    }, []);

    return (
        <Fragment>
            <Helmet>
                <meta charSet="utf-8" />
                <title>Business stationery, best business stationery design in the UK. </title>
                <meta name="description" content="Represent your business proficiently with our business stationery designs. We are experts in designing business cards, logos & letterheads." />
            </Helmet>
            <BusinessStationary />
            <BusinessStationaryFAQ />
            <PortfolioSection />
            <Footer />
        </Fragment>
    )
};

export default withTranslation('common')(BusinessStationaryPage);