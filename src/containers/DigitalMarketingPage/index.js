import React, { Fragment, useEffect } from 'react';
import NewsLetter from "../../components/NewsLetter";
import DigitalMarketing from "../../components/DigitalMarketting";
import ContactUsForm from "../../components/ContactUsForm";
import DigitalMarketingFAQ from "../../components/DigitalMarketingFAQ";
import Footer from "../../components/Footer";
import PortfolioSection from '../../components/PortfolioSection';
import { withTranslation } from 'react-i18next';
import './style.scss';
import { Helmet } from "react-helmet";
const ContactUsPage = (props) => {
    useEffect(() => {
        window.scrollTo(0, 0)
    }, [])
    return (
        <Fragment>
            <Helmet>
                <meta charSet="utf-8" />
                <title>Digital marketing agency, Digital marketing agency London </title>
                <meta name="description" content="Let's digitalise your business and make yourself known across the globe with the most advanced digital marketing agency in London. " />
            </Helmet>
            <DigitalMarketing />
            {/* <ContactUsForm page_name='Digital Marketing' /> */}
            <DigitalMarketingFAQ />
            <PortfolioSection />
            <Footer />
        </Fragment>
    )
};

export default withTranslation('common')(ContactUsPage);