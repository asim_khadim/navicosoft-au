import React, { Fragment, useEffect } from 'react';
import BackorderDomain from "../../components/BackorderDomain";
import BackorderDomainFAQ from "../../components/BackorderDomainFAQ";
import Footer from "../../components/Footer";
import PortfolioSection from '../../components/PortfolioSection';
import { withTranslation } from 'react-i18next';
import './style.scss';
import { Helmet } from "react-helmet";
const BackorderDomainPage = (props) => {
    useEffect(() => {
        window.scrollTo(0, 0)
    }, [])

    return (
        <Fragment>
            <Helmet>
                <meta charSet="utf-8" />
                <title>Domain backorder, best domain registration in London </title>
                <meta name="description" content="Worried about the domain for your business? Navicosoft has one of the most efficient domain registration and domain backorder system in London. " />
            </Helmet>
            <BackorderDomain />
            <BackorderDomainFAQ />
            <PortfolioSection />
            <Footer />
        </Fragment>
    )
};

export default withTranslation('common')(BackorderDomainPage);