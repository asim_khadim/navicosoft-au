import React, { Fragment, useEffect } from 'react';
import Header from "../../components/header";
import CloudBackUp from "../../components/CloudBackUp";
import CloudBackupFAQ from "../../components/CloudBackupFAQ";
import Footer from "../../components/Footer";
import PortfolioSection from '../../components/PortfolioSection';
import { withTranslation } from 'react-i18next';
import {Helmet} from "react-helmet";
const CloudBackUpPage = (props) => {
    useEffect(() => {
        window.scrollTo(0, 0)
    }, []);
    return (
        <Fragment>
            <Helmet>
                    <meta charSet="utf-8" />
                    <title>Cloud backup, free cloud storage & data backup in London</title>
                    <meta name="description" content="Protect your business data and files with our best cloud backup solution. We are offering a free cloud storage data backup solution in London."/>
                    <body className="aboutuspage" />

            </Helmet>
            <CloudBackUp />
            <CloudBackupFAQ />
            <PortfolioSection />
            <Footer />
        </Fragment>
    )
};

export default withTranslation('common')(CloudBackUpPage);