import React, { Fragment, useEffect } from 'react';
import DedicatedHosting from "../../components/DedicatedHosting";
import DedicatedHostingFAQ from "../../components/DedicatedHostingFAQ";
import Footer from "../../components/Footer";
import PortfolioSection from '../../components/PortfolioSection';
import { withTranslation } from 'react-i18next';
import './style.scss';
import { Helmet } from "react-helmet";
const DedicatedHostingPage = (props) => {
    useEffect(() => {
        window.scrollTo(0, 0)
    }, [])
    return (
        <Fragment>
            <Helmet>
                <meta charSet="utf-8" />
                <title>Dedicated server, cheap dedicated server hosting in London</title>
                <meta name="description" content="Navicosoft offers fast, secure and easy-to-use dedicated servers in London. Manage your website using our cheap dedicated server hosting. " />
            </Helmet>
            <DedicatedHosting />
            <DedicatedHostingFAQ />
            <PortfolioSection />
            <Footer />
        </Fragment>
    )
};

export default withTranslation('common')(DedicatedHostingPage);