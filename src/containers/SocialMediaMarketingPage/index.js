import React, { Fragment, useEffect } from 'react';
import SocialMediaMarketing from "../../components/SocialMediaMarketing";
import Footer from "../../components/Footer";
import PortfolioSection from '../../components/PortfolioSection';
import { withTranslation } from 'react-i18next';
import './style.scss';
import { Helmet } from "react-helmet";
const SocialMediaMarketingPage = (props) => {
    useEffect(() => {
        window.scrollTo(0, 0)
    }, []);

    return (
        <Fragment>
            <Helmet>
                <meta charSet="utf-8" />
                <title>Social media marketing, Social media marketing agency in London</title>
                <meta name="description" content="Worried about choosing the right platform for social media marketing? Navicosoft is here to create your business dominance on all social media channels." />
            </Helmet>
            <SocialMediaMarketing />
            <PortfolioSection />
            <Footer />
        </Fragment>
    )
};

export default withTranslation('common')(SocialMediaMarketingPage);