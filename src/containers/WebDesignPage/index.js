import React, { Fragment, useEffect } from 'react';
import WebDesign from "../../components/WebDesign";
import WebDesignFAQ from "../../components/WebDesignFAQ";
import Footer from "../../components/Footer";
import PortfolioSection from '../../components/PortfolioSection';
import { withTranslation } from 'react-i18next';
import './style.scss';
import { Helmet } from "react-helmet";
const WebDesignPage = (props) => {
    useEffect(() => {
        window.scrollTo(0, 0)
    }, []);
    return (
        <Fragment>
            <Helmet>
                <meta charSet="utf-8" />
                <title>Website Design, Web Design Company in London</title>
                <meta name="description" content="Discover Navicosoft, a top web design company in London that designs a highly responsive and appealing website for your brand. " />
            </Helmet>
            <WebDesign />
            <WebDesignFAQ />
            <PortfolioSection />
            <Footer />
        </Fragment>
    )
};

export default withTranslation('common')(WebDesignPage);