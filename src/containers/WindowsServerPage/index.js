import React, { Fragment, useEffect } from 'react';
import WindowsServer from "../../components/WindowsServer";
import WindowsServerFAQ from "../../components/WindowsServerFAQ";
import Footer from "../../components/Footer";
import PortfolioSection from '../../components/PortfolioSection';
import { withTranslation } from 'react-i18next';
import './style.scss';
import { Helmet } from "react-helmet";
const WindowsServerPage = (props) => {
    useEffect(() => {
        window.scrollTo(0, 0)
    }, []);
    return (
        <Fragment>
            <Helmet>
                <meta charSet="utf-8" />
                <title>Windows Server, windows server 2016 & windows vps in London </title>
                <meta name="description" content="Navicosoft offers you the best solution to increase your website uptime, security and scalability with our London's best windows server. " />
            </Helmet>
            <WindowsServer />
            <WindowsServerFAQ />
            <PortfolioSection />
            <Footer />
        </Fragment>
    )
};

export default withTranslation('common')(WindowsServerPage);