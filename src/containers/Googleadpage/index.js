import React, { Fragment, useEffect } from 'react';
import GoogleAd from '../../components/GoogleAd';
import DomainRegistrationFAQ from '../../components/DomainRegistrationFAQ';
import Footer from "../../components/Footer";
import PortfolioSection from '../../components/PortfolioSection';
import { withTranslation } from 'react-i18next';
import { Helmet } from "react-helmet";
import './style.scss';

const Googleadpage = (props) => {
    useEffect(() => {
        window.scrollTo(0, 0)
    }, []);

    return (
        <Fragment>
            <Helmet>
                <meta charSet="utf-8" />
                <title>Google ads, google ads specialist in London</title>
                <meta name="description" content="Passionate about your business growth? Let us make your business growth unstoppable with our google ads specialist agency in London." />

            </Helmet>
            <GoogleAd />
            <PortfolioSection />
            <Footer />
        </Fragment>
    )
};

export default withTranslation('common')(Googleadpage);