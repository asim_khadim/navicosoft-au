import React, { Component, Fragment } from 'react';
import Header from "../../components/header";
import TopBar from "../../components/TopBar";
import {Route} from "react-router-dom";

export class PrivateRoute extends Component {
    render() {
        const Component = this.props.component;
        return (
            <Fragment>
                <TopBar />
                <Header fixHeader={this.props.fixHeader} />
                <Route {...this.props} render={props => <Component {...props} />} />
            </Fragment>
        );
    }
}

export default PrivateRoute;
