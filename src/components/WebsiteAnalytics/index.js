import React, { Component } from 'react';
import websiteanalytics from '../../images/website-analytics/websiteanalytics.png';
import wavisitors from '../../images/website-analytics/wavisitors.png';
import triangleanalysis from '../../images/website-analytics/triangleanalysis.png';
import socialanalysis from '../../images/website-analytics/socialanalysis.png';
import contentanalysis from '../../images/website-analytics/contentanalysis.png';
import iranalysis from '../../images/website-analytics/iranalysis.png';
import ctanalysis from '../../images/website-analytics/ctanalysis.png';
import 'react-tabs/style/react-tabs.css';
import Grid from '@material-ui/core/Grid';
import ScrollAnimation from 'react-animate-on-scroll';
import "../../css/animate.min.css";
import './style.scss';

class WebsiteAnalytics extends Component {
    render() {
        return (
            <div className="backorder_domain website_analytics">
                <ScrollAnimation animateIn='bounceInRight' animateOut='bounceOutLeft'>  
                    <h1 className="slider_text"> Get detailed customer<br /><span className="slidertext_color"> Insights with our Extensive<br /> Website Analytics!</span><br />

                    </h1>
                </ScrollAnimation>
                <div className="backgroundimg_regis" style={{ backgroundImage: `url(${websiteanalytics})` }} alt="navicosoft">
                    <h3>Track your Potential Visitors & get<br />Website Analytics with us!</h3>
                    <form className="domain_search_form" action="https://www.navicosoft.com/seo-result/?domain=navicosoft.com.au" >
                        {/* <input type="hidden" name="token" value="eec641d7cf9cbd6175e6d159332fd5659548444f" /> */}
                        <input type="text" placeholder="WWW.." name="domain" required />
                        <button type="search"><i class="fas fa-search"></i>Search</button>
                    </form>
                    <p>Web Analytics $8.73/mo</p>
                </div>

                <div className="dedicated_background_section">
                    <h2 className="sectthreee_heading mobile_stationary">Enjoy our <span className="slidertext_color">Exceptional Website <br />Analytics Services </span></h2>
                    <p className="para_background_linux">Do you require a detailed and precise data report? The digital specialists at Navicosoft are available to assist you! We are a website analytics company that can assist you in maximising the effectiveness of analytics tools for your website and increase conversion rates. We provide extensive yet exact analytics tracking and reporting, as well as data analysis. Navicosoft, founded in 2008, is a digital marketing agency based in the UK that specialises in comprehensive digital solutions and website analytics.
                        <br /><br />Moreover, we also produce extraordinary outcomes for our clients across a wide variety of industry sectors. In an era of unparalleled growth, Navicosoft is at the forefront of providing seamless services that help you improve your top-line revenue, enhance efficiency, and help develop strategies for the perpetual growth of your business.</p>

                </div>

                <div className="dark_Section_background">
                    <div className="smm_dark_section">
                        <div className="smm_first_row">
                            <h2>Increase your lead generation with<br /> Google Website Analytics Audit!</h2>
                            <p>Businesses can only be managed when their growth can be measured.  We are providing web analytics implementations and consulting services for companies in London to help them improve their operations. For the purpose of ensuring that you are getting accurate and relevant information, we audit both Google Tag Manager and Google Analytics. You will benefit from the data and statistics provided to enhance your company condition and position in the marketplace.</p>
                        </div>
                        <hr />
                        <div className="smm_smam">
                            <h2>Get data-driven insights<br /> with Website analytics</h2>
                            <p>Navicosoft, a highly distinctive marketing and web hosting company in London, assists its customers in obtaining the most appropriate website analytics solution for their particular needs. It contains free website analytics tools such as Tag Manager, Webmaster, and others.<br /><br />
                                We assist you in analysing your data and providing you with appropriate solutions and suggestions for increasing the effectiveness of your marketing efforts and the performance of your website. We also provide additional paid marketing options, such as retargeting marketing, Google Ads and Google shopping management services, to suit your specific marketing requirements.
                                </p>
                        </div>
                    </div>
                </div>

                <div className="backorder_flow">
                    <h2 className="sectthreee_heading">This collection of solutions <br />addresses all  <span className="underline_heading">of your website analytics requirements.</span></h2>
                    <div className="flow_order">
                        <div className="search_order">
                            <div className="circle_search">
                                <img src={wavisitors} alt="navicosoft" />
                            </div>
                            <h3>Know About <br />Your Visitors</h3>
                            <p>Our website analytics approach enables you to keep track of who is visiting your website. You can monitor website visitors in minutes, including everything from their location to the time of their visit, the number of pages they visited, and the number of clicks they made. With web analytics and Google Analytics square space's strong reporting and capabilities, you can also receive a more in-depth study of each visitor to your website.</p>
                        </div>

                        <div className="search_order">
                            <div className="circle_search">
                                <img src={socialanalysis} alt="navicosoft" />
                            </div>
                            <h3>Social <br />Analytics</h3>
                            <p>The internet has transformed into a platform where people can socialise with one another and share their interests. Website analytics with Google Analytics Squarespace can be used to monitor the performance of your social media campaigns as well as the traffic to your website. Analysis of how users engage with the site's sharing services such as Facebook, Twitter, and Google is made possible via our service. Furthermore, we also manage to interact and engage your target audience with the most relevant material of your business.</p>
                        </div>

                        <div className="search_order">
                            <div className="circle_search">
                                <img src={contentanalysis} alt="navicosoft" />
                            </div>
                            <h3>Content <br />Analytics</h3>
                            <p>Install Google Analytics on your website and use the web analytics report to figure out which parts of your website are doing well and which parts are not. A website analytics tool can also be used to determine which pages of your website are receiving the most traffic and which pages are receiving the least. Additionally, you may make changes to your web pages to provide a better user interface and experience for your visitors.</p>
                        </div>

                    </div>
                </div>

                <div className="backorder_flow">
                    <div className="flow_order scnd_table">
                        <div className="search_order">
                            <div className="circle_search">
                                <img src={iranalysis} alt="navicosoft" />
                            </div>
                            <h3>Intelligent <br />Reporting</h3>
                            <p>Navicosoft provides the ability to generate customised reports for your website analytics data. The most helpful platform of web Analytics reporting is brought to you by us. We make it straightforward for you to monitor and understand the interaction on your website. We will also define the user behaviours that will be observed and the key performance indicators (KPIs) that will help you make smarter marketing decisions.</p>
                        </div>

                        <div className="search_order">
                            <div className="circle_search">
                                <img src={ctanalysis} alt="navicosoft" />
                            </div>
                            <h3>Conversion <br />Tracking</h3>
                            <p>Navicosoft provides the most up-to-date technique for tracking website traffic as well as conversion rates. Our free website analytics tool provides you with information on the number of people who have visited your website. Furthermore, Google Analytics Squarespace informs you about the selling information of your goods and services, as well as how visitors interact with your website.</p>
                        </div>

                    </div>
                </div>

                <div className="confess_section_background">
                    <div className="confess_section">
                        <div className="confess_text">
                            <h2>Why Navicosoft<br /> For Website Analytics <br />Report?</h2>
                            <p>Navicosoft has the ability to retrieve data and report user interactions with different components such as integrated tracking. Our experienced team creates customised analytics reports to monitor website traffic. Additionally, we offer continuous consulting and analysis of your data in order to optimise the user experience.</p>
                            <div class="font_text"><i class="far fa-check-circle"></i><p>Navicosoft guarantees to help you every step of the process.</p></div>
                            <div class="font_text"><i class="far fa-check-circle"></i><p>With Google Analytics Squarespace, we offer accurate statistics & reporting.</p></div>
                            <div class="font_text"><i class="far fa-check-circle"></i><p>Our support steam is available 24/7 to help you and resolve technical problems.</p></div>
                            <div class="font_text"><i class="far fa-check-circle"></i><p>As a Google website analytics specialist, we assist you in identifying your target audience via relevant data.</p></div>
                            <div class="font_text"><i class="far fa-check-circle"></i><p>Our staff is available to assist you with technical configuration-based problems.</p></div>
                        </div>
                        <div className="confess_img">
                            <img src={triangleanalysis} alt="navicosoft" />
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}

export default WebsiteAnalytics;
