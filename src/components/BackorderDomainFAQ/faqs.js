export const faqs = [
    {
        id: '1',
        question: 'How can I check Domain Name Status?',
        answer: 'With Navicosofts domain name checker, you can quickly determine the status of a domain name. Just enter your domain name followed by the desired tld in the checker bar and wait for the procedure to begin. If your desired domain is unavailable, it indicates that it has already been purchased by another party. However, if that domain is accessible, it implies that you may get it.'
    },
    {
        id: '2',
        question: 'Is there a guarantee of domain backorder with Navicosoft?',
        answer: 'Well, its not 100% sure whether you will be able to obtain your desired domain or not, but we continue to take every precaution to backorder expired domains. If your existing registrar does not renew your domain, we will make sure to register it for you via a safe channel to ensure your privacy. Once you connect to us, you will very certainly be able to get your desired domain name.'
    },
    {
        id: '3',
        question: 'How can I determine if the backordered domain name is still available?',
        answer: 'When you choose a backorder for a domain name with Navicosoft, we will inform you immediately as the domain becomes available for registration. Moreover, we take all necessary steps to track down the domain for you. Therefore, you wont have to worry about domain name availability as long as you are with us!'
    },
    {
        id: '4',
        question: 'What features does Navicosoft provide with the backorder expired domain?',
        answer: 'It is estimated that a million domain names expire each day worldwide. So, if you are looking to backorder an expired domain, our efficient backorder domain monitoring system will assist you in obtaining your desired domain in a short period of time. It keeps track of the domain name that you want to secure for yourself. You can rest assured that Navicosoft will look for the expired domain for you and inform you as soon as it becomes accessible.'
    },
    {
        id: '5',
        question: 'What is the proper procedure to backorder a domain name from Navicosoft in the UK?',
        answer: 'Not everyone is aware of the precise procedure for backordering a domain name. Additionally, the procedure differs per registrar, but the fundamentals remain the same. When you request Navicosoft to backorder your desired domain name, the payment is applied to the domain name as it approaches expiration. However, if several individuals want to backorder that domain name, we use the requested amount toward starting a bid.'
    },
    {
        id: '6',
        question: 'Where can I anticipate the backorder domain to appear in my account?',
        answer: 'If your desired domain name is successfully backordered, we will register it for you and transfer it to your account within 45 days. Thus, your domain will appear in your account. The domain name is renewed for a minimum of one year. You will be prompted to do the renewal after one year.'
    }
]