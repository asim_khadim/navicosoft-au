export const faqs = [
    {
        id: '1',
        question: 'What are the main differences between managed Linux server & an unmanaged Linux server?',
        answer: 'In managed Linux server, your hosting company take full responsibility for control panel installation, security and other basics. Whereas in an unmanaged Linux server, you will only be given a server that needs to be managed from your end.'
    },
    {
        id: '2',
        question: 'Is it possible to upgrade my plan after I have placed my order?',
        answer: 'Upgrades to your plan are possible at any time if necessary. In contrast to other service providers, we believe in meeting the requirements of our customers. When you discover that your existing server is no longer enough, just submit a request, and we will update your plan to meet your needs.'
    },
    {
        id: '3',
        question: 'How long it takes for a server to be started?',
        answer: 'We guarantee on-time delivery of services since we understand the significance of time in the corporate world. So, with Navicosoft, you can be confident that the server will be available immediately after the purchase. Once you make your purchase, we begin the process of setting up a server for you. As a result, your server is provided instantly after you make the payment.'
    },
    {
        id: '4',
        question: 'Why do I really need a dedicated Ubuntu ssh server?',
        answer: 'Having a dedicated Ubuntu server is never a waste owing to the incalculable advantages it offers. However, if you are operating a resource-intensive website, such as an eCommerce site, investing in a Linux server is critical. The sophisticated security features combined with unlimited scalability make it the ideal solution for your digital business requirements.'
    }
];