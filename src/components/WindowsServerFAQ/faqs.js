export const faqs = [
    {
        id: '1',
        question: 'What is a Windows Server?',
        answer: 'A Windows VPS server is primarily a virtual private server that is fully devoted to your needs and requirements. The resources are entirely reserved exclusively for your use, ensuring that your websites are hosted with complete security, scalability as well as maximum uptime. You will also be provided with the Plesk control panel, which will allow you to administer your website.'
    },
    {
        id: '2',
        question: 'Why should I choose cheap Windows VPS instead of shared hosting? ',
        answer: 'If you want more control over your hosting environment, a Windows VPS server is the best option for you. A virtual private server (VPS) or dedicated server offers you more control and flexibility in meeting your resource requirements, which may include disc storage, RAM, and other resources. On the other hand, shared hosting, although it is a more cost-effective option, for the time being, has many disadvantages in terms of security, scaling, and flexibility.'
    },
    {
        id: '3',
        question: 'Does Navicosoft provide a Windows Server in the United Kingdom? ',
        answer: 'Navicosoft is dedicated to making your life simpler in every way. We prefer to be available to you in the most beneficial manner possible. From shared hosting platforms to a dedicated and virtual private server (VPS) hosting, you can be certain that we provide any kind of hosting in the UK. As a digital marketing agency, we also provide website design, development and SEO services.'
    },
    {
        id: '4',
        question: 'Is it possible to increase the resources of my Windows VPS?',
        answer: 'When you use our services, our first priority is to meet your requirements. Do not be concerned if your resources are running low since Navicosoft offers the simplest method for scaling up your resources. Simply contact us or submit a ticket describing your exceptional requirements, and we will take care of the rest. Additionally, the Plesk panel allows you to control your resources.'
    }
];