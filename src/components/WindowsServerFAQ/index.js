import React, { Component } from 'react';
import { faqs } from './faqs'
import './style.scss';

class WindowsServerFAQ extends Component {
    render() {
        return (
            <div className="digitalmarkettingfaq">
                <h2>FAQ’s</h2>
                {faqs.map((faq) => {
                    return (
                        <div key={faq.id}>
                            <h3>{faq.question}</h3>
                            <p>{faq.answer}</p>
                        </div>

                    )
                })}
            </div>
        );
    }
}

export default WindowsServerFAQ;
