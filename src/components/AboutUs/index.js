import React, { Component } from 'react';
import aboutus from '../../images/aboutus/aboutus.png';
import stratimage from '../../images/aboutus/stratimage.png';
import whoweare from '../../images/aboutus/whoweare.png';
import visionstatement from '../../images/aboutus/visionstatement.png';
import arrow from '../../images/aboutus/arrow.png';
import arrowtext from '../../images/aboutus/arrowtext.png';
import dottedimage from '../../images/aboutus/dottedimage.png';
import office from '../../images/aboutus/office.png';
import officeimage from '../../images/aboutus/officeimage.png';
import bigyellowtick from '../../images/aboutus/bigyellowtick.png';
import 'react-tabs/style/react-tabs.css';
import Grid from '@material-ui/core/Grid';
import ScrollAnimation from 'react-animate-on-scroll';
import "../../css/animate.min.css";
import { Hidden } from '@material-ui/core';


import './style.scss';

class AboutUs extends Component { render() { return (
<div className="about_us">
    <div className="about_us_background" style={{ backgroundImage: `url(${aboutus})` } } alt="navicosoft">
            <h2>Navicosoft Brings <br />Mesmerizing Services</h2>
            <p>& an Enthusiastic Squad to <br/>Make a Place to Fulfill your Digital<br />Needs Here in Australia!  </p>
    </div>

    <div className="section_strategy">
        <h2 className="slider_text"> We Bring The Right <br /><span className="slidertext_color"> Strategy For You to Prove Yourself</span> <br /> Smartly in Every Move!<br />
         </h2>
         <div className="text_strat">
            <div className="left_sect">
                <p><strong>Navicosoft is the paramount and cheap website hosting & website development company in Australia, providing subtle IT solutions to people, as well as small to medium-sized organizations & businesses.</strong></p><br/>
                <p>We bring the strategy to serve you the things right. With a strong emphasis on customer care & support, we, as a web hosting and digital marketing agency, focus on superior support and long-term friendly relationships with our clients. Whether you are just starting it out, growing fast, or already running a large business, we have your back. Our business model is focused on reliability, as well as trust, which we believe directly relates to our success over a decade.<br/><br/>
                    Our services range from the most flexible and cheap website hosting packages to website development, SEO services, Social Media Marketing services, website security SSL certificates, and many more! We target to continue your business growth to make you a market leader in Australia. We are committed to providing you the fine quality yet affordable services through our sustainable initiatives.</p>
            </div>
            <div className="right_sec">
                <img src={stratimage} className="strat_img" alt="navicosoft" loading="lazy" />
            </div>
         </div>
    </div>

    <div className="section_whoweare" style={{ backgroundImage: `url(${whoweare})` } } alt="navicosoft">
            <h2>Who Are We?</h2>
            <p>We are not just another web development or digital marketing agency in Australia, but we come up with the solution to solve the complex digital issues of our clients.<br /><br />
                Navicosoft is the most trusted Australian web hosting company for over a decade. We are on the mission to connect your website with the world in the fastest and most reliable way. When you choose Navicosoft, we help you find the best suitable plans for your needs. Not only that but Navicosoft was formed in 2008 with the ambition to deliver affordable yet high-quality web hosting, website development, digital marketing, as well as website security solutions to the Australian market.<br /><br />
                Today millions of businesses have made their digital home over the internet with Navicosoft. We make enterprises more efficient and productive because that is what we are; enterprise masters! Navicosoft, as a website development company, brings enterprise information management, business process management, and eCommerce business solutions to each organization and firm.<br /><br />
                We are here to solve your complex web problems and provide you the web application solutions, web design, & development solutions, as well as mobile application solutions. We deploy and run custom solutions with complete monitoring and support round the clock.</p>
    </div>

    <div className="section_vision_statement">
        <h2>Our Vision<br /><span className="purple_size">Statement</span></h2>
        <div className="vision_text_img">
            <div className="vision_left_text">
                <img src={visionstatement} className="strat_img" alt="navicosoft" loading="lazy" />
            </div>
            <div className="vision_right_text">
                <p>Our vision, as a team, is to empower every person to achieve superior IT services because we know about your business goals and how to bring them to new heights instantly! We always strive to provide you a diverse set of services including domain, and hosting, digital marketing, website development, as well as website security.<br /><br />
                    Our visionary motto is “nimble, marvelous, and approachable services for everyone.” So, to accelerate and amplify your digital existence without security issues, we exhibit value-packed services as we are bound to deliver what we have committed!</p>
            </div>
        </div>
    </div>

    <div className="secret_sause_background" style={{ backgroundImage: `url(${arrow})` } } alt="navicosoft">
        <div className="secret_sause_text">
            <h2>The Secret-sauce <br/><span className="orange_color">Behind Our Success!</span></h2>
            <p className="secrect_sause_textone">A high conversion website is a lead generation channel that helps to build a handsome revenue stream. Navicosoft, as a web design, and website development company, always stands at the forefront to reach your potential customers and engage them with interesting content and an attractive interface in such a way that they finally convert into your regular visitors. Not only that, but we engineer and design your website according to your business needs. As the best web design company in Australia, we provide you with fully equipped websites with all the essential features and functionalities. </p>
            <p className="secrect_sause_texttwo">We have skilled expertise in content writing and SEO, which can help you stand out from your rivals and portray your brand across the globe by ranking you on the search engine's first page! Our content writing services with a budget-friendly approach can give you a good throw on the way to achieve the best. We totally understand the need for digital marketing you require for your brand's awareness. It's time to grow and enhance your name with digital marketing. We are an agency to bring your imaginations into reality. </p>
            <p className="secrect_sause_textthree">We, as a digital marketing agency, believe in serving our customers with the most competent pricing schemes. Consider us as your brand enhancers because they have incredible marketing solutions for you. Despite all these services, we own staff members who are always there to assist you with their friendly behavior.</p>
        </div>
    </div>


    <div className="technical_specification">
                    <h2 className="web_hosting_h2">We Serve in Come <br />Today & Get Today Style!</h2>
                    <p>We are nailing to serve our clients with the best services and best features in Australia for many years.<br /> When you partner with us, we provide you the surety to partner with innovative cloud technology <br />solutions. Furthermore, our team is spread across the world to help you find your answers, irrespective of <br />the time zone! We are here with the top IT solutions for over 100,000 creatives.</p>
                    <Grid container spacing={3} className="grid_technical">
                    <Grid item xs={12} sm={4}>
                        <div className="technical_content">
                            <img src={arrowtext} className="" alt="navicosoft" loading="lazy" />
                            <p>Beautifully designed <br/>web interfaces.</p>
                        </div>
                        </Grid>
                        <Grid item xs={12} sm={4}>
                        <div className="technical_content">
                            <img src={arrowtext} className="" alt="navicosoft" loading="lazy" />
                            <p>Solid Security <br />Measures</p>
                        </div>
                        </Grid>
                        <Grid item xs={12} sm={4}>
                        <div className="technical_content">
                            <img src={arrowtext} className="" alt="navicosoft" loading="lazy" />
                            <p>Simple Collaboration <br />Tools</p>
                        </div>
                        </Grid>
                    </Grid>
                    <Grid className="grid_technical" container spacing={3}>
                    <Grid item xs={12} sm={4} >
                        <div className="technical_content">
                            <img src={arrowtext} className="" alt="navicosoft" loading="lazy" />
                            <p>Built-in <br />Site Stats</p>
                        </div>
                        </Grid>
                        <Grid item xs={12} sm={4}>
                        <div className="technical_content">
                            <img src={arrowtext} className="" alt="navicosoft" loading="lazy" />
                            <p>Easy CDN <br />Integrations</p>
                        </div>
                        </Grid>
                        <Grid item xs={12} sm={4}>
                        <div className="technical_content">
                            <img src={arrowtext} className="" alt="navicosoft" loading="lazy" />
                            <p>Up to 99.99% of <br />Server Uptime.</p>
                        </div>
                        </Grid>
                    </Grid>
                    <Grid className="grid_technical align_center_column" container spacing={3}>
                    <Grid item xs={12} sm={4} >
                        <div className="technical_content">
                            <img src={arrowtext} className="" alt="navicosoft" loading="lazy" />
                            <p>Free SSL certificates <br />powered by let’s encrypt</p>
                        </div>
                        </Grid>
                        <Grid item xs={12} sm={4}>
                        <div className="technical_content">
                            <img src={arrowtext} className="" alt="navicosoft" loading="lazy" />
                            <p>Expert support <br />Availability noon & night</p>
                        </div>
                        </Grid>
                    </Grid>
                    
                </div> 


        <div className="aboutus_our_mission">
            <div className="our_mission" style={{ backgroundImage: `url(${dottedimage})` } } alt="navicosoft">
                <div className="tetxt_ourmission">
                <h2>Our Mission</h2>
                <p>For more than a decade, Navicosoft has been revolving around providing services reliable enough to bring more than expected. We, as a team, focus on meeting our customer's expectations and needs as the most important thing for us is you. The foundation and aspiration of our company are built on the trust you put in us. Our mission statement is to deliver you the best, as we believe in flaunting outstanding service for outstanding you! Since our only concern is to achieve you the best kind of services, so rest assured to enjoy great achievements with us today and in the future.</p>
                </div>
            </div>
            <div className="right_images_ourmission">
                <img src={office} className="image1" alt="navicosoft" loading="lazy" />
                <img src={officeimage} className="image2" alt="navicosoft" loading="lazy" />
            </div>
        </div>

        <div className="google_ad">
        <div className="backorder_flow">
                    <h2 className="sectthreee_heading">Our Value & Ethics</h2>
                    <div className="flow_order">
                        <div className="search_order">
                            <div className="circle_search">
                                <img src={bigyellowtick} alt="navicosoft"/>
                            </div>
                            <p>We owe to create <br />sustainable, <br />reliable, and smart <br />solutions for all.</p>
                        </div>

                        <div className="search_order">
                            <div className="circle_search">
                                <img src={bigyellowtick} alt="navicosoft"/>
                            </div>
                            <p>As a global family,<br /> we value our <br />promises and your <br />investments.</p>
                        </div>

                        <div className="search_order">
                            <div className="circle_search">
                                <img src={bigyellowtick} alt="navicosoft"/>
                            </div>
                            <p>We shape your <br />success with <br />innovation at every <br />step.</p>
                        </div>

                        <div className="search_order">
                            <div className="circle_search">
                                <img src={bigyellowtick} alt="navicosoft"/>
                            </div>
                            <p>We aim to deliver <br />the best services <br />and standing right <br />beside you in any <br />technical need.</p>
                        </div>

                        <div className="search_order">
                            <div className="circle_search">
                                <img src={bigyellowtick} alt="navicosoft"/>
                            </div>
                            <p>Navicosoft ensure <br />to provide a <br />compact package <br />of service, support, <br />integrity</p>
                        </div>
                    </div>
                    
                </div>
                </div>

</div>
); } } export default AboutUs;