import React, { Component } from 'react';
import dedicatedhosting from '../../images/dedicatedhosting.png';
import 'react-tabs/style/react-tabs.css';
import pic from '../../images/pic.png';
import imgg2 from '../../images/imgg2.png';
import imgg3 from '../../images/imgg3.png';
import securededicatedserver from '../../images/securededicatedserver.png';
import enterprisehardware from '../../images/enterprisehardware.png';
import remoteaccess from '../../images/remoteaccess.png';
import bandwidthmonitoring from '../../images/bandwidthmonitoring.png';
import maximumuptime from '../../images/maximumuptime.png';
import managedsolution from '../../images/managedsolution.png';
import heighcapacity from '../../images/heighcapacity.png';
import manageaccount from '../../images/manageaccount.png';
import mysqldatabase from '../../images/mysqldatabase.png';
import clickinstallation from '../../images/1clickinstallation.png';
import tick from '../../images/tick.png';
import whynavicosectionimg from '../../images/whynavicosectionimg.png';
import whynavicosectionimg2 from '../../images/whynavicosectionimg2.png';
import whynavicosectionimg3 from '../../images/whynavicosectionimg3.png';
import Grid from '@material-ui/core/Grid';
import ScrollAnimation from 'react-animate-on-scroll';
import "../../css/animate.min.css";
import './style.scss';
import Products from '../Products';
import { HashLink } from 'react-router-hash-link';

class DedicatedHosting extends Component {
    render() {
        return (
            <div className="dedicated_hosting">
                <ScrollAnimation animateIn='bounceInRight' animateOut='bounceOutLeft'>
                    <h1 className="slider_text">Dedicated Server <br /><span className="slidertext_color"> Hosting Solutions, Committed to <br />Helping You Create the Perfect  <br />Digital Connection! </span><br />
                        <HashLink smooth to="#buy-now">
                        <button className="btn_digitalmarketting">
                        <a>Go to packages</a>
                        </button>
                        </HashLink>
                    </h1>
                </ScrollAnimation>
                <div className="dedicated_background_section" style={{ backgroundImage: `url(${dedicatedhosting})` }} alt="Navicosoft Dedicated Hosting">
                    <h2>Fast & Safe <br /> UK’s Dedicated <br />Servers</h2>
                    <p>To establish your business online, you must prioritise superior  <br />quality! We provide fastest UK's dedicated servers in order  <br />to facilitate you with the finest dedicated server hosting  <br />experience possible. The moment has come to enjoy the speed  <br />you deserve. Indeed, grab one of our dependable UK dedicated  <br />servers to benefit from robust features and performance.<br /><br />
                    Additionally, we provide affordable dedicated Server hosting  <br />with data centres situated in the United Kingdom. As a result,  <br />your company in UK may benefit from high speed connectivity  <br />to its surrounds for a fraction of the expense of traditional  <br />dedicated server hosting. In addition to this, we facilitate you with:</p>
                    <div className="font_text"><i className="far fa-check-circle"></i><p>VIP attention to your business with<br /> high speed servers.</p></div>
                    <div className="font_text"><i className="far fa-check-circle"></i><p>Fastest website speed <br />and security. </p></div>
                </div>
                <div className="purchase table_pricing dedi_host" id="buy-now">
                    <div className="container-fluid">
                        <h2 className="web_hosting_h2"><span>Our Cheap Dedicated </span>Server Hosting Plans</h2>
                        <ul>

                            <Products
                                group_id={5}
                                cycle={'annually'}
                                imgsrc={pic}
                                product_title={''}
                                perprice={'mo'}
                                action={'dedicatedHostingPlans'}
                            />
                        </ul>

                    </div>
                </div>

                <div className="secure_dedicated_background_section" style={{ backgroundImage: `url(${securededicatedserver})` }} alt="Navicosoft Dedicated Hosting">
                    <h2>Fast & Safe UK’s <br />Dedicated Servers</h2> 
                    <p>Both managed and unmanaged cheap dedicated servers help you define your online space. Our dedicated<br /> hosting in London will be ideal for your business, meeting all performance and high uptime<br /> requirements. We are focused on helping you develop your company even further. Furthermore, our cheap <br />dedicated servers are ideal for applications that need high processing power and storage space.<br /><br />
                    Given that our dedicated servers provide the highest possible performance and the custom configuration <br />to your specifications, they are unquestionably the most reliable solution available. In addition, DDoS<br /> protection is included with every one of our dedicated servers, providing you a complete sense of <br />security. Only one thing remains for you to do: choose the plan that best suits your company's requirements!</p>
                </div>

                <div className="businessstationary">
                    <div className="businessstationary_process"> 
                        <h2>Main characteristic <span className="underline_heading">of Dedicated Servers</span></h2>
                        <p>We offer enterprise-level hardware together with custom-built servers that provide optimum performance<br /> and dependability while being cost-effective. With dedicated server hosting, we offer you all of the key<br /> features that you are likely to need. Aside from that, we guarantee that we will be an excellent<br /> option for your next dedicated server!</p>
                        <div className="process_table_business">
                            <div className="process_table_three">
                                <div className="square_describe">
                                    <img src={enterprisehardware} alt="Navicosoft Dedicated Hosting"/>
                                </div>
                                <h3>Enterprise Hardware</h3>
                                <p>We offer you with industry-leading servers that are capable of meeting even the most demanding requirements, including those involving big data analytics. You may either select from pre-configured packages or get in touch with us if you need to customise any packages.</p>
                            </div>
                            <div className="process_table_three">
                                <div className="square_describe">
                                    <img src={remoteaccess} alt="Navicosoft Dedicated Hosting"/>
                                </div>
                                <h3>Remote Access</h3>
                                <p>Our affordable UK dedicated servers provide complete administrative and root access. This implies that you can always control your server. Additionally, you may get access through a remote access card associated with a separate IP address.</p>
                            </div>
                            <div className="process_table_three">
                                <div className="square_describe">
                                    <img src={bandwidthmonitoring} alt="Navicosoft Dedicated Hosting"/>
                                </div>
                                <h3>Bandwidth Monitoring</h3>
                                <p>Our dedicated servers are monitored for bandwidth use at all times. We offer 100% accurate traffic statistics, as well as complete access to graphs and usage reports for our clients with no extra charges. </p>
                            </div>
                        </div>
                        <div className="process_table_business second_table_dedicate">
                            <div className="process_table_three">
                                <div className="square_describe">
                                    <img src={maximumuptime} alt="Navicosoft Dedicated Hosting"/>
                                </div>
                                <h3>Maximum Uptime</h3>
                                <p>Our network guarantees that data to and from your server is sent in the quickest time possible. Servers can be linked to network connections with speeds of up to 20 Gbps on some top bandwidth providers, with uptime guaranteed at 100%. Thus, you can be confident that your server will be available at all times.</p>
                            </div>
                            <div className="process_table_three">
                                <div className="square_describe">
                                    <img src={managedsolution} alt="Navicosoft Dedicated Hosting"/>
                                </div>
                                <h3>Managed Solutions</h3>
                                <p>Many managed dedicated server options are available for businesses that need the least amount of downtime or time spent on technical assistance. Please contact us for more information. Furthermore, this encompasses everything from proactive server monitoring to fully managed hardware and software.</p>
                            </div>
                            <div className="process_table_three">
                                <div className="square_describe">
                                    <img src={heighcapacity} alt="Navicosoft Dedicated Hosting"/>
                                </div>
                                <h3>High Capacity IP addresses</h3>
                                <p>We provide a large number of IP addresses with our low-cost dedicated server hosting plans. In addition, our technical staff can provide thousands of IP addresses to ensure your 24/7/365 network accessibility as per your requirements.</p>
                            </div>
                        </div>

                    </div>
                </div>

                <div className="bestplan_section_background">
                <div className="section_two">
                    <div className="secondtextsec"> 
                        <h2>The Most Effective Control<br />Panel for Dedicated Servers</h2>
                        <p>Navicosoft is one of the most trusted webhosting companies in the United Kingdom, we offer you with the most user-friendly cPanel and a dedicated server to handle your hosting, website, and domain names. Furthermore, you have the ability to create subdomains, email accounts, apps, and a variety of other features. Now install 200+ apps with just a single click.</p>
                    </div>
                    <div className="imgandtextsec">
                        <div className="lefttextsec">
                            <h2>Manage accounts</h2>
                            <p>Let’s enjoy many features such as Create email accounts, edit FTP accounts and change passwords.</p>
                        </div>
                        <div className="rightimgsec"><img src={manageaccount} className="righttext_img" alt="Navicosoft Dedicated Hosting" /></div>
                    </div>

                    <div className="imgandtextsec">
                        <div className="lefttextsec">
                            <h2>MySQL Databases</h2>
                            <p>Create MySQL databases and administer them using the PHP MyAdmin interface. Allow your customers to have their own MYSQL databases on your servers.</p>
                        </div>
                        <div className="rightimgsec"><img src={mysqldatabase} className="righttext_img" alt="Navicosoft Dedicated Hosting" /></div>
                    </div>

                    <div className="imgandtextsec">
                        <div className="lefttextsec">
                            <h2>1-click installation</h2>
                            <p>With dedicated server hosting, you can now build a softaculous application using the one-click installation feature.</p>
                        </div>
                        <div className="rightimgsec"><img src={clickinstallation} className="righttext_img" alt="Navicosoft Dedicated Hosting" /></div>
                    </div>
                </div>
                </div>

                <div className="sectionthree">
                    <h2 className="sectthreee_heading mobile_stationary">Why Navicosoft for <br /><span className="slidertext_color">Dedicated Server in United Kingdom?</span></h2>
                    <p className="center_text_stationary">It's likely that you're seeking for the most reliable hosting partner in UK when you're looking for a dedicated server around your area. Navicosoft<br /> is the most qualified company in UK to offer you with all of the features you need.</p>
                    <div className="digitalmarketting_section">
                        <div className="tickimg1">
                            <img src={tick} alt="Navicosoft Dedicated Hosting"/>   
                            <p className="digitalmkt_txt">Our technical staff is available around the clock to assist you with any technical issues you may be experiencing.</p>
                        </div>
                        <div className="tickimg2">
                            <img src={tick} alt="Navicosoft Dedicated Hosting"/> 
                            <p className="digitalmkt_txt">We promise 99.9% uptime as well as optimum performance on our cheap dedicated servers.</p>
                        </div>
                        <div className="tickimg">
                            <img src={tick} alt="Navicosoft Dedicated Hosting"/> 
                            <p className="digitalmkt_txt">Our servers are equipped with solid-state storage (SSD)as well as DDoS protection.</p>
                        </div>
                    </div>
                </div>
                <div className="partner_section">
                    <div className="partner_images">
                        <img className="first_dedi_img" src={whynavicosectionimg} alt="Navicosoft Dedicated Hosting"/>
                        <img src={whynavicosectionimg2} alt="Navicosoft Dedicated Hosting"/>
                        <img src={whynavicosectionimg3} alt="Navicosoft Dedicated Hosting"/>
                    </div>
                </div>
            </div>
        );
    }
}

export default DedicatedHosting;
