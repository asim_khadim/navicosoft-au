import React, { Component } from 'react';

import './style.scss';

class WebDesignFAQ extends Component {
    render() {
        return (
            <div className="grey_background_faq">
                <div className="digitalmarkettingfaq webdesign_faq">
                    <h2>FAQ's </h2>
                    <div>
                        <h3>What is website design?</h3>
                        <p>Website design is the process of designing websites that are viewable on the internet and is sometimes referred to as internet design. It is mainly concerned with the elements of website creation that affect the user experience rather than with software development. While web design is primarily concerned with creating websites for desktop and mobile browsers, design for mobile browsers and tablets has grown in popularity and importance.<br /><br />
                            A web designer has the expertise to works on the layout, appearance, and sometimes the website content of a website. In contrast to appearance, which includes colours, pictures, and fonts, layout refers to how the content and structure of a website design are recorded.  A well-designed website is always interactive, user-friendly, visually appealing, and aligns with the brand's mission and target audience.
                            </p>
                    </div>
                    <div>
                        <h3>Why should I invest in developing a website for my business?</h3>
                        <p>If your company has been operating well without a website and you're questioning if you really need one. Why would I use a website if my business is already successful?<br /><br />
                            The correct response is that there has never been a more critical or advantageous moment to invest in a good website. A well-designed website provides many advantages to both small and big companies. The majority of advantages accrue exponentially in value over time, much like the internet. Are you still worried? Consider the following significant benefits of creating your website:
                            </p>
                        <ul className="tick_faq_css">
                            <li>Your company will seem more professional with a website.</li>
                            <li>A well-designed website attracts new consumers through Google.</li>
                            <li>An effective way to demonstrate your products and services.</li>
                            <li>Web design allows you to display customer’s reviews and testimonials about your company.</li>
                            <li>A well-designed website compels visitors to contact you.</li>
                            <li>Make use of Google Maps to integrate your website.</li>
                            <li>Create a presence in the market by creating a website.</li>
                            <li>Easily manageable</li>
                            <li>The website guarantees the company's success and brand recognition.</li>
                        </ul>
                    </div>
                    <div>
                        <h3>What can I reasonably expect from your website design and development company?</h3>
                        <p>With the advancement of technology, it's unsurprising that a large number of companies have simultaneously established an online presence. Therefore, there is an increased demand for web design and development companies.<br /><br />
                            A competent web design company will almost certainly be able to offer you all of the functionality you need for your website. The company you choose should be able to accomplish all of the underlying objectives following your specifications. Furthermore, they should organise the whole website so that the essential components and features utilised in the future are prominently shown. Once the site is created and launched, a website design company should also offer mockups so that you can see how your website will seem more appealing and viewed differently. Finally, the web design company should have adequate expertise to comprehend your requirements and create a design layout that meets those requirements.
                            </p>
                    </div>
                    <div>
                        <h3>What is meant by responsive website design?</h3>
                        <p>The majority of you have probably seen a web page that seems OK on your PC but behaves erratically on mobile phones or other portable devices. It is possible that you may lose a client due to poor website design if your website design is not properly optimised to open correctly on every platform and device, whether it is a mobile device or any other device.<br /><br />
                            Responsive website design provides you with the ability to create websites that are readily adaptable across all platforms, whether they are on a desktop, mobile phone, tablet, or any other device. In other words, your website will be able to interact with your consumers flawlessly across all platforms. Website designs that alter themselves and adapt to the size and characteristics of the device are known as responsive web designs. The interface is very user-friendly, and it includes a number of dynamic adjustments.
                            </p>
                    </div>

                </div>
            </div>
        );
    }
}

export default WebDesignFAQ;
