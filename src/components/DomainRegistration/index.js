import React, { Component } from 'react';
import domainregistrationimg from '../../images/domainregistrationimg.png';
import managedomain from '../../images/managedomain.png';
import tick from '../../images/tick.png';
import confessimg from '../../images/confessimg.png';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
import { HashLink } from 'react-router-hash-link';
import './style.scss';

class DomainRegistration extends Component {
    render() {
        return (
            <div className="container_dm">
                <h1 className="slider_text"><span className="slidertext_color">
                Let’s Rock Your<br /> </span> Business With A <span className="slidertext_color">Best <br />Domain Name</span><br />
                    <HashLink smooth to="#search">
                        <button className="btn_digitalmarketting">
                            <a>Search Your Domain</a>
                        </button>
                    </HashLink>
                </h1>
                <div className="backgroundimg_regis" style={{ backgroundImage: `url(${domainregistrationimg})` }} id="search" alt="Navicosoft Domain Registration">
                    <form className="domain_search_form" action="https://register.navicosoft.com/domainchecker.php" >
                        <input type="hidden" name="token" value="eec641d7cf9cbd6175e6d159332fd5659548444f" />
                        <input type="text" placeholder="WWW.." name="domain" required />
                        <button type="search"><i className="fas fa-search"></i>Search</button>
                    </form>
                </div>


                <div className="sectionthree"></div>
                <h2 className="sectthreee_heading"><span className="heading_color">WOW! </span> Free Perks With <br />Every <span className="underline_heading">Domain Registration</span></h2>
                <div className="digitalmarketting_section">
                    <div className="tickimg1">
                        <img src={tick} alt="Navicosoft Domain Registration" />
                        <h3>Domain Manager</h3>
                        <p className="digitalmkt_txt">With our user-friendly domain management tool, you can manage all of your domain registrations, domain transfers, billing, and services from one location.</p>
                    </div>
                    <div className="tickimg2">
                        <img src={tick} alt="Navicosoft Domain Registration" />
                        <h3>DNS Hosting</h3>
                        <p className="digitalmkt_txt">Once you register a domain name, you may take control of your DNS settings. With our dependable and powerful DNS hosting solution, you may configure real-time DNS records from anywhere in the world.</p>
                    </div>
                    <div className="tickimg3">
                        <img src={tick} alt="Navicosoft Domain Registration" />
                        <h3>24/7 Support</h3>
                        <p className="digitalmkt_txt">Our technical support staff makes every single effort to be accessible for you 24/7/365. </p>
                    </div>
                    <div className="tickimg">
                        <img src={tick} alt="Navicosoft Domain Registration" />
                        <h3>Domain Transfer</h3>
                        <p className="digitalmkt_txt">We provide you with an open policy, simple domain transfer at any time and from any location, and, unlike other registrars, we do not charge you any additional fees or costs.</p>
                    </div>
                </div>
                <div className="domainsearch_btn">
                    {/* <button>Search Your Domain</button> */}
                    <HashLink smooth to="#search">
                        <button className="btn_digitalmarketting">
                            <a>Search Your Domain</a>
                        </button>
                    </HashLink>
                </div>

                <div className="confuse_background">
                <div className="confess_section">
                    <div className="confess_text"> 
                        <h2>Looking for A<br />Perfect Domain<br /> NameFor your Brand?</h2>
                        <p>We understand the significance of choosing the appropriate domain name while creating your website. A domain is a combination of your website address, such as www.navicosoft.co.uk, and your email address, such as <span className="heading_color">name@navicosoft.co.uk</span>.</p>
                        <p>Your domain name or web address should be concise, easy to remember, and accurately represent your business. To begin the domain registration procedure, enter the desired domain name in our domain search bar and click "Search," then make an order and have it registered instantly!</p>
                    </div>
                    <div className="confess_img">
                        <img src={confessimg} alt="Navicosoft Domain Registration" />
                    </div>
                </div>
                </div>

                <div className="domain_extension_section">
                    <h2>Which Domain Extension <br /><span className="heading_color">is right</span> For You?</h2>
                    <p>There are multiple options for Domain Extension. Feel free to<br /> register as many domains as you want!</p>
                    <div className="tabs_domain">
                        <Tabs defaultIndex={1}>
                            <TabList>
                                <Tab>Generic</Tab>
                                <Tab>Country</Tab>
                                <Tab>Global</Tab>
                            </TabList>

                            <TabPanel>
                                <div className="tabs_section_custom">
                                    <div className="tabs_text">
                                        <h3>Generic</h3>
                                        <p>These are also referred to as generic top-level domains (gTLDs) or new top-level domains (NTLDs), and they will assist you in increasing business awareness of your brand. We recommend that you register a domain name for your industry-related TLD in addition to your country domain.</p>
                                    </div>
                                    <div className="tabs_table">
                                        <ul>
                                            <li>Example</li>
                                            <li><span className="textbold_tabs">.cafe</span> for <span className="textbold_tabs">a coffee shop</span></li>
                                            <li><span className="textbold_tabs">.plumbing</span> for<span className="textbold_tabs"> a plumber</span></li>
                                        </ul>
                                    </div>
                                </div>
                            </TabPanel>
                            <TabPanel>
                                <div className="tabs_section_custom">
                                    <div className="tabs_text">
                                        <h3>Country</h3>
                                        <p>This type of domains are known as country code top-level domains (cc TLDs) or country code domains, are ideal for linking you with a particular region's intended audience. They are the most well-known principal domain names for a wide range of businesses.</p>
                                    </div>
                                    <div className="tabs_table">
                                        <ul>
                                            <li>Example</li>
                                            <li><span className="textbold_tabs">.com.au</span> for <span className="textbold_tabs">Australia</span></li>
                                            <li><span className="textbold_tabs">.PK</span> for<span className="textbold_tabs"> Pakistan</span></li>
                                        </ul>
                                    </div>
                                </div>
                            </TabPanel>
                            <TabPanel>
                                <div className="tabs_section_custom">
                                    <div className="tabs_text">
                                        <h3>Global</h3>
                                        <p>These domains are referred to as top-level domains. The following domain kinds are the most appropriate for you if your website is intended to target the whole world. The vast majority of people and companies register both a worldwide domain and a country-specific domain at the same time.</p>
                                    </div>
                                    <div className="tabs_table">
                                        <ul>
                                            <li>Example</li>
                                            <li><span className="textbold_tabs">.org </span> for <span className="textbold_tabs">educational or global organizations</span></li>
                                            <li><span className="textbold_tabs">.com</span> for<span className="textbold_tabs"> commercial purposes </span></li>
                                        </ul>
                                    </div>
                                </div>
                            </TabPanel>
                        </Tabs>
                    </div>
                </div>

                <div className="manage_background">
                <div className="manage_domain_section">
                    <div className="backgroundimg_regis" style={{ backgroundImage: `url(${managedomain})` }} alt="Navicosoft Domain Registration">
                        <div className="text_manage_domain">
                            <h2>How to manage your<br /> domain name?</h2>
                            <p>With our comprehensive domain registration management, you can manage your domain registration fast and instantly! Using our domain manager, you can now manage all of your activities such as registering a domain name, transfer a domain, domain renewals, billing, and other services from one place, saving you time and money. It is included in the price of every domain registration you purchase with us.</p>
                            <p>With our one-click setup tool, you can connect your just bought domain to your web hosting account in seconds! Our domain management tool includes features for managing and grouping your domains, such as bulk updates and domain registration folders that make it simple to manage and organise your domains.</p>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        );
    }
}

export default DomainRegistration;
