import React, { Component } from 'react';
import windowsserve from '../../images/windowsserver/windowsserver.png';
import pleskcp from '../../images/windowsserver/pleskcp.png';
import fullycustomized from '../../images/windowsserver/fullycustomized.png';
import unlimitedbandwidth from '../../images/windowsserver/unlimitedbandwidth.png';
import vmware from '../../images/windowsserver/vmware.png';
import guranteeuptime from '../../images/windowsserver/guranteeuptime.png';
import firewallmanagement from '../../images/windowsserver/firewallmanagement.png';
import sslenc from '../../images/windowsserver/sslenc.png';
import backups from '../../images/windowsserver/backups.png';
import certified from '../../images/windowsserver/certified.png';
import tick from '../../images/tick.png';
import 'react-tabs/style/react-tabs.css';
import { Hidden } from '@material-ui/core';
import ScrollAnimation from 'react-animate-on-scroll';
import "../../css/animate.min.css";
import './style.scss';

class WindowsServer extends Component {
    render() {
        return (
            <div className="backorder_domain windows_server">
                <ScrollAnimation animateIn='bounceInRight' animateOut='bounceOutLeft'>  
                    <h1 className="slider_text"> Let’s try UK’s fastest <br /><span className="slidertext_color"> Windows Server with<br />  full SSD storage!</span><br />

                    </h1>
                </ScrollAnimation>
                <div className="digital_marketing_image_section">
                    <img src={windowsserve} className="first_img" alt="navicosoft" loading="lazy" />
                </div>

                <div className="businessstationary">
                    <div className="creativity_section">  
                        <h2 className="sectthreee_heading">We are aimed to revolutionise <br /><span className="underline_heading">Web Hosting with our <br /> Elite Windows Server! </span></h2>
                        <div className="text_sectionn">
                            <p>Do you require hosting for your resource-intensive ASP.Net application? Are you still on the lookout for a hosting solution that is superior to Windows hosting? Navicosoft introduces the elite windows server to meet your requirements for Windows VPS. As a respected and cheap windows VPS hosting provider, you can be confident that you are in excellent hands with us. We've been evolving since 2008, serving millions of websites, demonstrating our skills and providing unmatched service standards to our clients. Our Windows server is designed to give you more control, fewer service limitations, and a slew of other capabilities.<br /><br />
                            It is possible to modify the resources on our cheap Windows VPS. You have the ability to scale the Windows Server2016 up and down according to your specific requirements. Do not be concerned if your hosting platform is running out of resources since now is the ideal moment to acquire the most reliable and cheap Windows VPS available everywhere. We are bringing you the Windows server that you have been waiting for since the beginning. From now on, you can benefit from dedicated resources, full root access, Windows KMS activation, and many more features for a modest price.</p>
                        </div>

                    </div>
                </div>

                <div className="smm_agency_section">
                    <div className="inner_section">
                        <div className="smm_agency_text">
                            <h2>Advantages of our <br />Windows Server</h2> 
                            <p>Try our Windows Server 2016 to get a physical server that is completely isolated<br />from the rest of the network. We provide you with lightning-fast Windows VPS, SSD storage, and full root access, allowing you to manage an infinite amount of traffic on your website. Our cheap Windows VPS with SSD storage is built to withstand the needs of<br /> Our SSD storage empowered cheap windows VPS are designed to meet the demands of the most <br /> the most demanding resources on the internet.
                            </p>
                        </div>
                        <Hidden mdDown>
                            <div className="smm_secondlast_section">
                                <div className="smm_img_text_section">
                                    <img src={pleskcp} alt="navicosoft" loading="lazy" />
                                    <div>
                                        <h3>Plesk Control Panel</h3>
                                        <p>With our Windows server, we provide the Plesk control panel, which is simple to use and maintain. Now you can administer your website from a single, easy-to-use control panel.</p>
                                    </div>
                                </div>
                                <div className="smm_img_text_section">
                                    <img src={fullycustomized} alt="navicosoft" loading="lazy" />
                                    <div>
                                        <h3>Fully Customized</h3>
                                        <p>We facilitate our clients with control over the customisation of Windows VPS. Now, assign the appropriate level of customisation to your website.</p>
                                    </div>
                                </div>
                            </div>

                            <div className="smm_secondlast_section">
                                <div className="smm_img_text_section">
                                    <img src={unlimitedbandwidth} alt="navicosoft" loading="lazy" />
                                    <div>
                                        <h3>Unlimited Bandwidth</h3>
                                        <p>With our cheap Windows VPS, you can take advantage of unlimited bandwidth and data traffic. We are here to undertake the heavy amount of traffic receiving by your website.</p>
                                    </div>
                                </div>
                                <div className="smm_img_text_section">
                                    <img src={vmware} alt="navicosoft" loading="lazy" />
                                    <div>
                                        <h3>VMware Virtualization</h3>
                                        <p>Our Windows kms is completely reliant on virtual machine virtualization. When compared to other virtualization technologies, it provides you with more isolation, autonomy, and security.</p>
                                    </div>
                                </div>
                            </div>

                            <div className="smm_secondlast_section">
                                <div className="smm_img_text_section">
                                    <img src={guranteeuptime} alt="navicosoft" loading="lazy" />
                                    <div>
                                        <h3>Guaranteed uptime</h3>
                                        <p>We guarantee 99.9% uptime to your website. Enjoy! The days of experiencing server downtime are over since we provide you with the ability to keep your website up and running at all times.</p>
                                    </div>
                                </div>
                                <div className="smm_img_text_section">
                                    <img src={firewallmanagement} alt="navicosoft" loading="lazy" />
                                    <div>
                                        <h3>Firewall Management</h3>
                                        <p>Our cheap Windows VPS server with Windows KMS has a firewall that is already configured and ready to use. Hackers and other security risks will be prevented from accessing your sensitive information.</p>
                                    </div>
                                </div>
                            </div>

                            <div className="smm_secondlast_section">
                                <div className="smm_img_text_section">
                                    <img src={sslenc} alt="navicosoft" loading="lazy" />
                                    <div>
                                        <h3>SSL Encryption</h3>
                                        <p>We make it possible for you to provide your customers with the best exposure possible for your website. As a result, all of our Windows servers come pre-configured with SSL.</p>
                                    </div>
                                </div>
                                <div className="smm_img_text_section">
                                    <img src={backups} alt="navicosoft" loading="lazy" />
                                    <div>
                                        <h3>Backups</h3>
                                        <p>We provide data backup service to reduce the likelihood of data loss. It is certain that you will not be disappointed with our industry-leading data backup solutions!</p>
                                    </div>
                                </div>
                            </div>

                            <div className="smm_secondlast_section">
                                <div className="smm_img_text_section">
                                    <img src={certified} alt="navicosoft" loading="lazy" />
                                    <div>
                                        <h3>Certified Infrastructure</h3>
                                        <p>Navicosoft has technically advanced data centres to provide you with secure and most up-to-date servers. We are ISO certified and guarantee the highest level of uptime.</p>
                                    </div>
                                </div>
                            </div>
                        </Hidden>
                        <Hidden lgUp>
                            <div>
                                <div className="smm_img_text_section smm_texttt">
                                    <img src={pleskcp} alt="navicosoft" loading="lazy" />
                                    <div>
                                        <h3>Plesk Control Panel</h3>
                                        <p>With our Windows server, we provide the Plesk control panel, which is simple to use and maintain. Now you can administer your website from a single, easy-to-use control panel.</p>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div className="smm_img_text_section smm_texttt">
                                    <img src={fullycustomized} alt="navicosoft" loading="lazy" />
                                    <div>
                                        <h3>Fully Customized</h3>
                                        <p>We facilitate our clients with control over the customisation of Windows VPS. Now, assign the appropriate level of customisation to your website.</p>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div className="smm_img_text_section smm_texttt">
                                    <img src={unlimitedbandwidth} alt="navicosoft" loading="lazy" />
                                    <div>
                                        <h3>Unlimited Bandwidth</h3>
                                        <p>With our cheap Windows VPS, you can take advantage of unlimited bandwidth and data traffic. We are here to undertake the heavy amount of traffic receiving by your website.</p>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div className="smm_img_text_section smm_texttt">
                                    <img src={vmware} alt="navicosoft" loading="lazy" />
                                    <div>
                                        <h3>VMware Virtualization</h3>
                                        <p>Our Windows kms is completely reliant on virtual machine virtualization. When compared to other virtualization technologies, it provides you with more isolation, autonomy, and security.</p>
                                    </div>
                                </div>
                            </div>
                            <div>

                                <div className="smm_img_text_section smm_texttt">
                                    <img src={guranteeuptime} alt="navicosoft" loading="lazy" />
                                    <div>
                                        <h3>Guaranteed uptime</h3>
                                        <p>We guarantee 99.9% uptime to your website. Enjoy! The days of experiencing server downtime are over since we provide you with the ability to keep your website up and running at all times.</p>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div className="smm_img_text_section smm_texttt">
                                    <img src={firewallmanagement} alt="navicosoft" loading="lazy" />
                                    <div>
                                        <h3>Firewall Management</h3>
                                        <p>Our cheap Windows VPS server with Windows KMS has a firewall that is already configured and ready to use. Hackers and other security risks will be prevented from accessing your sensitive information.</p>
                                    </div>
                                </div>
                            </div>
                            <div>


                                <div className="smm_img_text_section smm_texttt">
                                    <img src={sslenc} alt="navicosoft" loading="lazy" />
                                    <div>
                                        <h3>SSL Encryption</h3>
                                        <p>We make it possible for you to provide your customers with the best exposure possible for your website. As a result, all of our Windows servers come pre-configured with SSL.</p>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div className="smm_img_text_section smm_texttt">
                                    <img src={backups} alt="navicosoft" loading="lazy" />
                                    <div>
                                        <h3>Backups</h3>
                                        <p>We provide data backup service to reduce the likelihood of data loss. It is certain that you will not be disappointed with our industry-leading data backup solutions!</p>
                                    </div>
                                </div>
                            </div>
                            <div>


                                <div className="smm_img_text_section smm_texttt">
                                    <img src={certified} alt="navicosoft" loading="lazy" />
                                    <div>
                                        <h3>Certified Infrastructure</h3>
                                        <p>Navicosoft has technically advanced data centres to provide you with secure and most up-to-date servers. We are ISO certified and guarantee the highest level of uptime.</p>
                                    </div>
                                </div>
                            </div>
                        </Hidden>
                    </div>
                </div>

                <div className="backorder_services_why">
                    <h2 className="sectthreee_heading">Have a look at the  <br />use of our Windows Server!</h2>
                    <p className="text_ccenter_align">The limitless traffic feature of cheap windows VPS enables you to utilise it on a variety of platforms. The complete virtualisation <br /> of VMWare enables it to be utilised for a variety of applications. With our most secure windows server, protect your website from<br />hackers, spamming, phishing, and spoofing. Navicosoft pledges to be there for you along the process, from<br /> resolving storage problems to providing industry-leading backup solutions to facilitating data transfer and security.<br /> As a result, any technical problems you may have are going to be addressed once you retain our services. Now, you <br />can use our Windows server to do the following tasks:</p>
                    <div className="box_section">
                        <div className="smm_box_text">
                            <div className="smm_arrow _text">
                                <p>eCommerce stores</p>
                            </div>
                        </div>
                        <div className="smm_box_text">
                            <div className="smm_arrow _text">
                                <p>Educational & informational websites</p>
                            </div>
                        </div>
                        <div className="smm_box_text">
                            <div className="smm_arrow _text">
                                <p>WordPress websites</p>
                            </div>
                        </div>
                    </div>
                    <div className="box_section below_sec">
                        <div className="smm_box_text">
                            <div className="smm_arrow _text">
                                <p>Game Servers</p>
                            </div>
                        </div>
                        <div className="smm_box_text">
                            <div className="smm_arrow _text">
                                <p>Small corporate level businesses</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="web_development">
                    <div className="rightchoice_section">
                        <h2>Why Navicosoft For <br />Linux Windows Hosting in UK?</h2>
                        <div className="digitalmarketting_section">
                            <div className="tickimg1">
                                <img src={tick} alt="navicosoft" /> 
                                <p className="digitalmkt_txt">Our windows server <br />2016 is specifically <br />built to provide the<br /> highest degree of protection <br />and scalability possible.</p>
                            </div>
                            <div className="tickimg2">
                                <img src={tick} alt="navicosoft" />  
                                <p className="digitalmkt_txt">We don't just give you <br />a VPS box and send <br />you on your way; instead, <br />we offer completely <br />managed services.</p>
                            </div>
                            <div className="tickimg3">
                                <img src={tick} alt="navicosoft" />  
                                <p className="digitalmkt_txt">Our support<br />team is available for you<br />  24/7/365.</p>
                            </div>
                            <div className="tickimg3">
                                <img src={tick} alt="navicosoft" />    
                                <p className="digitalmkt_txt">We offer you with <br />the quickest and most <br />convenient method of <br />increasing CPU <br />resources in accordance <br />with your requirements.</p>
                            </div>
                            <div className="tickimg">
                                <img src={tick} alt="navicosoft" />
                                <p className="digitalmkt_txt">As an  <br />industry-leading <br />company, we let you have <br />a free three-day trial <br />period to evaluate <br />our services.</p>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        );
    }
}

export default WindowsServer;
