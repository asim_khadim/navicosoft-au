import React, { Component } from 'react';
import fb from '../../images/facebook/fb-banner.png';
import fbbuttonback from '../../images/facebook/fb-button-back.png';
import handimage from '../../images/facebook/hand-image.png';
import facebookaddesign from '../../images/facebook/facebookaddesign.png';
import userengagement from '../../images/facebook/userengagement.png';
import facebookadcopy from '../../images/facebook/facebookadcopy.png';
import facebookadao from '../../images/facebook/facebookadao.png';
import facebookrightimg from '../../images/facebook/facebookrightimg.png';
import ScrollAnimation from 'react-animate-on-scroll';
import "../../css/animate.min.css";
import 'react-tabs/style/react-tabs.css';
import { HashLink } from 'react-router-hash-link';

import './style.scss';
import Contactusform from '../ContactUsForm';

class Facebook extends Component {
    render() {
        return (
            <div className="google_ad facebook_ads">
                <ScrollAnimation animateIn='bounceInRight' animateOut='bounceOutLeft'> 
                    <h1 className="slider_text"><span className="slidertext_color">Connect Your Brand To </span><br />People Through Facebook<br />Advertising!<br />
                        <HashLink smooth to="#contact">
                        <button className="btn_digitalmarketting">
                        <a>Talk with our Facebook ads management experts</a>
                        </button>
                        </HashLink>
                    </h1>
                </ScrollAnimation>
                <div className="digital_marketing_image_section">
                    <img src={fb} className="first_img" alt="fb" loading="lazy" />
                </div>

                <div className="web_development">
                    <div className="development_solutions">
                        <h2>Create, Boost & <br /><span className="text_color">Popularise Your Brand with </span><br />Facebook Advertisement</h2>
                        <div className="text_dev">
                            <div className="text_section_left">
                                    <p>When it comes to promoting your brand and business in today's super connectable digital world, when everyone is active on social media platforms like Facebook, Instagram, Twitter, and others, Facebook advertising is unquestionably a great choice. As a digital marketing agency, we identify and hire extraordinary people who are also specialists in social media marketing. Facebook advertisements are not a service that can be set and forgotten, but effective Facebook advertising requires ongoing monitoring and management. We take care of everything; we use the most effective ad formats and methodologies at the most reasonable Facebook ad prices. Additionally, we make sure that your advertisements are shown to the right target audience. Our ultimate goal is to promote your brand and company on the world's most popular social media platform through Facebook advertisements.</p>                                
                            </div>

                        </div>
                    </div>
                </div>

                <div className="backgroundimg_regis" style={{ backgroundImage: `url(${fbbuttonback})` } } alt="fb"> 
                <h2 className="whitecolorhead">Ready to Discuss <br />Your Project & Next Step?</h2>
               
                    <HashLink smooth to="#contact">
                        <button className="btn_table">
                        <a>Contact Us</a>
                        </button>
                        </HashLink>
                </div>


                <div className="section_two">
                    <div className="secondtextsec">
                    <h2>Brace yourself to gain leads <br /><span className="text_color">from Facebook </span> Advertisement!</h2>
                            <p>In today's world, Facebook has established itself as the optimal location for promoting both your company and brand. Facebook manages advertising in the most beneficial manner possible, resulting in an immediate boost to your business. Therefore, let us assist your audience in capturing the attention of prospective customers by using us as your primary Facebook advertising agency.</p>
                        </div>
                    <div className="imgandtextsec">
                        <div className="lefttextsec">
                            <h2>Audience Targeting</h2>
                            <p>It makes no difference how engaging and entertaining your Facebook paid ads are unless they are placed in front of the appropriate audience. If you are not targeting the appropriate audience of your business niche, you are wasting both time and money. As one of the top-ranking digital marketing agencies, we make sure to create Facebook ads that are not only interactive but also effectively expose your business to the right demographic!</p>
                           
                        </div>
                        <div className="rightimgsec"><img src={handimage} className="righttext_img" alt="handimage" /></div>
                    </div>

                    <div className="imgandtextsec">
                        <div className="lefttextsec">
                            <h2>Facebook ads Design</h2>
                            <p>Unsure which ad format would work best for your business? We can help. As an experienced Facebook marketing agency, we are well aware of all techniques of Facebook ads to guarantee output. We will assist you from the creation of Facebook ads formats to generating the perfect impression on viewers using our successful Facebook ads strategies. We have a track record of creating the most effective Facebook ads possible.</p>
                           
                        </div>
                        <div className="rightimgsec"><img src={facebookaddesign} className="righttext_img" alt="facebookaddesign" /></div>
                    </div>

                    <div className="imgandtextsec">
                        <div className="lefttextsec">
                            <h2>User Engagement</h2>
                            <p>User engagement is essential for successful Facebook ads. Let's work together to keep your Facebook campaign from coming to an end with the top Facebook marketing agency in the UK. If you choose Navicosoft as your Facebook marketing agency, you will have the opportunity to make the best use of ads by enabling your customers to get more user interaction.</p>
                           
                        </div>
                        <div className="rightimgsec"><img src={userengagement} className="righttext_img" alt="userengagement" /></div>
                    </div>

                    <div className="imgandtextsec">
                        <div className="lefttextsec">
                            <h2>Facebook ad Copywriting</h2>
                            <p>Even though Facebook ad pictures speak all about your brand but at the same time, ads copy is an excellent tool to convey your message and attract the target audience. As your Facebook advertising partners, we assist you in developing engaging ad content that optimises your conversions. Additionally, we assist with developing such creatives that can capture your audience's attention by conveying your brand narrative and demonstrating your values.</p>
                           
                        </div>
                        <div className="rightimgsec"><img src={facebookadcopy} className="righttext_img" alt="facebookadcopy" /></div>
                    </div>

                    <div className="imgandtextsec">
                        <div className="lefttextsec">
                            <h2>Facebook ads <br />Analytics & Optimization</h2>
                            <p>Did you know the only way to track the insights of Facebook ads is through analytics reports? The measurement not only helps you understand about the goal's achievement but also informs you about the ads strategy. We after creating Facebook ads at affordable Facebook ads cost, closely track the conversions and do optimizations accordingly.</p>
                           
                        </div>
                        <div className="rightimgsec"><img src={facebookadao} className="righttext_img" alt="facebookad" /></div>
                    </div>

                </div>

                <div className="one_stop_background">
                <div className="dedicated_background_section">
                    <h2 className="sectthreee_heading mobile_stationary">Your Extraordinary <br /><span class="text_color">Facebook Advertising</span> <br />in UK</h2>
                    <div className="google_ad_service">
                        <div className="left_text">
                            <p className="para_background_linux">At Navicosoft, we have expertise in developing visually appealing Facebook advertisements at very reasonable Facebook advertising costs possible. But that's not all; we're also knowledgeable on how to create income for your eCommerce businesses. In order to help you grow your eCommerce company, we create customised query-based and contextual advertising campaigns. With us, you won't have to worry about losing money on your campaigns; in fact, you may expect to see an increase in ROI of more than 200 percent! Our expert services are constantly evolving to help you reach new heights in your company. In addition, we all know that the secret to effective Facebook advertising is to consider from the perspective of your target audience. Keeping all of these considerations in mind, we always provide the most relevant advertising services and ad designs.</p>
                            </div>
                        <div className="right_image">
                            <img src={facebookrightimg} className="first_img" alt="facebook" loading="lazy" />
                        </div>
                    </div>
                   
                </div>
                </div>
                <div id="contact">
                <Contactusform page_name='Facebook' />
                </div>
                

                
            </div>
        );
    }
}

export default Facebook;
