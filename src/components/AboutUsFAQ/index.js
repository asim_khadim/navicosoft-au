import React, { Component } from 'react';

import './style.scss';

class AboutUsFAQ extends Component {
    render() {
        return (
            <div className="digitalmarkettingfaq">
                <h2>FAQ's</h2>
                <div>
                    <h3>What is shared hosting?</h3>
                    <p>Shared hosting plans are one of the most affordable hosting solutions. In shared hosting, a single server is shared among various users. It is a great fit for websites with average traffic rates. Navicosoft offers cheap website hosting solutions with secured servers. We offer various hosting plans so that you could choose according to your requirements. </p>
                </div>
                <div>
                    <h3>How can I get started with Navicosoft in Australia?</h3>
                    <p>We offer the quickest method to get started with us—all you have to follow a few steps without any hassle. Go to the website and create a user account. Choose the type of service you want to avail with the right package. Click on the purchase or open a ticket through your client area. Your desired services will get activated within few minutes.</p>
                </div>
                <div>
                    <h3>Can I transfer my existing cheap website hosting to Navicosoft in Australia?</h3>
                    <p>Well, Navicosoft provides the easiest way to transfer your hosting services to us. You don't have to do any hassle. We bring the easiest & quickest way to transfer your hosting as well as other services. All you have to do is select the suitable package of services and let us know to activate it for you. We shall provide you with free backups and website migrations for sure.</p>
                </div>
                <div>
                    <h3>Do you provide a money-back guarantee?</h3>
                    <p>Navicosoft is here with the aim to serve, not to mere earn. We come up with several iterations of work until you are fully satisfied. Furthermore, we provide a free trial of three days to let you check the quality of our services. But still, if you don't feel satisfied, we ensure to provide you a full money-back guarantee!</p>
                </div>
                <div>
                    <h3>Can I get cheap website hosting from Navicosoft in Australia?</h3>
                    <p>Getting cheap website hosting has now become so easy in Australia. We bring cheap website hosting packages that can set up right according to your hosting needs. Furthermore, if you are not sure about your hosting needs, then you can get a free trial of three days. So don’t worry about all those expensive hosting packages because we have brought cheap website hosting packages with vast space and bandwidth.</p>
                </div>
                <div>
                    <h3>How can I pay Navicosoft?</h3>
                    <p>Well, we offer various payment options. It all depends on you, which options you want to go with. When you get our services, we provide you the ease to pay accordingly. Our available payment options are written as below:</p>
                    <ul className="tick_faq_css">
                        <li>Credit card/Debit card</li>
                        <li>BTC</li>
                        <li>Bank transfer</li>
                    </ul>
                </div>


            </div>
        );
    }
}

export default AboutUsFAQ;
