import React, { Component } from 'react';
import google from '../../images/google.png';
import smmbackgroundimg from '../../images/smmbackgroundimg.png';
import facebook from '../../images/facebook.png';
import linkedin from '../../images/linkedin.png';
import instagram from '../../images/instagram.png';
import youtube from '../../images/youtube.png';
import smmimg1 from '../../images/smmimg1.png';
import smmimg2 from '../../images/smmimg2.png';
import smmimg3 from '../../images/smmimg3.png';
import smmimg4 from '../../images/smmimg4.png';
import smmimg5 from '../../images/smmimg5.png';
import smmpricingstrategy from '../../images/smmpricingstrategy.png';
import 'react-tabs/style/react-tabs.css';
import Grid from '@material-ui/core/Grid';
import ScrollAnimation from 'react-animate-on-scroll';
import "../../css/animate.min.css";
import { Hidden } from '@material-ui/core';
import { Link } from "react-router-dom"

import './style.scss';

class SocialMediaMarketing extends Component {
    render() {
        return (
            <div className="container_dm social_media_marketting">
                <ScrollAnimation animateIn='bounceInRight' animateOut='bounceOutLeft'>
                    <h1 className="slider_text ">We Provide <span className="slidertext_color">Grow your business <br /> with our social media marketing services.</span> <br />
                        <Link to="contact-us"><button className="btn_digitalmarketting" ><a>Work WIth Us</a></button> </Link>
                    </h1>
                </ScrollAnimation>

                <div className="smm_background_section" style={{ backgroundImage: `url(${smmbackgroundimg})` }} alt="navicosoft">
                    <h2>Most Effective Social <br />Media Marketing Strategies<br /> For Your Business!</h2>
                    <p>We are driven to assist businesses in growing their social media<br /> presence via the most effective social media marketing <br />advanced strategies. Navicosoft is ready to offer Social <br />Media Management solutions that are tailored <br />to your company's specific needs!
                    </p>
                </div>
                <div className="smm_platform_section">
                    <h2><span className="heading_color">Let's us Target</span> your audience with all <br /> social media platforms</h2>
                    <p>We are offering you Social Media Marketing Services for:</p>
                    <ScrollAnimation animateIn='bounceInRight' animateOnce={true}>
                        <Grid container className="smm_google">
                            <Grid item={true} xs={12} sm={6}><img src={google} alt="navicosoft" loading="lazy" /></Grid>
                            <Grid item={true} xs={12} sm={6} className="google_text_sec">
                                <h3>Google</h3>
                                <p>Google ads will help you build your company's reputation! With us, you can now advertise your business on the world's biggest and most trusted social media network.</p>

                                <Link to="/google-ad"><button className="btn_digitalmarketting" ><a>Learn More</a></button> </Link>
                            </Grid>
                        </Grid>
                    </ScrollAnimation>
                    <ScrollAnimation animateIn='bounceInLeft' animateOnce={true}>
                        <Grid container className="smm_facebook">
                            <Grid item={true} xs={12} sm={6} className="facebook_text_sec">
                                <h3>Facebook</h3>
                                <p>With the use of advanced artificial intelligence methods and innovative marketing strategies, Facebook will enable you to expand your local audience! If you are experiencing problems with your Facebook page, you may delegate the problem to our team. We will contact the developers and work to fix the issues since we are one of the approved Facebook agencies!</p>
                                <Link to="/facebook-ad"><button className="btn_digitalmarketting" ><a>Learn More</a></button> </Link>
                            </Grid>
                            <Grid item={true} className="image_center" xs={12} sm={6}><img src={facebook} alt="navicosoft" loading="lazy" /></Grid>
                        </Grid>
                    </ScrollAnimation>
                    <ScrollAnimation animateIn='bounceInRight' animateOnce={true}>
                        <Grid container className="smm_google">
                            <Grid item={true} className="linkedin_center" xs={12} sm={6}><img src={linkedin} alt="navicosoft" loading="lazy" /></Grid>
                            <Grid item={true} xs={12} sm={6} className="google_text_sec">
                                <h3>Linkedin</h3>
                                <p>It is not only about recruiting people; it is also about promoting your brand. Social media marketing through Linkedin will expand opportunities for B2B and B2C sales and leads significantly.</p>
                                <button className="btn_digitalmarketting"><a href="#">Start marketing</a></button>
                            </Grid>
                        </Grid>
                    </ScrollAnimation>
                    <ScrollAnimation animateIn='bounceInLeft' animateOnce={true}>
                        <Grid container className="smm_facebook">
                            <Grid item={true} xs={12} sm={6} className="facebook_text_sec">
                                <h3>Instagram</h3>
                                <p>Who doesn't like showcasing their brand's goods and services on Instagram IGTV? However, we do it in an Instagram-friendly manner by gaining followers and using this app as a virtual source for providing product information!
                                </p>
                                <button className="btn_digitalmarketting"><a href="#">Let us begin</a></button>
                            </Grid>
                            <Grid item={true} className="image_center" xs={12} sm={6}><img src={instagram} alt="navicosoft" loading="lazy" /></Grid>
                        </Grid>
                    </ScrollAnimation>
                    <ScrollAnimation animateIn='bounceInRight' animateOnce={true}>
                        <Grid container className="smm_google">
                            <Grid item={true} className="linkedin_center" xs={12} sm={6}><img src={linkedin} alt="navicosoft" loading="lazy" /></Grid>
                            <Grid item={true} xs={12} sm={6} className="twitter_text_sec">
                                <h3>Twitter</h3>
                                <p>Twitter is a highly genuine social media network, and it enables you to connect with your customers and share your business narrative. We craft Twitter social media marketing plans in such a manner that your business receives all of the due attention from your customer base! Additionally, a Twitter ad manager can assist you in acquiring genuine consumers!</p>
                                <button className="btn_digitalmarketting"><a href="#">Start marketing</a></button>
                            </Grid>
                        </Grid>
                    </ScrollAnimation>
                    <ScrollAnimation animateIn='bounceInLeft' animateOnce={true}>
                        <Grid container className="smm_facebook">
                            <Grid item={true} xs={12} sm={6} className="facebook_text_sec">
                                <h3>Youtube</h3>
                                <p>Let us work together to obtain the Diamond play button! We are certain that many individuals desire to improve their Youtube channels, and we are here to assist you. Advertisements on Youtube enable you to market your products to consumers who view the video!  And, after all, who doesn't?</p>
                                <button className="btn_digitalmarketting"><a href="#">Let us begin</a></button>
                            </Grid>
                            <Grid item={true} className="image_center" xs={12} sm={6}><img src={youtube} alt="navicosoft" loading="lazy" /></Grid>
                        </Grid>
                    </ScrollAnimation>
                </div>

                <div className="dark_section_background">
                <div className="smm_dark_section">
                    <div className="smm_first_row">
                        <h2>Market Competitive <br /> Social Media Marketing Planning</h2>
                        <p>With the changing environment of the industry, there is an increased demand for a most authentic and popular platform capable of reaching a large customer base. Through media consumption, digital platforms, and audience fragmentation, Navicosoft offers possible solutions for your audience. We value diversity in our digital marketing tactics to ensure your brand's social media marketing is effective and results-driven.</p>
                    </div>
                    <hr />
                    <div className="smm_smam">
                        <h2>Social Media <br /> Agency London</h2>
                        <p>We realise that marketing via a traditional advertising agency is a big issue! Our client requires a massive impact on their consumers through social media while saving time and money and achieving significant results. Navicosoft is a well-known social media marketing agency with the capability of reaching the broadest possible audience for our customers in various business sectors. The concept of effective online marketing is very rare, and delivering superior service while keeping the SMM price plan in mind is also critical. Therefore, we provide our clients with one of the most economical yet advanced and effective social media marketing solutions.</p>
                    </div>
                </div>
                </div>
                
                <div className="self_sufficent_smm">
                    <h2 className="sectthreee_heading">Self Sufficient Social <br /> Media <span className="underline_heading">Marketing Strategies </span></h2>
                    <p>At Navicosoft, we provide the utmost self-sustaining online marketing methods to channelise the importance of your company in<br /> front of our consumers. We place a high emphasis on objectivity and use the most effective SSM strategies for our customers.</p>
                    <p className="bold_text">Pick Us for your social media marketing Because:</p>
                    <div className="smm_box_section">
                        <ScrollAnimation animateIn='bounceInRight' animateOnce={true} delay={1000}>
                            <div className="smm_box_text">
                                <div className="smm_arrow _text">
                                    <i className="fa fa-long-arrow-right"></i>
                                    <p>Market Competitive <br /> SMM Pricing Strategy</p>
                                </div>
                            </div>
                        </ScrollAnimation>
                        <ScrollAnimation animateIn='bounceInRight' animateOnce={true} delay={1500}>
                            <div className="smm_box_text">
                                <div className="smm_arrow _text">
                                    <i className="fa fa-long-arrow-right"></i>
                                    <p>Create your digital <br /> Marketing Package</p>
                                </div>
                            </div>
                        </ScrollAnimation>
                        <ScrollAnimation animateIn='bounceInRight' animateOnce={true} delay={2500}>
                            <div className="smm_box_text">
                                <div className="smm_arrow _text">
                                    <i className="fa fa-long-arrow-right"></i>
                                    <p>Analysis and <br /> Campaign Building</p>
                                </div>
                            </div>
                        </ScrollAnimation>
                    </div>
                    <div className="smm_box_section">
                        <ScrollAnimation animateIn='bounceInRight' animateOnce={true} delay={3000}>
                            <div className="smm_box_text">
                                <div className="smm_arrow _text">
                                    <i className="fa fa-long-arrow-right"></i>
                                    <p>Run Ads on <br />any Social Media<br /> Platform </p>
                                </div>
                            </div>
                        </ScrollAnimation>
                        <ScrollAnimation animateIn='bounceInRight' animateOnce={true} delay={3500}>
                            <div className="smm_box_text">
                                <div className="smm_arrow _text">
                                    <i className="fa fa-long-arrow-right"></i>
                                    <p>Target Different <br />Audiences on Several<br />Social Media Platforms. </p>
                                </div>
                            </div>
                        </ScrollAnimation>
                        <ScrollAnimation animateIn='bounceInRight' animateOnce={true} delay={4000}>
                            <div className="smm_box_text">
                                <div className="smm_arrow _text">
                                    <i className="fa fa-long-arrow-right"></i>
                                    <p>Decades of experience <br />with all types of brands<br />around the globe</p>
                                </div>
                            </div>
                        </ScrollAnimation>
                    </div>
                </div>
                <div className="smm_agency_section">
                    <div className="inner_section">
                        <div className="smm_agency_text">
                            <h2>Social media marketing <br /> Agency of London</h2>
                            <p>Every plan we complete and every effort we make is built on a strategy. Delivering successful <br />projects all over the globe is our legacy, which is why we are turning out to be the most reliable <br />and competent social media company in London.
                            </p>
                        </div>
                        <Hidden mdDown>
                            <div className="smm_secondlast_section">
                                <div className="smm_img_text_section">
                                    <img src={smmimg1} alt="navicosoft" loading="lazy" />
                                    <div>
                                        <h3>Focused Social <br /> Media Management Strategy</h3>
                                        <p>Making sure that your plan will be successful is an essential consideration. Navicosoft will make certain that all of our efforts are targeted and result in value for our clients.</p>
                                    </div>
                                </div>
                                <div className="smm_img_text_section">
                                    <img src={smmimg2} alt="navicosoft" loading="lazy" />
                                    <div>
                                        <h3>Analysis of Social <br /> Media Requirements</h3>
                                        <p>We are focused with delivering the best solutions possible to our customers. We are unconcerned about what other social media marketing companies have to offer, and instead focus on providing our best to our clients in the most cost-effective manner possible.</p>
                                    </div>
                                </div>
                            </div>

                            <div className="smm_secondlast_section">
                                <div className="smm_img_text_section">
                                    <img src={smmimg3} alt="navicosoft" loading="lazy" />
                                    <div>
                                        <h3>Constant Efforts</h3>
                                        <p>Social media strategies are always evolving, and we are committed to designing your digital marketing strategy in a way that is both effective and diverse.</p>
                                    </div>
                                </div>
                                <div className="smm_img_text_section">
                                    <img src={smmimg4} alt="navicosoft" loading="lazy" />
                                    <div>
                                        <h3>Audience Formation</h3>
                                        <p>To our way of thinking, a well-targeted audience is the core of every successful campaign. Create a distinct audience for your business endeavour by contacting us now.</p>
                                    </div>
                                </div>
                            </div>

                            <div className="smm_secondlast_section">
                                <div className="smm_img_text_section">
                                    <img src={smmimg5} alt="navicosoft" loading="lazy" />
                                    <div>
                                        <h3>Decades of Experience</h3>
                                        <p>Navicosoft has accumulated decades of expertise from a variety of diverse locations. Making your business endeavour a success is just a few clicks away!</p>
                                    </div>
                                </div>
                            </div>
                        </Hidden>
                        <Hidden lgUp>
                            <div>
                                <div className="smm_img_text_section smm_texttt">
                                    <img src={smmimg1} alt="navicosoft" loading="lazy" />
                                    <div>
                                        <h3>Focused Social <br /> media management Strategy</h3>
                                        <p>Making sure that your plan will be successful is an essential consideration. Navicosoft will make certain that all of our efforts are targeted and result in value for our clients.</p>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div className="smm_img_text_section smm_texttt">
                                    <img src={smmimg2} alt="navicosoft" loading="lazy" />
                                    <div>
                                        <h3>Analysis of Social <br /> media Requirements</h3>
                                        <p>We are focused with delivering the best solutions possible to our customers. We are unconcerned about what other social media marketing companies have to offer, and instead focus on providing our best to our clients in the most cost-effective manner possible.</p>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div className="smm_img_text_section smm_texttt">
                                    <img src={smmimg3} alt="navicosoft" loading="lazy" />
                                    <div>
                                        <h3>Constant Efforts</h3>
                                        <p>Social media strategies are always evolving, and we are committed to designing your digital marketing strategy in a way that is both effective and diverse.</p>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div className="smm_img_text_section smm_texttt">
                                    <img src={smmimg4} alt="navicosoft" loading="lazy" />
                                    <div>
                                        <h3>Audience Formation</h3>
                                        <p>To our way of thinking, a well-targeted audience is the core of every successful campaign. Create a distinct audience for your business endeavour by contacting us now.</p>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div className="smm_img_text_section smm_texttt">
                                    <img src={smmimg5} alt="navicosoft" loading="lazy" />
                                    <div>
                                        <h3>Decades of Experience</h3>
                                        <p>Navicosoft has accumulated decades of expertise from a variety of diverse locations. Making your business endeavour a success is just a few clicks away!</p>
                                    </div>
                                </div>
                            </div>
                        </Hidden>
                    </div>
                </div>

                <ScrollAnimation animateIn='bounceInDown' animateOnce={true}>
                    <div className="smm_pricing_strategy_section">
                        <div className="smm_pricing_strategy">
                            <img src={smmpricingstrategy} alt="navicosoft" />
                        </div>
                        <div className="smm_pricing_strategy_text">
                            <h2>SMM Pricing Strategy</h2>
                            <p>Our goal is to provide an affordable social media marketing approach that incorporates sophisticated campaign planning. We aim to provide social media programmes that are completely easy and result-oriented, as well as a pricing approach that is beneficial for SMM. Our SMM team will actively handle the sponsored campaign we're planning for our clients. There is a mixture of audit and implementation services across all social media platforms, as well as helpful creatives in the form of infographics, pictures, carousels, advertisements, collages, and videos. Navicosoft's social media management is similar to co-creating new buzzwords.</p>
                        </div>
                    </div>
                </ScrollAnimation>
            </div>
        );
    }
} export default SocialMediaMarketing;