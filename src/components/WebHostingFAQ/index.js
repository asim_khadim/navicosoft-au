import React, { Component } from 'react';

import './style.scss';

class WebHostingFAQ extends Component {
    render() {
        return (
            <div className="digitalmarkettingfaq">
                <h2>Having Questions? Get Our Answers!</h2>
                <div>
                    <h3>Why should I select Navicosoft for web hosting in the United Kingdom?</h3>
                    <p>With over 17 years of expertise in web hosting, Navicosoft enables you to develop and launch your website on high-performance servers. Our reliable client service distinguishes us from our competitors and allows us to dominate the industry. Our technical assistance is available 24/7, 365 days a year. We take pleasure in being UK’s only web hosting firm, providing managed and unmanaged website hosting services staffed by the industry's finest hosting experts with years of expertise.</p>
                </div>
                <div>
                    <h3>How does website hosting work?</h3>
                    <p>Web hosting is the process of renting a plot of land or a particular amount of server space in order to have an online presence. Buying web hosting entails purchasing resources on a server in order to establish your online presence and launch your website. Our web hosting services include not only the finest technological characteristics, but also round-the-clock technical assistance.</p>
                </div>
                <div>
                    <h3>Is web hosting really necessary?</h3>
                    <p>As is well known, that website is built utilising a variety of computer languages and tools, databases, and scripts. Once the website is developed, it must be made available to the public for browsing and viewing. This is where web hosting plays a vital role.<br />
                    As stated before, web hosting is the act of renting server space in one or more data centres that are linked to other data centres situated around the globe. A web server maintains your website's files and data, and when a user does a relevant search using various search engines, the server responds with the necessary data.<br />
                    A significant advantage of our web hosting is our widespread use of SSD RAM. Solid State Drives are the newest form of computer memory, which enables our server to run very quickly.<br />
                    We are pleased to provide helpful technical services at Navicosoft. Our web hosting in the United Kingdom is always intended to be as environmentally friendly as possible.
                    </p>
                </div>
                <div>
                    <h3>Is it possible to upgrade my plan later?</h3>
                    <p>Yes, if your website does not now have enough traffic but has the potential to grow in the near future, you may increase your plan at any moment, based on your needs. Our plans offer you the freedom to continue developing and growing your website as much as you need to.<br /><br />
                    You may request improvements at any time. Simply submit a support request or call our support staff; they will assist you along the route. Your data is secure, and the upgrade procedure has no effect on your website. In a matter of minutes, your web hosting account will be extended according to your specification
                    </p>
                </div>
                <div>
                    <h3>Is there free domain registration with website hosting?</h3>
                    <p>When you sign up for a minimum of one year of hosting with Navicosoft, you get a free domain registration. With all yearly web hosting services, we provide a free domain registration for the first year. However, if you terminate your hosting account, a non-refundable domain cost will be taken from the yearly hosting subscription you paid to Navicosoft.</p>
                </div>
                <div>
                    <h3>Which web hosting package should I buy?</h3>
                    <p>Our web hosting services provide a robust feature set that caters to everyone from beginners to professionals to webmasters and everyone in between. You just need to choose a plan that meets your technical specifications requirements. Additionally, you may always contact our support staff for assistance in selecting a web hosting package. Our experienced support team is available 24/7 to assist you throughout the process.</p>
                </div>
                <div>
                    <h3>Can you help me transfer my current web hosting provider?</h3>
                    <p>Ofcourse Yes, Our technical experts will oversee the whole procedure, so you won't need to worry if you're not technically inclined. The greatest thing is that we take care of the whole setup for free. You just need to ensure that you have not terminated your current account without first speaking with our support staff. Once you've completed the Navicosoft purchase procedure, log into your client area and submit a support request with information about your existing web hosting account's state and condition, and our staff will assist you.</p>
                </div>
                <div>
                    <h3>Can I use your web hosting plans to host multiple domains and websites?</h3>
                    <p>Yes, our cheap web hosting plans enable you to host an infinite number of domains and websites. You may host up to two websites with our basic plan, which is ideal for new startup companies and beginners looking to host their website on a fast server at an inexpensive price. You may always upgrade or decrease your plan in accordance with your preferences and requirements.</p>
                </div>
                <div>
                    <h3>What is the significance of domain hosting and web hosting in the United Kingdom?</h3>
                    <p>You should take note of the host's actual location - failing to do so may result in future performance degradation. By using UK's web hosting options, you may increase the performance of your website, and we are here to assist you.
                        The datacenter location ensures that the data you send/receive does not rely on foreign infrastructure or must travel a great distance. As such, it is critical in ensuring that the performance is flawless.

                    </p>
                </div>

            </div>
        );
    }
}

export default WebHostingFAQ;
