import React, { Component } from 'react';
import pic from '../../images/pic.png';
import emailmarkettingsolution from '../../images/emailmarkettingsolution/emailmarkettingsolution.png';
import anitipisshing from '../../images/emailmarkettingsolution/anitipisshing.png';
import instantsetup from '../../images/emailmarkettingsolution/instantsetup.png';
import antispoofing from '../../images/emailmarkettingsolution/antispoofing.png';
import ddosprotection from '../../images/emailmarkettingsolution/ddosprotection.png';
import virusfree from '../../images/emailmarkettingsolution/virusfree.png';
import neelampower from '../../images/neelampower.png';
import designergy from '../../images/designergy.png';
import sadi from '../../images/sadi.png';
import design from '../../images/design.png';
import devep from '../../images/devep.png';
import strategy from '../../images/strategy.png';
import deployment from '../../images/deployment.png';
import 'react-tabs/style/react-tabs.css';
import Grid from '@material-ui/core/Grid';
import ScrollAnimation from 'react-animate-on-scroll';
import "../../css/animate.min.css";
import { Hidden } from '@material-ui/core';
import './style.scss';
import Products from '../Products';
import { Link } from "react-router-dom";

class BusinessEmailSolution extends Component {
    render() {
        return (
            <div className="businessemailsolution">
                <ScrollAnimation animateIn='bounceInRight' animateOut='bounceOutLeft'>
                    <h1 className="slider_text">We Bring the Professional <br /><span className="slidertext_color">Business Email With the</span><br /> Most Requested Features!<br />

                        <Link to="contact-us"><button className="btn_digitalmarketting" ><a>Work WIth Us</a></button> </Link>
                    </h1>
                </ScrollAnimation>
                <div className="digital_marketing_image_section">
                    <img src={emailmarkettingsolution} className="first_img" alt="Email Marketting Solution" loading="lazy" />
                </div>

                <div className="dedicated_hosting linuxhosting ">
                    <div className="purchase table_pricing">
                        <div className="container">
                            <h2 className="web_hosting_h2">Our<span> Business Email Solution </span>Packages</h2>
                            <ul >
                                <Products
                                    group_id={155}
                                    imgsrc={pic}
                                    perprice={'mo'}
                                    action={'dedicatedHostingPlans'}
                                />
                            </ul>

                        </div>
                    </div>
                </div>

                <div className="pro_email_section_background">
                    <div className="pro_email_section"> 
                        <h2>Let’s represent yourself professionally <br /><span className="yellow_text">to the world </span> with a business email.</h2>
                        <p>Interested in creating a lasting impact on clients by being a professional business entity? Create a professional email address that corresponds to your business and domain name to get things started. Navicosoft provides cost-effective methods to enhance your brand presentation via professional business email addresses. Using the lightning-fast secure email hosting networks, you can now advertise your company's brand while also checking your accounts from any location you want. In addition, our brilliant Google business email services will help you build your online reputation since we offer the following features.</p>
                        <div className="pro_blocks">
                            <div className="blox_pro">Spam & virus protection</div>
                            <div className="blox_pro">Easiet setup with any domain name</div>
                            <div className="blox_pro">Access from anywhere & everywhere</div>
                        </div>
                    </div>
                </div>


                <div className="section_two">
                    <div className="secondtextsec">
                        <h2>Get Advanced Features <br /><span className="purple_color">With Your Business Email!</span></h2>
                        <p>Navicosoft enables you to run your business email on the most up-to-date SSD servers, resulting in the fastest and most reliable email delivery possible. You can log in to your business email accounts from any computer or mobile device, at any time and from any location across the world. We allow you to remain in touch with your prospects at any time of day or night, with no restrictions or limitations!</p>
                    </div>
                    <div className="imgandtextsec">
                        <div className="lefttextsec">
                            <h2>Anti-phishing Solution</h2>
                            <p>With our corporate email solutions, you may get an anti-spam service as well as a secure firewall. We offer spam prevention solutions that are effective against all phishing techniques. Because we safeguard your sensitive information from being phished, you can relax knowing that you are in good hands.</p>

                        </div>
                        <div className="rightimgsec"><img src={anitipisshing} className="righttext_img" alt="Anti Pishing Solution" /></div>
                    </div>

                    <div className="imgandtextsec">
                        <div className="lefttextsec">
                            <h2>Instant Setup</h2>
                            <p>: Once you have pointed your MX records to our safe and premium email services, all else is handled by our lightning-fast devices and servers. All that remains is for you to do now is relax, take a deep breath, and take pleasure in a worry-free and spam-free corporate email service atmosphere!</p>

                        </div>
                        <div className="rightimgsec"><img src={instantsetup} className="righttext_img" alt="instantsetup" /></div>
                    </div>

                    <div className="imgandtextsec">
                        <div className="lefttextsec">
                            <h2>Anti-spoofing</h2>
                            <p>Navicosoft’s premium email solutions include the industry's most advanced anti-spoofing technology. With Navicosoft as your email provider, you can be confident that faked sender addresses will not be sent to you. This feature enables you to define a list of IP addresses permitted to have an internal address.</p>

                        </div>
                        <div className="rightimgsec"><img src={antispoofing} className="righttext_img" alt="antispoofing" /></div>
                    </div>

                    <div className="imgandtextsec">
                        <div className="lefttextsec">
                            <h2>DDoS Protection</h2>
                            <p>Our premium email security services are intended to protect you against spammers and hackers that attempt to overload your email server or steal your identity or data. Our servers provide comprehensive DDoS protection, preventing your network from being disabled or having its efficacy diminished.</p>

                        </div>
                        <div className="rightimgsec"><img src={ddosprotection} className="righttext_img" alt="ddos protection" /></div>
                    </div>

                    <div className="imgandtextsec">
                        <div className="lefttextsec">
                            <h2>Virus-free & Safe Emails</h2>
                            <p>Our email hosting services include complimentary anti-spam and anti-virus security. We would never put your identity or online company in danger, which is why we ensure that anytime a virus attempts to enter an email, it is immediately stopped by anti-spam and virus protection software. Additionally, we provide a spam prevention system that offers complete filter management and real-time malware detection for your business emails. <br /><br />
                                Today, email is the most-trusted method of communication for companies. Unfortunately, all spams, viruses, security risks, and malware are significant hazards to company productivity and downtime. All it takes is a sophisticated layer of security to ensure you have a complete and up-to-date solution against spam and malware.
                                </p>

                        </div>
                        <div className="rightimgsec"><img src={virusfree} className="righttext_img" alt="virus free safe email" /></div>
                    </div>

                </div>

                <div className="backorder_flow">
                    <h2 className="sectthreee_heading">Changing Email Server For <br /> <span className="yellow_color">Business Email</span> Has Never Been This Simple!</h2>
                    <p>Allow us to simplify the process. Navicosoft assists you in configuring and migrating your email address in the most cost effective way. We follow these simple steps:</p>
                    <div className="flow_order">
                        <div className="search_order">
                            <div className="circle_search">
                                <p>Step</p>
                                <h2>01</h2>

                            </div>
                            <p className="paragraph_style">We begin by copying your whole inbox, including all emails, contacts, tasks, and calendars.</p>
                        </div>

                        <div className="search_order">
                            <div className="circle_search">
                                <p>Step</p>
                                <h2>02</h2>
                            </div>

                            <p className="paragraph_style">We complete the migration procedure in a single go, allowing you to continue operating your company.</p>
                        </div>

                        <div className="search_order">
                            <div className="circle_search">
                                <p>Step</p>
                                <h2>03</h2>
                            </div>

                            <p className="paragraph_style">With no extra downtime, you'll finally have full access to all your emails, contacts, and tasks.</p>
                        </div>

                    </div>

                </div>


                <div className="secure_business_email">
                    <h2 className="sectthreee_heading">Why Navicosoft For <br /> <span className="yellow_color">Secure Business Email </span>Services?</h2>
                    <div className="secure_full">
                        <div className="secure_rows">
                            <p>We are available with 99.9% uptime and minimum downtime.</p>
                        </div>
                        <div className="secure_rows">
                            <p>Navicosoft enables you to do business from any location and on any device.</p>
                        </div>
                        <div className="secure_rows">
                            <p>We provide easy-to-use interface for managing your business emails.</p>
                        </div>
                        <div className="secure_rows">
                            <p>Our support team is available 24/7 to assist you with any security or technical problems.</p>
                        </div>
                        <div className="secure_rows">
                            <p>We keep track of security risks in real time.</p>
                        </div>
                    </div>
                </div>

            </div>

        );
    }


}

export default BusinessEmailSolution;