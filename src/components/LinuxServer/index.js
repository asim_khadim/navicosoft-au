import React, { Component } from 'react';
import linuxserver from '../../images/linuxserver.png';
import 'react-tabs/style/react-tabs.css';
import rootacess from '../../images/rootacess.png';
import linuxcustomization from '../../images/linuxcustomization.png';
import linuxsecurity from '../../images/linuxsecurity.png';
import regularupdate from '../../images/regularupdate.png';
import highscalibility from '../../images/highscalibility.png';
import oneclickisnt from '../../images/oneclickisnt.png';
import controlpanel from '../../images/controlpanel.png';
import metallinux from '../../images/metallinux.png';
import guranteuptime from '../../images/guranteuptime.png';
import freeupgratation from '../../images/freeupgratation.png';
import fullprotetctionnetwork from '../../images/fullprotetctionnetwork.png';
import unlimitedbandthwidth from '../../images/unlimitedbandthwidth.png';
import tick from '../../images/tick.png';
import dedicatedtechsupport from '../../images/dedicatedtechsupport.png';
import continousmonitoring from '../../images/continousmonitoring.png';
import whynavicosectionimg3 from '../../images/whynavicosectionimg3.png';
import Grid from '@material-ui/core/Grid';
import { Hidden } from '@material-ui/core';
import ScrollAnimation from 'react-animate-on-scroll';
import "../../css/animate.min.css";
import './style.scss';
import Products from '../Products';
import pic from '../../images/pic.png';
import { HashLink } from 'react-router-hash-link';

class LinuxServer extends Component {
    render() {
        return (
            <div className="dedicated_hosting linux_server">
                <ScrollAnimation animateIn='bounceInRight' animateOut='bounceOutLeft'>
                    <h1 className="slider_text">Step into the technical world<br /><span className="slidertext_color"> with our Supreme Linux<br /> Server Hosting!</span><br />
                        <HashLink smooth to="#buy-now">
                            <button className="btn_digitalmarketting">
                                <a>Get Started</a>
                            </button>
                        </HashLink>
                    </h1>
                </ScrollAnimation>
                <div className="dedicated_background_section" style={{ backgroundImage: `url(${linuxserver})` }} alt="Navicosoft Linux Server">
                
                    <h2 className="background_header_text">New Linux Hosting <br /> Services in London</h2>
                    <p className="para_background_linux">Are you upset with the slow speed of your website and wish to upgrade to a more secure and faster hosting solution? Don't panic, since Navicosoft provides the most reliable Linux platform to manage the high traffic to your website. We are based in London, where you can obtain cheap and the best-dedicated hosting with an ultra-modern, reliable, and secure Ubuntu server. Navicosoft provides the solution to your website problems. We provide your desired speed, scalability and guarantee to offer you our cutting-edge Linux web server, which has the advantages of leveraging your company's growth while also providing root-level access!<br /><br />
                    As one of the UK's leading web hosting companies, we have a client base that belongs to various industries, including information technology, supply chain management, education, medicine, and manufacturing. So take advantage of our dedicated Linux server and allow us to sort through the complexities of your company and replace them with the finest solutions available. In a nutshell, we offer everything to help you grow in the digital world.</p>

                </div>
                <div className="purchase table_pricing" id="buy-now">
                    <div className="container">
                        <h2 className="web_hosting_h2"><span>Our Packages of </span>Linux Hosting</h2>
                        <ul>
                            <Products
                                group_id={135}
                                imgsrc={pic}
                                perprice={'mo'}
                                action={'sharedHostingPlans'}
                            />
                        </ul>

                    </div>
                </div>
                <div className="backorder_services_why">
                    <h2 className="sectthreee_heading">Dedicated Linux  <br /><span className="underline_heading">Servers Are Now Available!</span></h2>
                    <p className="linux_para">The most logical option for hosting websites that need a high degree of security, scalability and storage capacity is dedicated servers.<br /> Navicosoft team members recognise the importance of your website's uptime and availability. We are aiming to facilitate you with the ease<br /> to manage your business smoothly! Now is the time to run your eCommerce shop on a dedicated<br /> Ubuntu server because of the high volume of traffic it receives.<br /><br />
                    Our Linux servers are among the best performing servers available in the UK. Moreover, you are not restricted from upgrading<br /> at any time since we are here to assist you in obtaining the most up-to-date hardware and cutting-edge software. In order <br />to operate a resource-intensive website, you should consider purchasing our cheap Ubuntu ftp server now. Our team<br /> is always available to assist you in selecting the most appropriate dedicated server<br /> at the most appropriate location.</p>

                </div>

                <div className="smm_agency_section">
                    <div className="inner_section">
                        <div className="smm_agency_text">
                            <h2>Most Appealing <br />Features of our Linux Servers</h2>
                            <p>Our Ubuntu SSH servers are designed to provide the most outstanding performance possible. Ubuntu ftp servers make use of the finest technology, including solid-state drives (SSDs) and the most recent virtualization technologies. Moreover, our staff is dedicated to providing you with high-quality services at a reasonable cost.
                            </p>
                        </div>
                        <Hidden mdDown>
                            <div className="smm_secondlast_section">
                                <div className="smm_img_text_section">
                                    <img src={rootacess} alt="Navicosoft Linux Server" loading="lazy" />
                                    <div>
                                        <h3>Root Access</h3>
                                        <p>You are granted root access to a Linux web server because you are the only owner of the server and are responsible for administering its advanced features.</p>
                                    </div>
                                </div>
                                <div className="smm_img_text_section smm_texttt">
                                    <img src={linuxcustomization} alt="Navicosoft Linux Server" loading="lazy" />
                                    <div>
                                        <h3>Customization</h3>
                                        <p>You are given full flexibility to choose the hardware and software that you want to use using this feature.</p>
                                    </div>
                                </div>
                            </div>

                            <div className="smm_secondlast_section">
                                <div className="smm_img_text_section">
                                    <img src={linuxsecurity} alt="Navicosoft Linux Server" loading="lazy" />
                                    <div>
                                        <h3>Security</h3>
                                        <p>Because our Ubuntu server operating system comes preinstalled with security measures, we can guarantee you the greatest degree of security.</p>
                                    </div>
                                </div>
                                <div className="smm_img_text_section">
                                    <img src={regularupdate} alt="Navicosoft Linux Server" loading="lazy" />
                                    <div>
                                        <h3>Regular Updates</h3>
                                        <p>Ensure that your RAM, processor, and storage are upgraded on a regular basis in order to maintain good web performance since any spike in your business may cause your website to slow down.</p>
                                    </div>
                                </div>
                            </div>

                            <div className="smm_secondlast_section">
                                <div className="smm_img_text_section">
                                    <img src={oneclickisnt} alt="Navicosoft Linux Server" loading="lazy" />
                                    <div>
                                        <h3>One-click Installation</h3>
                                        <p>Install WordPress, Joomla, and Drupal with only one click installation technology using the Ubuntu server interface provided by us.</p>
                                    </div>
                                </div>
                                <div className="smm_img_text_section">
                                    <img src={controlpanel} alt="Navicosoft Linux Server" loading="lazy" />
                                    <div>
                                        <h3>Control Panel</h3>
                                        <p>We make it as simple as possible for you to administer your ssh Linux server. We provide you with a user-friendly control panel that allows you to manage the operations of your website without difficulty.</p>
                                    </div>
                                </div>
                            </div>

                            <div className="smm_secondlast_section">
                                <div className="smm_img_text_section">
                                    <img src={metallinux} alt="Navicosoft Linux Server" loading="lazy" />
                                    <div>
                                        <h3>Bare-metal Linux Server</h3>
                                        <p>The single-tenant metal server provides plenty of processing power. Obtain full administrative access to the server, including control of the kernel.</p>
                                    </div>
                                </div>
                                <div className="smm_img_text_section">
                                    <img src={guranteuptime} alt="Navicosoft Linux Server" loading="lazy" />
                                    <div>
                                        <h3>Guaranteed Uptime</h3>
                                        <p>Our ubuntu server gui maximise your website uptime to 99.9%, which means that now your business will no longer suffer any website downtime, thus resulting in higher client visits to your website.</p>
                                    </div>
                                </div>
                            </div>

                            <div className="smm_secondlast_section">
                                <div className="smm_img_text_section">
                                    <img src={highscalibility} alt="Navicosoft Linux Server" loading="lazy" />
                                    <div>
                                        <h3>High Scalability</h3>
                                        <p>The most significant degree of scalability is provided by the Linux server operating system, which allows your company infrastructure to keep pace with technological advances. <br />So you do not need to be concerned about losing your critical data again since our Linux server does automated backups and upgrades on a regular basis.</p>
                                    </div>
                                </div>
                            </div>
                        </Hidden>
                        <Hidden lgUp>
                            <div>
                                <div className="smm_img_text_section smm_texttt">
                                    <img src={rootacess} alt="Navicosoft Linux Server" loading="lazy" />
                                    <div>
                                        <h3>Root Access</h3>
                                        <p>You are granted root access to a Linux web server because you are the only owner of the server and are responsible for administering its advanced features.</p>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div className="smm_img_text_section smm_texttt">
                                    <img src={linuxcustomization} alt="Navicosoft Linux Server" loading="lazy" />
                                    <div>
                                        <h3>Customization</h3>
                                        <p>You are given full flexibility to choose the hardware and software that you want to use using this feature.</p>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div className="smm_img_text_section smm_texttt">
                                    <img src={linuxsecurity} alt="Navicosoft Linux Server" loading="lazy" />
                                    <div>
                                        <h3>Security</h3>
                                        <p>Because our Ubuntu server operating system comes preinstalled with security measures, we can guarantee you the greatest degree of security.</p>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div className="smm_img_text_section smm_texttt">
                                    <img src={regularupdate} alt="Navicosoft Linux Server" loading="lazy" />
                                    <div>
                                        <h3>Regular Updates</h3>
                                        <p>Ensure that your RAM, processor, and storage are upgraded on a regular basis in order to maintain good web performance since any spike in your business may cause your website to slow down.</p>
                                    </div>
                                </div>
                            </div>
                            <div>

                                <div className="smm_img_text_section smm_texttt">
                                    <img src={oneclickisnt} alt="Navicosoft Linux Server" loading="lazy" />
                                    <div>
                                        <h3>One-click Installation</h3>
                                        <p>Install WordPress, Joomla, and Drupal with only one click installation technology using the Ubuntu server interface provided by us.</p>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div className="smm_img_text_section smm_texttt">
                                    <img src={controlpanel} alt="Navicosoft Linux Server" loading="lazy" />
                                    <div>
                                        <h3>Control Panel</h3>
                                        <p>We make it as simple as possible for you to administer your ssh Linux server. We provide you with a user-friendly control panel that allows you to manage the operations of your website without difficulty.</p>
                                    </div>
                                </div>
                            </div>
                            <div>


                                <div className="smm_img_text_section smm_texttt">
                                    <img src={metallinux} alt="Navicosoft Linux Server" loading="lazy" />
                                    <div>
                                        <h3>Bare-metal Linux Server</h3>
                                        <p>The single-tenant metal server provides plenty of processing power. Obtain full administrative access to the server, including control of the kernel.</p>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div className="smm_img_text_section smm_texttt">
                                    <img src={guranteuptime} alt="Navicosoft Linux Server" loading="lazy" />
                                    <div>
                                        <h3>Guaranteed Uptime</h3>
                                        <p>Our ubuntu server gui maximise your website uptime to 99.9%, which means that now your business will no longer suffer any website downtime, thus resulting in higher client visits to your website.</p>
                                    </div>
                                </div>
                            </div>
                            <div>


                                <div className="smm_img_text_section smm_texttt">
                                    <img src={highscalibility} alt="Navicosoft Linux Server" loading="lazy" />
                                    <div>
                                        <h3>High Scalability</h3>
                                        <p>The most significant degree of scalability is provided by the Linux server operating system, which allows your company infrastructure to keep pace with technological advances.
                                            So you do not need to be concerned about losing your critical data again since our Linux server does automated backups and upgrades on a regular basis.
                                            </p>
                                    </div>
                                </div>
                            </div>
                        </Hidden>
                    </div>
                </div>

                <div className="section_two">
                    <div className="secondtextsec">
                        <h2>Lifetime Linux Server Solution<br /> just one stop to manage it all</h2>
                        <p>Navicosoft provides simple, straightforward solutions to help you run your company more efficiently. Let our Linux server provide you with the most satisfactory service possible! Our data centres are built with rigorous security measures, ensuring that your information is safe and secure. If you want to grow your company infrastructure without experiencing any downtime, our fine-tuned Linux web server may be your next step.</p>
                    </div>
                    <div className="imgandtextsec">
                        <div className="lefttextsec">
                            <h2>Free Upgradation</h2>
                            <p>We can facilitate you with free service upgrades in case you need to grow your IT infrastructure. We believe you to say no to downtime to extend your digital area without causing any downtime to your business.</p>
                        </div>
                        <div className="rightimgsec"><img src={freeupgratation} className="righttext_img" alt="Navicosoft Linux Server" /></div>
                    </div>

                    <div className="imgandtextsec">
                        <div className="lefttextsec">
                            <h2>Fully Protected Network</h2>
                            <p>We value the data security of your business. Therefore, we ensure that your data is fully secure and protected from all kinds of spamming and viruses by preinstalling the most recent antivirus software on your Linux server.</p>
                        </div>
                        <div className="rightimgsec"><img src={fullprotetctionnetwork} className="righttext_img" alt="Navicosoft Linux Server" /></div>
                    </div>

                    <div className="imgandtextsec">
                        <div className="lefttextsec">
                            <h2>Unlimited Bandwidth</h2>
                            <p>We look after your online business requirements; therefore, we offer Linux server with the highest possible bandwidth rate. The prime advantage of high bandwidth is that you will handle high traffic volume on your website, which includes video streaming.  </p>
                        </div>
                        <div className="rightimgsec"><img src={unlimitedbandthwidth} className="righttext_img" alt="Navicosoft Linux Server" /></div>
                    </div>
                    <div className="imgandtextsec">
                        <div className="lefttextsec">
                            <h2>Dedicated Technical Support</h2>
                            <p>Navicosoft has a solution to your online problems! Throughout the day, our experienced and committed support team is here to assist you with any technical issues. Now you can input your energy into your business growth while we take care of the technical issues that may arise.</p>
                        </div>
                        <div className="rightimgsec"><img src={dedicatedtechsupport} className="righttext_img" alt="Navicosoft Linux Server" /></div>
                    </div>
                    <div className="imgandtextsec">
                        <div className="lefttextsec">
                            <h2>Continuous Monitoring</h2>
                            <p>We have a very efficient monitoring system that monitors your website 24/7. The reason behind this constant monitoring is to eliminate any security threats.  If you choose us to be your Linux server provider, you can be confident that your servers will be secured from any viruses that may arise.</p>
                        </div>
                        <div className="rightimgsec"><img src={continousmonitoring} className="righttext_img" alt="Navicosoft Linux Server" /></div>
                    </div>

                </div>

                <div className="web_development">
                    <div className="rightchoice_section">
                        <h2>Why Navicosoft For <br />Linux Server Hosting?</h2>
                        <div className="digitalmarketting_section">
                            <div className="tickimg1">
                                <img src={tick} alt="Navicosoft Linux Server"/>
                                <p className="digitalmkt_txt">Our data centers have <br />the highest level of<br />security measures.</p>
                            </div>
                            <div className="tickimg2">
                                <img src={tick} alt="Navicosoft Linux Server"/>
                                <p className="digitalmkt_txt">Our support team is<br /> available 24/7 at  <br />your service.</p>
                            </div>
                            <div className="tickimg3">
                                <img src={tick} alt="Navicosoft Linux Server"/> 
                                <p className="digitalmkt_txt">Get a smooth approach<br />to your website that <br /> is free of downtime <br />interruptions.</p>
                            </div>
                            <div className="tickimg3">
                                <img src={tick} alt="Navicosoft Linux Server"/>  
                                <p className="digitalmkt_txt">Take advantage of <br />dedicated IPs and a server<br /> in the location<br /> of your choice!</p>
                            </div>
                            <div className="tickimg">
                                <img src={tick} alt="Navicosoft Linux Server"/> 
                                <p className="digitalmkt_txt">Automated backups to <br />avoid data loss.</p>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        );
    }
}

export default LinuxServer;




