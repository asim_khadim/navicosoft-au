/* global jQuery, padding */
import React, { Component } from 'react';
import $ from "jquery";
import '../../js/jquery.carousel-ticker.js';
import '../../js/jquery.magnific-popup.min.js'
import '../../js/jquery.marquee.js';
import portfolio_img from '../../images/portfolio_img.png';
import portfolio_pricegage from '../../images/portfolio_pricegage.png';
import portfolio_marketing from '../../images/portfolio_marketing.png';

import './style.scss';

class PortfolioSection extends Component {

	componentDidMount() {
		/*carouselTicker START*/
		const container = $('.carouselTicker');
		let lastScrollTop = 0;
		if ($(window).width() > 1025) {
			container.carouselTicker();
		}
		/*carouselTicker END*/
		/* Apply Marquee */

		let n = 0;
		showImage(n);

		$(".image_text").on("mouseenter", function () {

			n += 1;
			showImage(n);
		});

		function showImage(n) {
			n = n % $(".single_image").length;
			$(".single_image").not(`:eq(${n})`).hide('slow');
			$(".single_image").eq(n).show('slow');
		}

		$('marquee').marquee('cb-tagreel');

		/* End */
	}

	render() {
		//this.h(); 
		return (
			<div className="latest-testimonials">
				<div className="carouselTicker">
					<div className="tm-lists carouselTicker__list">
						<div className="item carouselTicker__item">
							<div className="tmbox twoBox">
								<figure className="tm-img">
									<img loading="lazy" src={portfolio_img} alt="Sample_Image" />
									<a href="#" target="_blank" className="ag-name">Susanna from<br />Designergy</a>
									<a href="#" target="_blank" className="ag-name-small">Susanna from Designergy</a>
									<a href="https://www.youtube.com/watch?v=V4_TI2FBHyY" className="play_icon video_icon"></a>
								</figure>
								<div className="tm-info">
									<h4 className="name">Susanna</h4>
									<div className="post">Customer</div>
								</div>
							</div>
						</div>
						<div className="item carouselTicker__item">
							<div className="tmbox twoBox">
								<figure className="tm-img">
									<img loading="lazy" src={portfolio_pricegage} alt="Portfolio Pricegage" />
									<a href="#" target="_blank" className="ag-name">Conell from<br />Pricegage</a>
									<a href="#" target="_blank" className="ag-name-small">Conell from Pricegage</a>
									<a href="https://www.youtube.com/watch?v=kgUDUmXTJxw" className="play_icon video_icon"></a>
								</figure>
								<div className="tm-info">
									<h4 className="name">Conell</h4>
									<div className="post">Customer</div>
								</div>
							</div>
						</div>
						<div className="item carouselTicker__item">
							<div className="tmbox twoBox">
								<figure className="tm-img">
									<img loading="lazy" src={portfolio_marketing} alt="Portfolio Marketing" />
									<a href="#" target="_blank" className="ag-name">John from <br />Technology Works</a>
									<a href="#" target="_blank" className="ag-name-small">John from Technology Works</a>
									<a href="https://www.youtube.com/watch?v=6flve0jqmQM" className="play_icon video_icon"></a>
								</figure>
								<div className="tm-info">
									<h4 className="name">John</h4>
									<div className="post">Customer</div>
								</div>
							</div>
						</div>








					</div>
				</div>
			</div>

		);
	}
}

export default PortfolioSection;
