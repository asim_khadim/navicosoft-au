import React, { Component } from 'react';
import ReactHtmlParser from "react-html-parser";

import { Hidden } from '@material-ui/core';
import $ from "jquery";
import '../../js/jquery.carousel-ticker.js';
import '../../js/jquery.marquee.js';
import digital4 from '../../images/digital4.png';
import digital3 from '../../images/digital3.png';
import digital2 from '../../images/digital2.png';
import digital1 from '../../images/digital1.png';
import immg2 from '../../images/immg2.webp';
import immg3 from '../../images/immg3.webp';
import partner1 from '../../images/partner1.png';
import partner2 from '../../images/partner2.png';
import partner3 from '../../images/partner3.webp';
import partner4 from '../../images/partner4.webp';
import partner5 from '../../images/partner5.webp';
import partner6 from '../../images/partner6.webp';
import partner7 from '../../images/partner7.webp';
import partner8 from '../../images/partner8.webp';
import partner9 from '../../images/partner9.png';
import partner10 from '../../images/partner10.png';
import dbbimg1 from '../../images/dbbimg1.webp';
import { portfolioList } from './portfolioList'

import './style.scss';


class Portfolio extends Component {

    componentDidMount() {
        /*carouselTicker START*/
        const container = $('.carouselTicker');
        let lastScrollTop = 0;
        if ($(window).width() > 1025) {
            container.carouselTicker();
        }
        /*carouselTicker END*/
        /* Apply Marquee */

        let n = 0;
        showImage(n);

        $(".image_text").on("mouseenter", function () {

            n += 1;
            showImage(n);
        });

        function showImage(n) {
            n = n % $(".single_image").length;
            $(".single_image").not(`:eq(${n})`).hide('slow');
            $(".single_image").eq(n).show('slow');
        }

        $('marquee').marquee('cb-tagreel');

        /* End */
    }


    render() {
        //this.h(); 
        return (
            <div className="portfolio_home_section">

                <div className="works l-container">
                    <Hidden mdDown>
                        <ul className="works-list">

                            {portfolioList.map(portfolio => {
                                return (

                                    !portfolio.rightClass ?

                                        <li className="works-list-item section">
                                            <a className="btn js-tilt">
                                                <div className="works-img">
                                                    <figure className="wow fadeInUp img_style">
                                                        <img src={portfolio.imgSrc} className="attachment-360x240x1 size-360x240x1 wp-post-image" alt="navicosoft Australia" loading="lazy" /></figure>
                                                    <figure className="clone">
                                                        <img src={portfolio.imgSrc} className="attachment-360x240x1 size-360x240x1 wp-post-image" alt="navicosoft Australia" loading="lazy" /></figure>
                                                </div>

                                                <div className="works-text luxy-el" data-speed-y="-5">
                                                    <figcaption className="wow fadeInUp img_style">
                                                        <h2>{ReactHtmlParser(portfolio.siteName)}</h2>
                                                        <span>{ReactHtmlParser(portfolio.taskAccomplished)}</span>
                                                    </figcaption>
                                                </div>
                                            </a>
                                        </li>
                                        : <li className="works-list-item section">
                                            <a className="btn js-tilt">
                                                <div className="works-text luxy-el text_left" data-speed-y="-5">
                                                    <figcaption className="wow fadeInUp left_img img_style">
                                                        <h2>{ReactHtmlParser(portfolio.siteName)}</h2>
                                                        <span>{ReactHtmlParser(portfolio.taskAccomplished)}</span>
                                                    </figcaption>
                                                </div>
                                                <div className="works-img right_img">
                                                    <figure className="wow fadeInUp img_style">
                                                        <img src={portfolio.imgSrc} className="attachment-360x240x1 size-360x240x1 wp-post-image" alt="navicosoft Australia" loading="lazy" /></figure>
                                                    <figure className="clone">
                                                        <img src={portfolio.imgSrc} className="attachment-360x240x1 size-360x240x1 wp-post-image" alt="navicosoft Australia" loading="lazy" /></figure>
                                                </div>
                                            </a>
                                        </li>
                                )
                            }
                            )};

                        </ul>
                    </Hidden>
                    <Hidden lgUp>
                        <ul className="works-list">
                            {portfolioList.map(portfolio => {
                                return (

                                    !portfolio.rightClass ?

                                        <li className="works-list-item section">
                                            <a className="btn js-tilt">
                                                <div className="works-img">
                                                    <figure className="wow fadeInUp img_style">
                                                        <img src={portfolio.imgSrc} className="attachment-360x240x1 size-360x240x1 wp-post-image" alt="navicosoft Australia" loading="lazy" /></figure>
                                                    <figure className="clone">
                                                        <img src={portfolio.imgSrc} className="attachment-360x240x1 size-360x240x1 wp-post-image" alt="navicosoft Australia" loading="lazy" /></figure>
                                                </div>

                                                <div className="works-text luxy-el" data-speed-y="-5">
                                                    <figcaption className="wow fadeInUp img_style">
                                                        <h2>{ReactHtmlParser(portfolio.siteName)}</h2>
                                                        <span>{ReactHtmlParser(portfolio.taskAccomplished)}</span>
                                                    </figcaption>
                                                </div>
                                            </a>
                                        </li> :
                                        <li className="works-list-item section">
                                            <a className="btn js-tilt">

                                                <div className="works-img right_img">
                                                    <figure className="wow fadeInUp img_style">
                                                        <img src={portfolio.imgSrc} className="attachment-360x240x1 size-360x240x1 wp-post-image" alt="navicosoft Australia" loading="lazy" /></figure>
                                                    <figure className="clone">
                                                        <img src={portfolio.imgSrc} className="attachment-360x240x1 size-360x240x1 wp-post-image" alt="navicosoft Australia" loading="lazy" /></figure>
                                                </div>
                                                <div className="works-text luxy-el text_left" data-speed-y="-5">
                                                    <figcaption className="wow fadeInUp left_img img_style">
                                                        <h2>{ReactHtmlParser(portfolio.siteName)}</h2>
                                                        <span>{ReactHtmlParser(portfolio.taskAccomplished)}</span>
                                                    </figcaption>
                                                </div>
                                            </a>
                                        </li>
                                )
                            }
                            )};
                        </ul>
                    </Hidden>
                </div>
                <Hidden mdDown>
                    <marquee behavior="scroll" scrollamount="2" direction="left" width="100%" className="cb-tagreel">
                        <div className="cb-tagreel-content">
                            <div className="cb-tagreel-items" >
                                <div className="cb-tagreel-row strategy" >
                                    <div className="cb-tagreel-item"><span>Strategy</span></div>
                                    <div className="cb-tagreel-item -stroke"><span>Strategy</span></div>
                                    <div className="cb-tagreel-item"><span>Strategy</span></div>
                                    <div className="cb-tagreel-item -stroke"><span>Strategy</span></div>
                                    <div className="cb-tagreel-item"><span>Strategy</span></div>
                                </div>
                                <div className="cb-tagreel-row design" >
                                    <div className="cb-tagreel-item -stroke"><span>Design</span></div>
                                    <div className="cb-tagreel-item"><span>Design</span></div>
                                    <div className="cb-tagreel-item -stroke"><span>Design</span></div>
                                    <div className="cb-tagreel-item"><span>Design</span></div>
                                    <div className="cb-tagreel-item -stroke"><span>Design</span></div>
                                </div>
                                <div className="cb-tagreel-row development" >
                                    <div className="cb-tagreel-item"><span>Development</span></div>
                                    <div className="cb-tagreel-item -stroke"><span>Development</span></div>
                                    <div className="cb-tagreel-item"><span>Development</span></div>
                                    <div className="cb-tagreel-item -stroke"><span>Development</span></div>
                                    <div className="cb-tagreel-item"><span>Development</span></div>
                                </div>
                                <div className="cb-tagreel-row marketting" >
                                    <div className="cb-tagreel-item -stroke"><span>Marketing</span></div>
                                    <div className="cb-tagreel-item"><span>Marketing</span></div>
                                    <div className="cb-tagreel-item -stroke"><span>Marketing</span></div>
                                    <div className="cb-tagreel-item"><span>Marketing</span></div>
                                    <div className="cb-tagreel-item -stroke"><span>Marketing</span></div>
                                </div>
                            </div>
                        </div>
                    </marquee>
                </Hidden>
                <div className="main result_focus_section">
                    <div className="first">
                        <h2>A Result Focused Digital<br />Marketing Agency!</h2>
                    </div>
                    <div className="second"></div>
                    <div className="digital_section">
                        <div className="img1">
                            <img src={digital4} alt="navicosoft Australia"/>
                            <p className="digital_txt">10,000+ <br />Happy Customers</p>
                        </div>
                        <div className="img2">
                            <img src={digital3} alt="navicosoft Australia"/>
                            <p className="digital_txt">5k+ Successfully marketing <br />strategies deployed</p>
                        </div>
                        <div className="img3">
                            <img src={digital2} alt="navicosoft Australia"/>
                            <p className="digital_txt">Numerous Australian <br />Team Members</p>
                        </div>
                        <div className="img4">
                            <img src={digital1} alt="navicosoft Australia"/>
                            <p className="digital_txt">$400 Million <br />Ad-spend managed</p>
                        </div>
                    </div>
                </div>
                {/* <Hidden smDown> */}
                <div className="images_section dont_section">
                    <div className="single_image first_image"><img src={dbbimg1} alt="navicosoft Australia"/></div>
                    <div className="single_image second_image"><img src={immg2} alt="navicosoft Australia"/></div>
                    <div className="single_image third_image"><img src={immg3} alt="navicosoft Australia"/></div>
                    <div className="text_section">
                        <div className="text_div">
                            <p className="image_text">Don't</p>
                            <p className="image_text">be</p>
                            <p className="image_text">boring</p>
                        </div>
                    </div>
                </div>
                {/* </Hidden> */}
                <div className="partner_section">
                    <h2><span>PARTNERSHIP</span></h2>
                    <div className="partner_images">
                        <img src={partner1} alt="navicosoft Australia"/>
                        <img src={partner2} alt="navicosoft Australia"/>
                        <img src={partner3} alt="navicosoft Australia"/>
                        <img src={partner4} alt="navicosoft Australia"/>
                        <img src={partner5} alt="navicosoft Australia"/>
                    </div>
                    <div className="partner_images">
                        <img src={partner6} alt="navicosoft Australia"/>
                        <img src={partner7} alt="navicosoft Australia"/>
                        <img src={partner8} alt="navicosoft Australia"/>
                        <img src={partner9} alt="navicosoft Australia"/>
                        <img src={partner10} alt="navicosoft Australia"/>
                    </div>
                </div>



            </div>
        );
    }
}

export default Portfolio;
