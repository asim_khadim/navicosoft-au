
import image1 from '../../images/image1.webp';
import image2 from '../../images/image2.png';
import image3 from '../../images/image3.png';
import image4 from '../../images/image4.png';

export const portfolioList = [
    {
        id: 1,
        imgSrc: image1,
        siteName: 'FIX <br />SMILE',
        taskAccomplished: 'DESIGN AND FRONT-END',
        rightClass: false
    },
    {
        id: 2,
        imgSrc: image2,
        siteName: 'THE<br />PRINT<br />FUN',
        taskAccomplished: 'Choreography &amp; Dance',
        rightClass: true
    },
    {
        id: 3,
        imgSrc: image3,
        siteName: 'EXQUISITE <br />DESIGNER',
        taskAccomplished: 'DESIGN, FRONT END<br />DEVELOPMENT, SEO',
        rightClass: false
    },
    {
        id: 4,
        imgSrc: image4,
        siteName: 'ALSA',
        taskAccomplished: 'DESIGN, FRONT END<br />DEVELOPMENT, SEO',
        rightClass: true
    }

];