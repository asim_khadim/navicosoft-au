import React, { Fragment, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Collapse from '@material-ui/core/Collapse';
import { Link } from "react-router-dom";
import Grid from "@material-ui/core/Grid";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import FontAwesome from "../UiStyle/FontAwesome";
import { menulists } from "./menuitems";
import logo from '../../images/navicosoft_au_logo.png';

import './style.scss';

const useStyles = makeStyles(theme => ({
    expand: {
        transform: 'rotate(0deg)',
        marginLeft: 'auto',
        transition: theme.transitions.create('transform', {
            duration: theme.transitions.duration.shortest,
        }),
    },
    expandOpen: {
        transform: 'rotate(180deg)',
    },
}));


const MobileMenu = ({ showMobile, setShowMobile }) => {
    const classes = useStyles();

    const [menuId, setActiveMid] = useState();

    function handleExpandClick(id) {
        setActiveMid(id);
        if (menuId === id) {
            setActiveMid()
        }
    }

    return (
        <Fragment>
            <Grid className={`mobileMenu ${showMobile ? '' : 'hidden'}`}>
                <Grid className="sLogo">
                    <Link to="/"><img src={logo} alt="logo" /></Link>
                </Grid>
                <List>
                    {menulists.map(item => {
                        return (
                            <ListItem key={item.id}>
                                {!item.submenu
                                    ? <Link to={item.link}>{item.name}</Link>
                                    : <Grid
                                        className="mItem"
                                        onClick={() => handleExpandClick(item.id)}
                                    >
                                        <span>{item.name}</span>
                                        <Grid
                                            className={clsx(classes.expand, {
                                                [classes.expandOpen]: menuId === item.id,
                                            })}
                                        >
                                            <FontAwesome name="angle-down" />
                                        </Grid>
                                    </Grid>
                                }
                                {item.submenu ? item.submenu.map(subitem => {
                                    return (
                                        <Collapse
                                            key={subitem.id}
                                            className="subMenu"
                                            in={menuId === item.id}
                                            timeout="auto"
                                            unmountOnExit>
                                            {!subitem.submenu
                                                ? <Link to={subitem.link}>{subitem.name}</Link>
                                                : <Link to={subitem.link}>{subitem.name}</Link>
                                                // <span>{subitem.name}</span>
                                                // <Grid
                                                //     className={clsx(classes.expand, {
                                                //         [classes.expandOpen]: menuId === subitem.id,
                                                //     })}
                                                // >
                                                //     {/* <FontAwesome name="angle-down" /> */}
                                                // </Grid>
                                                // </Grid>
                                            }

                                            {subitem.submenu
                                                ? subitem.submenu.map(subitems => {
                                                    return (
                                                        <Collapse
                                                            key={subitems.id}
                                                            className="subMenu"
                                                            in='true'
                                                            timeout="auto"
                                                            unmountOnExit>
                                                            {subitems.link.external
                                                                ? <Link to={{ pathname: subitems.link.external }} target="_blank">{subitems.name}</Link>
                                                                : <Link to={subitems.link}>{subitems.name}</Link>}
                                                            {/* {subitems.link.external && <Link to={{pathname : subitems.link.external}} target="_blank">{subitems.name}</Link>} */}

                                                        </Collapse>
                                                    )
                                                }) : ''}

                                        </Collapse>
                                    )
                                }) : ''}
                            </ListItem>
                        )
                    })}
                </List>
            </Grid>
            <Grid onClick={() => setShowMobile(!showMobile)} className={`menuHoverlay ${showMobile ? 'show' : ''}`}></Grid>
        </Fragment>
    )
};
export default MobileMenu;