export const menulists = [
    {
        id: 1,
        name: 'Hosting',
        link: '',
        submenu: [
            
            {
                id: 1,
                name: 'Web Hosting Services',
                 link: 'web-hosting',
                submenu: [
                    {
                        id: 1,
                        name: 'Linux Web Hosting',
                        link: 'linux-hosting',
        
                    },
                ],

            },
            {
                id: 2,
                name: 'Dedicated Server Hosting',
                link: 'dedicated-hosting',
                submenu: [
                    {
                        id: 1,
                        name: 'VPS Hosting',
                        link: 'vps-hosting',
        
                    },
                    {
                        id: 2,
                        name: 'Windows Server',
                        link: 'windows-server',
        
                    },
                    {
                        id: 3,
                        name: 'Linux Server',
                        link: 'linux-server',
        
                    },
                    
                    
                ],

            },
            {
                id: 3,
                name: 'Apps Hosting',
                link: '/',
                submenu: [
                    {
                        id: 1,
                        name: 'Cloud Backup',
                        link: 'cloud-backup',
        
                    },
                    {
                        id: 2,
                        name: 'Wordpress Hosting',
                        link: '/wordpress-hosting',
        
                    },
                    
                ],
            },
        ]
    },
    {
        id: 2,
        name: 'Domain',
        link: '/domain-registration',
        submenu: [
            {
                id: 1,
                name: 'Domain Registration',
                link: 'domain-registration',
            },
            // {
            //     id: 1,
            //     name: 'Buy Cheap Licenses',
            //      link: {
            //         external: "https://www.navicosoft.com/cheap-licenses/"
            //     } 
            // },
            // {
            //     id: 2,
            //     name: 'Domains Transfer',
            //     link: {
            //         external: "https://www.navicosoft.com/domains-transfer/"
            //     } 
            // },
            {
                id: 3,
                name: 'Backorder Domain',
                link: '/backorder-domain',
            },
        ]
    },
    {
        id: 3,
        name: 'Digital Marketing',
        link: '/digital-marketing',
        submenu: [
            {
                id: 1,
                name: 'Social Media Marketing',
                 link:'/digital-marketing',
                    submenu: [
                        // {
                        //     id: 1,
                        //     name: 'Twitter Ads',
                        //     link: {
                        //         external: "https://www.navicosoft.com/twitter-ads/"
                        //     } 
            
                        // },
                        {
                            id: 2,
                            name: 'Facebook Ads',
                            link:'/facebook-ad'
                        },
                        // {
                        //     id: 3,
                        //     name: 'Linkein Ads',
                        //     link:  {
                        //         external: "https://www.navicosoft.com/linkedin-ads/"
                        //     } 
                        // },
                        {
                            id: 4,
                            name: 'Google Ads',
                            link:'/google-ad'
                        },
                        // {
                        //     id: 5,
                        //     name: 'Youtube',
                        //     link:  {
                        //         external: "https://www.navicosoft.com/youtube/"
                        //     } 
                        // },
                        // {
                        //     id: 6,
                        //     name: 'Google Business',
                        //     link:  {
                        //         external: "https://www.navicosoft.com/google-business/"
                        //     } 
                        // },
                    ],
                    
                }, 
                {
                    id: 2,
                    name: 'Seo Services',
                    link:'/',
                    submenu: [
                        {
                            id: 1,
                            name: 'Website Analytics',
                            link:'/website-analytics'
                        },
                        // {
                        //     id: 2,
                        //     name: 'Content Writing Services',
                        //     link:{
                        //         external: "https://www.navicosoft.com/content-writing-services/"
                        //     } 
                        // },
                    ],
                },
                {
                    id: 3,
                    name: 'Search Engine Marketing',
                    link: '/',
                    submenu: [
                        {
                        id: 1,
                        name: 'Business Email Solution',
                        link: '/business-email-solution'
            
                        },
                        
                    ],
                },
                // {
                //     id: 4,
                //     name: 'Email Marketing Server',
                //     link: {
                //         external: "https://www.navicosoft.com/email-marketing-server/"
                //     } ,
                //     submenu: [
                //         {
                //             id: 1,
                //             name: 'G-Suite For Business',
                //             link: {
                //                 external: "https://www.navicosoft.com/gsuit-business-professional-email-account/"
                //             } 
            
                //         },
                        
                //     ],
                // },
        ]
        
    },
    {
        id: 4,
        name: 'Design',
        link: '/web-design',
        submenu: [
            {
                id: 1,
                name: 'Website Design',
                 link:'/web-design',
                 submenu: [
                    // {
                    //     id: 1,
                    //     name: 'WordPress Website Design',
                    //     link: {
                    //         external: "https://www.navicosoft.com/wordpress-website-design/"
                    //     } 
        
                    // },
                    // {
                    //     id: 2,
                    //     name: 'PSD to HTML Services',
                    //     link: {
                    //         external: "https://www.navicosoft.com/psd-to-html-services/"
                    //     } 
        
                    // },
                    // {
                    //     id: 3,
                    //     name: 'PSD to WordPress Services',
                    //     link: {
                    //         external: "https://www.navicosoft.com/psd-to-wordpress-services/"
                    //     } 
        
                    // },
                    
                ],
            },
            {
                id: 2,
                name: 'Business Design',
                link:'/',
                submenu: [
                    {
                        id: 1,
                        name: 'Logo Design',
                        link:'/logo-design'
        
                    },
                    {
                        id: 2,
                        name: 'Business stationery',
                        link:'/business-stationary'
                    },
                ],
            },
        ]
        
    },
    {
        id: 5,
        name: 'Development',
        link: '/web-development',
        submenu: [
            {
                id: 1,
                name: 'Website Development',
                 link:'/web-development',
                //  submenu: [
                //     {
                //         id: 1,
                //         name: 'WordPress Development',
                //         link: {
                //             external: "https://www.navicosoft.com/wordpress-web-development/"
                //         } 
        
                //     },
                //     {
                //         id: 2,
                //         name: 'OJS Development',
                //         link: {
                //             external: "https://www.navicosoft.com/ojs//"
                //         } 
        
                //     },
                //     {
                //         id: 3,
                //         name: 'Mobile App Development',
                //         link: {
                //             external: "https://www.navicosoft.com/mobile-app-development/"
                //         } 
        
                //     },
                //     {
                //         id: 4,
                //         name: 'E-Commerce Website',
                //         link: {
                //             external: "https://www.navicosoft.com/ecommerce-website-development/"
                //         } 
        
                //     },
                //     {
                //         id: 5,
                //         name: 'WooCommerce Optimization',
                //         link: {
                //             external: "https://www.navicosoft.com/woocommerce-speed-optimization-services/"
                //         } 
        
                //     },
                    
                // ],
            },
        ]
    },
    // {
    //     id: 6,
    //     name: 'AddOns',
    //     link: '/',
    //     submenu: [
    //         {
    //             id: 1,
    //             name: 'Cheap Licenses',
    //             link: {
    //                 external: "https://www.navicosoft.com/cheap-licenses/",
    //                 submenu: [
    //                     {
    //                         id: 1,
    //                         name: 'CPanel Licenses',
    //                         link: {
    //                             external: "https://www.navicosoft.com/cheap-cpanel-license/"
    //                         } 
            
    //                     },
    //                     {
    //                         id: 2,
    //                         name: 'Plesk Licences',
    //                         link: {
    //                             external: "https://www.navicosoft.com/plesk-license/"
    //                         } 
            
    //                     },
    //                     {
    //                         id: 3,
    //                         name: 'Cloud Linux Licences',
    //                         link: {
    //                             external: "https://www.navicosoft.com/cloudlinux-license/"
    //                         } 
            
    //                     },
    //                 ]
                    
    //             } 
                
    //         },
    //         {
    //             id: 2,
    //             name: 'Website Security',
    //             link:{
    //                 external: "https://www.navicosoft.com/website-security/"
    //             } ,
    //             submenu: [
    //                 {
    //                     id: 1,
    //                     name: 'SSL Certificate',
    //                     link:{
    //                         external: "https://www.navicosoft.com/cheap-ssl-certificate/"
    //                     } ,
        
    //                 },
    //                 {
    //                     id: 2,
    //                     name: 'WordPress Security',
    //                     link:{
    //                         external: "https://www.navicosoft.com/wordpress-security/"
    //                     } ,
    //                 },
    //                 {
    //                     id: 3,
    //                     name: 'Microsoft Dynamics',
    //                     link:{
    //                         external: "https://www.navicosoft.com/microsoft-dynamics/"
    //                     } ,
    //                 },
    //             ],
    //         },
    //     ]
    // },
    {
        id: 7,
        name: 'Contact Us',
        link: '/contact-us'
    }
];