import React, { useState } from 'react';
import './style.scss';
import $ from "jquery";

export default function Contactusform({ page_name }) {

    const [query, setQuery] = useState({ name: '', email: '', telephone: '', message: '' })

    const handleChange = (e) => {
        const name = e.target.name;
        const value = e.target.value;
        setQuery({ ...query, [name]: value })
        if (value == '') {
            e.target.classList.remove('fill');
        } else {
            e.target.classList.add('fill');
        }

    }

    const handleSubmit = (e) => {
        e.preventDefault();
        let data = {
            user_name: query.name,
            user_email: query.email,
            page_name: page_name,
            phone: query.telephone,
            message: query.message,
            page_link: window.location.href
        };
        if (!query.name && !query.email && !query.telephone && !query.message) {
            $('.danger_msg').show();
            setTimeout(function () {
                $('.danger_msg').fadeOut('fast');
            }, 7000);
        } else {
            fetch("https://www.navicosoft.com.au/api/index.php", {
                method: "POST",
                body: JSON.stringify(data)
            })
                .then(response => {
                    console.log(response.json())
                    if (response.ok) {
                        $('.success_msg').show();
                        window.dataLayer = window.dataLayer || [];
                        window.dataLayer.push({
                            'event': 'contact_form_trigger',
                            'name': query.name,
                            'email': query.email,
                            'phone': query.telephone,
                            'message': query.message
                        });
                        setTimeout(function () {
                            $('.success_msg').fadeOut('fast');
                        }, 7000);
                        setQuery({ name: '', email: '', telephone: '', message: '' });

                    }
                    if (!response.ok) {
                        throw new Error("Network response was not ok");
                    }
                    return response.json();
                })
                .catch(error => {
                    console.error(
                        "There has been a problem with your fetch operation:",
                        error
                    );
                });
        }
    }

    return (
        <div className="container_dm">


            <div className="bg-contact2">
                <div className="container-contact2">
                    <div className="wrap-contact2">
                        <form className="contact2-form validate-form">
                            <span className="contact2-form-title">
                                <p className="alert-success success_msg" style={{ display: 'none' }}>Thank you for getting in touch!</p>
                                <p className="alert-dangers danger_msg" style={{ display: 'none' }}> All Fields are Required</p>

                                <div className="contactusform">
                                    <h2>Contact Us</h2>
                                    <p>Leave a Message</p>
                                </div>
                            </span>

                            <div className="wrap-input2 validate-input" data-validate="Name is required">
                                <input
                                    className="input2"
                                    type="text"
                                    name="name"
                                    value={query.name}
                                    onChange={handleChange}
                                    required />
                                <span className="focus-input2" data-placeholder="NAME"></span>
                            </div>

                            <div className="wrap-input2 validate-input" data-validate="Valid email is required: ex@abc.xyz">
                                <input
                                    className="input2"
                                    type="email"
                                    name="email"
                                    value={query.email}
                                    onChange={handleChange}
                                    required
                                />
                                <span className="focus-input2" data-placeholder="EMAIL"></span>
                            </div>

                            <div className="wrap-input2 validate-input" data-validate="Phone number is required">
                                <input
                                    className="input2"
                                    type="text"
                                    name="telephone"
                                    value={query.telephone}
                                    onChange={handleChange}
                                    required />
                                <span className="focus-input2" data-placeholder="PHONE"></span>
                            </div>


                            <div className="wrap-input2 validate-input" data-validate="Message is required">
                                <textarea
                                    className="input2"
                                    name="message"
                                    value={query.message}
                                    onChange={handleChange}
                                    required
                                ></textarea>
                                <span className="focus-input2" data-placeholder="MESSAGE"></span>
                            </div>
                            <button className="btn_table" type='submit' onClick={handleSubmit} ><a>Submit</a></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
}