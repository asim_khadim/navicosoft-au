export const faqs = [
    {
        id: '1',
        question: 'What makes a logo design visually appealing?',
        answer: 'A logo design should include both stunning visuals and an accurate depiction of your company. Avoid pictures with popular symbols such as arrows. Always think creatively while developing the visual brand identity. Additionally, always use easily recognisable symbols and images. Select designs that are related to your business and include a suitable colour palette. Finally, choose classic designs over trendy ones.'
    },
    {
        id: '2',
        question: 'What are some of the standard logo design styles offered by Navicosoft?',
        answer: 'We believe that your brand initial impression should be exceptional! Our goal is to offer you everything you need to create an appealing logo. We provide a variety of logotypes, including iconic, typographic, and combinational designs. With instantly recognised forms, artwork, and symbols, iconic logo designs are easy to remember. In contrast, typographic logos are comprised of the words or names that define your business. We offer both icons and typography to reflect your brand in the combinational logo design.'
    },
    {
        id: '3',
        question: 'Where am I allowed to use my business logo design?',
        answer: 'You may incorporate your company logo design into any aspect of your business operations. You may use it on business cards, letterhead copies, team uniforms, caps, mugs, and pens, in addition to other promotional items. Additionally, you may utilise your business logo for internet marketing and social media marketing. Additionally, you may include your business logo on your Facebook and Instagram pages.'
    },
    {
        id: '4',
        question: 'Why should I choose Navicosoft for UK logo design?',
        answer: 'We are the UK leading designers of custom-designed logos. Our in-house design team is capable of completing projects on schedule and at a high standard. Additionally, we are constantly brainstorming new concepts to allow you to select from a variety of options. Our affordable packages are designed to your business specific requirements and budget. We encourage you to freely express your comments and ideas, as we are here to listen and make appropriate adjustments.'
    },
    {
        id: '5',
        question: 'How is Logo Design beneficial to my brand specifically?',
        answer: 'Once your logo design is ready, you can begin to establish your brand identity. A logo design is a visual representation of your brand that communicates to the public who you are. Besides appealing visuals, logo design helps you gain recognition and success. It demonstrates your sincerity and integrity to others. Because you are exposing the public to your brand via this visual sign, it provides you with a fantastic chance to establish your company reputation in the market.'
    },
    {
        id: '6',
        question: 'Can I submit my own logo design ideas to Navicosoft?',
        answer: 'Yes! We always value our clients and believe in creating designs that reflect their preferences. Your suggestions are what allow us to create a logo that reflects your specifications. Simply let us know about your concept. We will make it certain to complete it in the most efficient way. When you use Navicosoft as your UK logo designer, you wont have to be concerned about your graphics since we only create artifacts that are mesmerising and unforgettable.'
    }
]

