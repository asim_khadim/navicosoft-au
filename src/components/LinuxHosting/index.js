import React, { Component } from 'react';
import linuxhosting from '../../images/linuxhosting.png';
import 'react-tabs/style/react-tabs.css';
import pic from '../../images/pic.png';
import imgg2 from '../../images/imgg2.png';
import imgg3 from '../../images/imgg3.png';
import securededicatedserver from '../../images/securededicatedserver.png';
import rocket from '../../images/rocket.png';
import accurateperformance from '../../images/accurateperformance.png';
import oneclicksetup from '../../images/oneclicksetup.png';
import ondemand from '../../images/ondemand.png';
import support from '../../images/247support.png';
import autobackup from '../../images/autobackup.png';
import manageaccount from '../../images/manageaccount.png';
import mysqldatabase from '../../images/mysqldatabase.png';
import clickinstallation from '../../images/1clickinstallation.png';
import tick from '../../images/tick.png';
import whynavicosectionimg from '../../images/whynavicosectionimg.png';
import whynavicosectionimg2 from '../../images/whynavicosectionimg2.png';
import whynavicosectionimg3 from '../../images/whynavicosectionimg3.png';
import Grid from '@material-ui/core/Grid';
import ScrollAnimation from 'react-animate-on-scroll';
import "../../css/animate.min.css";
import './style.scss';
import Products from '../Products';
import { Link } from 'react-router-dom'
import { HashLink } from 'react-router-hash-link';

class LinuxHosting extends Component {
    render() {
        return (
            <div className="dedicated_hosting linuxhosting ">
                <ScrollAnimation animateIn='bounceInRight' animateOut='bounceOutLeft'> 
                    <h1 className="slider_text">Let Your Digital Vibes Meet <br /><span className="slidertext_color">Now With Our Linux <br /> Hosting Feature!</span><br />
                        <HashLink smooth to="#buy-now">
                            <button className="btn_digitalmarketting">
                                <a>Get Started</a>
                            </button>
                        </HashLink>

                        <span className="or_span"> OR </span>
                        <Link to="contact-us"><button className="btn_linux_hosting" ><a>Transfer Hosting</a></button> </Link>

                    </h1>
                </ScrollAnimation>
                <div className="digital_marketing_image_section">
                    <img src={linuxhosting} className="first_img" alt="Navicosoft Linux Hosting" loading="lazy" />
                </div>

                <div className="backorder_services_why">
                    <h2 className="sectthreee_heading">Now Available: Simplified<br /><span className="underline_heading"> Linux Hosting Solutions!</span></h2>
                    <p className="linux_para">It may be tough to choose a web hosting company in the United Kingdom that provides appropriate yet reasonably priced<br /> Linux server hosting when developing a website, whether on a personal or corporate level. Navicosoft, one of the most renowned<br /> companies in the United Kingdom, has been successfully providing Linux web hosting services since 2008. Our real characteristics, <br />such as honesty, kindness, and excellent conduct, along with the highest level of Linux shared hosting quality, distinguishes<br /> us from our competitors. Get everything you could want to host your website on a Linux server with the quickest web hosting service<br /> in the United Kingdom, thanks to the following features:</p>
                    <div className="box_section below_sec">
                        <div className="smm_box_text">
                            <div className="smm_arrow _text">
                                <p>Have more wonderful <br />possibilities now and tomorrow!</p>
                            </div>
                        </div>
                        <div className="smm_box_text">
                            <div className="smm_arrow _text">
                                <p>We guarantee great<br /> hosting</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="smm_dark_sec_background">
                <div className="smm_dark_section">
                    <div className="smm_first_row"> 
                        <h2>Linux Hosting: Linking <br />Businesses to the Global Marketplace</h2>
                        <p>By offering a plethora of options with server Linux hosting, we guarantee that you have a user friendly and super responsive website. For every kind of website, our budget Linux web hosting with cpanel is the most appropriate hosting solution. If you intend to create a blog or an eCommerce website, we offer a server Linux web Hosting package that is ideal for you. So don't wait for the appropriate moment to place your purchase; instead, place your order right now since today is the best time to take advantage of the powerful Linux shared hosting available! Purchase the most secure and sophisticated database choices available, together with one-click installation tools, to make your hosting experience with us as simple and engaging as possible.</p>
                    </div>
                    <hr />
                    <div className="smm_smam"> 
                        <h2>Get your website <br />highly secured!</h2>
                        <p>Prior to developing any website, the foremost consideration should be website security. Because hackers are always on the lookout for opportunities to attack your website, you must adopt appropriate security steps to safeguard your online presence. Our economic system Linux hosting with cpanel is generally regarded as the most secure option in the United Kingdom.<br /><br />
                        When you choose our hosting, you can relax and unwind knowing that we offer a strong and secure control panel. The server firewall, FTP access, and SSL certificates are just a few examples of our website security procedures. Apart from this, you also get full access to our security specialists, who will guarantee that no security breach occurs.
                        </p>
                    </div>
                </div>
                </div>

                <div className="purchase table_pricing" id="buy-now">
                    <div className="container">
                        <h2 className="web_hosting_h2"><span>Our Packages of </span>Linux Hosting</h2>
                        <ul id="buy-now">
                            <Products
                                group_id={135}
                                imgsrc={pic}
                                perprice={'mo'}
                                action={'sharedHostingPlans'}
                            />
                        </ul>

                    </div>
                </div>



                <div className="businessstationary">
                    <div className="businessstationary_process">
                        <h2>Accelerate Your Business <br />With Linux Hosting </h2>
                        <p>Navicosoft consistently puts the customer's satisfaction first. We are always present to help our customers in determining<br /> the most appropriate solution for their websites. Additionally, we use rigorous security procedures to ensure<br /> the safety and security of your online presence. Navicosoft's support staff is available 24/7 throughout the year<br /> with the appropriate solutions to make your life simpler. With the simple-to-use and cost-effective linux hosting with<br /> cpanel, you can simply manage your email using panel. Additionally, all of our Linux hosting plans offer PHP, Perl, and cgi scripts.</p>
                        <div className="process_table_business">
                            <div className="process_table_three">
                                <div className="square_describe">
                                    <img src={rocket} alt="rocket" />
                                </div>
                                <h3>The rocket-fast Server</h3>
                                <p>We optimise your website performance to 100%. Our premium low-density Linux servers will significantly improve the speed with which your website loads. Take advantage of our Linux hosting now and leave your competitors in the dust!</p>
                            </div>
                            <div className="process_table_three">
                                <div className="square_describe">
                                    <img src={accurateperformance} alt="accurateperformance" />
                                </div>
                                <h3>Accelerated Performance</h3>
                                <p>All of our Linux servers are equipped with solid-state disc storage. This implies that your website will load in a matter of nanoseconds. This enhanced performance provided by the newest SSD storage is well worth the investment!</p>
                            </div>
                            <div className="process_table_three">
                                <div className="square_describe">
                                    <img src={oneclicksetup} alt="oneclicksetup" />
                                </div>
                                <h3>One-click Domain Setup</h3>
                                <p>When you purchase a Linux server hosting package from us, you can be certain that one-click installation features comes with it. Connecting your domain to your hosting has never been this simple, but Navicosoft's one-click setup has simplified and accelerated the process for you.</p>
                            </div>
                        </div>
                        <div className="process_table_business second_table_dedicate">
                            <div className="process_table_three">
                                <div className="square_describe">
                                    <img src={ondemand} alt="ondemand" />
                                </div>
                                <h3>On-demand Resources</h3>
                                <p>Are you nearing the limit of your hosting panel's CPU resources? There is no need to be concerned, since Navicosoft enables one-click resource expansion. With a simple click, you can quickly boost your CPU, RAM, and I/O.</p>
                            </div>
                            <div className="process_table_three">
                                <div className="square_describe">
                                    <img src={support} alt="support" />
                                </div>
                                <h3>24/7 Support</h3>
                                <p>Chill Out! Navicosoft is there to host your website with round-the-clock monitoring and assistance. We offer DDoS protection to guarantee your website's security. Additionally, you will get 24/7 assistance in the event of a technical problem.</p>
                            </div>
                            <div className="process_table_three">
                                <div className="square_describe">
                                    <img src={autobackup} alt="autobackup" />
                                </div>
                                <h3>Automated Backups</h3>
                                <p>We enable you to backup your website data intelligently. You may backup your data quickly using the on-click backup feature in your cpanel. Not just that, but with the function of automatic backups, your data is backed up on a regular basis to guarantee its security.</p>
                            </div>
                        </div>

                    </div>
                </div>

                <div className="why_navicosoft_economy_linux_package">
                    <h2>Why Navicosoft</h2>
                    <h3>For Economy <span className="yellow_color">Linux Hosting with Cpanel?</span></h3>
                    <div className="linux_flex">
                        <div class="linux_left">
                            <p>It may be tough to choose a web hosting company in the United Kingdom that provides appropriate yet reasonably priced Linux server hosting when developing a website, whether on a personal or corporate level. Navicosoft, one of the most renowned companies in the United Kingdom, has been successfully providing Linux web hosting services since 2008. Our real characteristics, such as honesty, kindness, and excellent conduct, along with the highest level of Linux shared hosting quality, distinguishes us from our competitors. Get everything you could want to host your website on a Linux server with the quickest web hosting service in the United Kingdom, thanks to these features:</p>
                        </div>
                        <div class="linux_right">
                            <ul>
                                <li><i class="far fa-check-circle"></i><p>Navicosoft guarantees that website and cpanel<br />  migrations are completely free.</p></li>
                                <li><i class="far fa-check-circle"></i><p>Single-click installation is included with <br />all of our Linux shared hosting services.</p></li>
                                <li><i class="far fa-check-circle"></i><p>24/7/365 availability of <br />technical support.</p></li>
                                <li><i class="far fa-check-circle"></i><p>Navicosoft use all available resources to ensure <br />your website is safe and secure.</p></li>
                                {/* <li><i class="far fa-check-circle"></i><p>We are not the ones who just claim. Rather we take proper <br />measures to make your website secure.</p></li> */}
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}

export default LinuxHosting;
