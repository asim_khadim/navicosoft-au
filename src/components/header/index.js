import React, { useState, useEffect } from "react";
import $ from "jquery";
import { connect } from 'react-redux';
import { Grid } from "@material-ui/core";
import { Link, NavLink, withRouter } from "react-router-dom";
import ListItem from "@material-ui/core/ListItem";
import List from "@material-ui/core/List";
import Button from "@material-ui/core/Button";
import MobileMenu from "../MobileMenu";
import Hidden from "@material-ui/core/Hidden";
import Logo from "../Logo";
import { useTranslation } from 'react-i18next';
import Cookies from 'js-cookie';
import { Container } from '@material-ui/core';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
//import logo from '../../images/logo.png';
import logo from '../../images/navicosoft_au_logo.png';
import yellowline from '../../images/yellowline.png';
import whitelogo from '../../images/navicosoft_white_logo.webp';
import moblogo from '../../images/moblogo.png';
import en from '../../images/icon/flags/en.svg';
import br from '../../images/icon/flags/br.svg';
import navigation_background from '../../images/navigation_background.webp';
import './style.scss';
import { gsap, TimelineMax } from "gsap";

gsap.registerPlugin(TimelineMax);


const Header = (props) => {
    const [showMobile, setShowMobile] = useState(false);

    useEffect(() => {

        let cookie = Cookies.get('lang');
        if (!cookie) {
            cookie = 'en';
            Cookies.set('lang', cookie);
            setLang(cookie);
        } else {
            setLang(Cookies.get('lang'));
        }

    }, []);
    const { t, i18n } = useTranslation();

    function handleClick(lang) {
        i18n.changeLanguage(lang);
        i18n.changeLanguage(lang);
    }

    const [lang, setLang] = React.useState('en');
    const handleChange = event => {
        i18n.changeLanguage(event.target.value);
        Cookies.set('lang', event.target.value);
        setLang(Cookies.get('lang'));
    };

    //nav animation
    var t1 = new TimelineMax({ paused: true });

    // t1.to(".one",{
    //     y: 6,
    //     rotation: 45,
    //     duration: 0.5,
    //     ease: 'power2.inOut',
    // });
    // t1.to(".two",{
    //     y: -6,
    //     rotation: -45,
    //     duration: 0.5,
    //     ease: 'power2.inOut',
    // }, "-=0.5");


    t1.to(".menus", 0.5, {
        transform: "scale(1)",
        //webkitClipPath:"circle(100% at 50% 35px)",
        ease: 'power2.inOut',
        duration: 1,
    });

    t1.to(".menus", 0.5, {
        //transform: "scale(1)",
        webkitClipPath: "circle(100% at 50% 35px)",
        ease: 'power2.inOut',
        duration: 1,
    });


    t1.reverse();

    $(document).on("click", ".column a, a.dropbtn", function () {
        $('.dropdown-content').css("display", "none");
        setTimeout(() => {
            $('.dropdown-content').css("display", "");
        }, 1000);

    });


    $(document).on("click", ".toggle-btn", function () {
        t1.reversed(!t1.reversed());
    });



    // $('.dropdown').toggle(function () {
    //     $(this).find(".dropdown-content").css("opacity", "0");
    // }, function () {
    //     $(this).find(".dropdown-content").css("opacity", "0");
    // });

    function reversecolor() {
        t1.to(".one", 0, {
            backgroundColor: "#000",

        });
        t1.to(".two", 0, {
            backgroundColor: "#000"
        });
    }
    const linkClickHandler = (e) => {
        // alert('Oh Hello');
        e.target.classList.remove('dropdown');
    }

    return (
        <Grid className={`header ${!props.fixHeader}`} item xs={12} md={12} container>

            <Grid item xs={6} md={4}>
                <div className="logo desktop_logo">
                    <Hidden smDown>
                        <Logo
                            logo={logo}
                            alt="navicosoft"
                            link="/"
                            isHeaderFix={!props.fixHeader}
                        />
                    </Hidden>
                </div>
                <div className="mobile_logo">
                    <Hidden smUp>
                        <Logo
                            logo={moblogo}
                            alt="navicosoft"
                            link="/"
                            isHeaderFix={!props.fixHeader}
                        />
                    </Hidden>
                </div>
            </Grid>
            {/* <Grid item xs={6} md={4}>
                <Hidden lgUp>
                    <Grid item xs={12} sm={12} className="text-right mobile_right_menu">
                        <Grid className={`humbergur ${showMobile ? 'show' : ''}`}
                              onClick={() => setShowMobile(!showMobile)}>
                            <span></span>
                            <span></span>
                            <span></span>
                        </Grid>
                    </Grid>
                </Hidden>
                <Hidden mdDown>
                <div className="header-right">
                    <Link to="/about-us" className="menu_item hover-1">HISTORY</Link>
                    <Link to="/" className="menu_item hover-1">PROJECTS</Link>
                    <Link to="/" className="menu_item hover-1">SOLUTIONS</Link>
                </div>
                </Hidden>
            </Grid>  */}
            <Hidden mdDown>
                <Grid item xs={8} className="nav_right">
                    {/* <div className="right_inner">
                    <div className="phone">
                        <i className="fa fa-phone"></i> +61 434 874 229
                    </div>

                    <div className="toggle-btn">
                        <div className="menu_icon">
                            <span className="one"></span>
                            <span className="two"></span>
                        </div>
                    </div>
                </div> */}
                    <div className="navbar">
                        <div className="dropdown">
                            <Button
                                component={Link}
                                to="/web-hosting"
                                className="dropbtn"
                            // onClick={linkClickHandler}
                            >Hosting
                            </Button>
                            <div className="dropdown-content">
                                <div className="row">
                                    <div className="inner_row">
                                        <div className="column">
                                            <h3>Web Hosting <br />Services</h3>
                                            <img src={yellowline} className="yellow_line" alt="navicosoft" loading="lazy" />
                                            <Link to='/linux-hosting' /*onClick={linkClickHandler}*/>Linux Web Hosting</Link>
                                            {/* <Link to={{ pathname: "https://www.navicosoft.com/windows-hosting/" }} target="_blank">Windows Hosting</Link> */}
                                            <Link to='/web-hosting'>Web Hosting Services</Link>
                                        </div>
                                        <div className="column">
                                            <h3>Dedicated <br />Server Hosting</h3>
                                            <img src={yellowline} className="yellow_line" alt="navicosoft" loading="lazy" />
                                            <Link to='/vps-hosting'>VPS Hosting</Link>
                                            <Link to='/windows-server'>Windows Server</Link>
                                            <Link to='/linux-server'>Linux Server</Link>
                                            {/* <Link to={{ pathname: "https://www.navicosoft.com/cloud-hosting/" }} target="_blank">Cloud Hosting</Link> */}
                                            <Link to='dedicated-hosting'>Dedicated Server Hosting</Link>
                                        </div>
                                        <div className="column">
                                            <h3>Apps <br />Hosting</h3>
                                            <img src={yellowline} className="yellow_line" alt="navicosoft" loading="lazy" />
                                            <Link to='/cloud-backup'>Cloud BackUp</Link>
                                            <Link to='/wordpress-hosting'>WordPress Hosting</Link>
                                            
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div className="dropdown">
                            <Button component={Link} to="/domain-registration" className="dropbtn">Domain
                            </Button>
                            <div className="dropdown-content">
                                <div className="row">
                                    <div className="inner_row">
                                        <div className="column">
                                            <h3>Get Your Domain</h3>
                                            <img src={yellowline} className="yellow_line" alt="navicosoft" loading="lazy" />
                                            {/* <Link to={{ pathname: "https://www.navicosoft.com/cheap-licenses/" }} target="_blank" >Buy Cheap Licenses</Link>
                                            <Link to={{ pathname: "https://www.navicosoft.com/domains-transfer/" }} target="_blank" >Domains Transfer</Link> */}
                                            <Link to="domain-registration">Domain Registration</Link>
                                            <Link to='backorder-domain'>Backorder Domain</Link>
                                        </div>
                                    </div >

                                </div >
                            </div >
                        </div >

                        <div className="dropdown">
                            <Button component={Link} to="/digital-marketing" className="dropbtn">Digital Marketing
                            </Button>
                            <div className="dropdown-content">
                                <div className="row">
                                    <div className="inner_row">
                                        <div className="column">
                                            <h3>Social Media <br />Marketing</h3>
                                            <img src={yellowline} className="yellow_line" alt="navicosoft" loading="lazy" />
                                            {/* <Link to={{ pathname: "https://www.navicosoft.com/twitter-ads/" }} target="_blank" >Twitter Ads</Link> */}
                                            <Link to='/facebook-ads' >Facebook Ads</Link>
                                            {/* <Link to={{ pathname: "https://www.navicosoft.com/linkedin-ads/" }} target="_blank">Linkein Ads</Link> */}
                                            <Link to='/google-ads'>Google Ads</Link>
                                            <Link to='/social-media-marketing'>Social Media Marketing</Link>
                                            {/* <Link to={{ pathname: "https://www.navicosoft.com/youtube/" }} target="_blank">Youtube</Link> */}
                                            {/* <Link to={{ pathname: "https://www.navicosoft.com/google-business/" }} target="_blank">Google Business</Link> */}
                                        </div>
                                        <div className="column">
                                            <h3>SEO <br /> Services</h3>
                                            <img src={yellowline} className="yellow_line" alt="navicosoft" loading="lazy" />
                                            <Link to='/website-analytics'>Website Analytics</Link>
                                            {/* <Link to={{ pathname: "https://www.navicosoft.com/on-page-seo/" }} target="_blank">On Page SEO</Link> */}
                                            {/* <Link to={{ pathname: "https://www.navicosoft.com/content-writing-services/" }} target="_blank">Content Writing Services</Link>                                          <Link></Link>Link 3</a> */}
                                        </div >
                                        <div className="column">
                                            <h3>Search Engine <br />Marketing</h3>
                                            <img src={yellowline} className="yellow_line" alt="navicosoft" loading="lazy" />
                                            <Link to='/business-email-solution'>Business Email Solution</Link>
                                            {/* <Link to={{ pathname: "https://www.navicosoft.com/ppc-pay-per-click/" }} target="_blank">Pay Per Click</Link> */}
                                        </div >
                                        {/* <div className="column four_column">
                                            <h3>Email Marketing <br />Server</h3>
                                            <img src={yellowline} className="yellow_line" alt="navicosoft" loading="lazy" />
                                           
                                        </div > */}
                                    </div >

                                </div >
                            </div >
                        </div >

                        <div className="dropdown">
                            <Button component={Link} to="/web-design" className="dropbtn">Design
                            </Button>
                            <div className="dropdown-content">
                                <div className="row">
                                    <div className="inner_row">
                                        <div className="column">
                                            <h3>Website Design</h3>
                                            <img src={yellowline} className="yellow_line" alt="navicosoft" loading="lazy" />
                                            <Link to='/web-design'>Website Design</Link>
                                        </div>
                                        <div className="column">
                                            <h3>Business Design</h3>
                                            <img src={yellowline} className="yellow_line" alt="navicosoft" loading="lazy" />
                                            <Link to='/logo-design'>Logo Design</Link>
                                            <Link to='/business-stationery'>Business stationery</Link>
                                        </div >
                                        <div className="column">
                                            <h3>Development</h3>
                                            <img src={yellowline} className="yellow_line" alt="navicosoft" loading="lazy" />
                                            <Link to='/web-development'>Web Development</Link>
                                            {/* <Link to='/business-stationary'>Business stationery</Link> */}
                                        </div >
                                    </div >

                                </div >
                            </div >
                        </div >

                        {/* <div className="dropdown">
                            <Button component={Link} to="/web-development" className="dropbtn">Development
                            </Button>
                            <div className="dropdown-content">
                                <div className="row">
                                    <div className="inner_row">
                                        <div className="column">
                                            <h3> Software Development</h3>
                                            <img src={yellowline} className="yellow_line" alt="navicosoft" loading="lazy" />
                                            {/* <Link to={{ pathname: "https://www.navicosoft.com/wordpress-web-development/" }} target="_blank">WordPress Development</Link>
                                            <Link to={{ pathname: "https://www.navicosoft.com/ojs/" }} target="_blank">OJS Development</Link>
                                            <Link to={{ pathname: "https://www.navicosoft.com/mobile-app-development/" }} target="_blank">Mobile Apps Development</Link>
                                            <Link to={{ pathname: "https://www.navicosoft.com/ecommerce-website-development/" }} target="_blank">E-Commerce Website</Link>
                                            <Link to={{ pathname: "https://www.navicosoft.com/woocommerce-speed-optimization-services/" }} target="_blank">WooCommerce Optimization</Link> */}
                                        {/* </div>
                                    </div >
                                </div >
                            </div >
                        </div > */}

                        {/* <div className="dropdown">
                            <Button component={Link} to="#" className="dropbtn">AddOns
                            </Button>
                            <div className="dropdown-content">
                                <div className="row">
                                    <div className="inner_row">
                                        <div className="column">
                                            <h3>Cheap Licenses</h3>
                                            <img src={yellowline} className="yellow_line" alt="navicosoft" loading="lazy" /> */}
                        {/* <Link to={{ pathname: "https://www.navicosoft.com/cheap-cpanel-license/" }} target="_blank">CPanel Licenses</Link>
                                            <Link to={{ pathname: "https://www.navicosoft.com/plesk-license/" }} target="_blank">Plesk Licences</Link>
                                            <Link to={{ pathname: "https://www.navicosoft.com/cloudlinux-license/" }} target="_blank"> Cloud Linux Licences</Link> */}
                        {/* </div> */}
                        {/* <div className="column">
                                            <h3>Website Security</h3>
                                            <img src={yellowline} className="yellow_line" alt="navicosoft" loading="lazy" /> */}
                        {/* <Link to={{ pathname: "https://www.navicosoft.com/cheap-ssl-certificate/" }} target="_blank">SSL Certificate</Link>
                                            <Link to={{ pathname: "https://www.navicosoft.com/wordpress-security/" }} target="_blank">WordPress Security</Link>
                                            <Link to={{ pathname: "https://www.navicosoft.com/microsoft-dynamics/" }} target="_blank">Microsoft Dynamics</Link> */}
                        {/* </div >

                                    </div >

                                </div >
                            </div >
                        </div > */}

                        <div className="dropdown">
                            <Button component={Link} to="/contact-us" className="dropbtn">Contact Us
                            </Button>
                        </div>
                    </div >



                </Grid >
            </Hidden >

            {/* POP UP Menu Html */}
            < div className="menus" style={{ backgroundImage: `url(${navigation_background})` }}>
                <Grid className="inner_menuheader" item xs={12} md={12} container>
                    <Grid item xs={6} md={4}>
                        <div className="logo desktop_logo">
                            <Hidden smDown>
                                <Logo
                                    logo={whitelogo}
                                    alt="navicosoft"
                                    link="/"
                                    isHeaderFix={!props.fixHeader}
                                />
                            </Hidden>
                        </div>
                    </Grid>
                    <Grid item xs={6} md={4}>
                        <Hidden mdDown>
                            <div className="header-right white_text">
                                <Link to="/about-us" className="menu_item hover-1">HISTORY</Link>
                                <Link to="/" className="menu_item hover-1">PROJECTS</Link>
                                <Link to="/" className="menu_item hover-1">SOLUTIONS</Link>
                            </div>
                        </Hidden>
                    </Grid>
                    <Hidden mdDown>
                        <Grid item xs={4} className="nav_right white_nav_right">
                            <div className="right_inner">
                                <div className="phone">
                                    <i className="fa fa-phone"></i> +61 434 874 229
                                </div>

                                <div className="toggle-btn">
                                    <div className="menu_icon">
                                        <span className="one"></span>
                                        <span className="two"></span>
                                    </div>
                                </div>
                            </div>

                        </Grid>
                    </Hidden>

                </Grid>
                <div className="menu_container">
                    <Grid className="menu_grid" item xs={12} md={12} container>
                        <Grid item xs={4} className="inner_nav">
                            <h2 className="menu_title">Digital Marketing</h2>
                            <ul className="navigation">
                                <li><a href="/digital-marketing">Digital Marketing</a></li>
                                <li><a href="/social-media-marketing">Social Media Marketing</a></li>
                                <li><a href="/business-stationary">Business</a></li>
                                <li><a href="/website-analytics">Website Analytics</a></li>
                                <li><a href="/google-ads">Google Ad</a></li>
                            </ul>
                        </Grid>
                        <Grid item xs={4} className="inner_nav">
                            <h2 className="menu_title">Web Design & Development</h2>
                            <ul className="navigation">
                                <li><a href="/web-design ">Web Design</a></li>
                                <li><a href="/web-development">Web Development</a></li>
                                <li><a href="/logo-design">Logo Design</a></li>
                            </ul>
                        </Grid>
                        <Grid item xs={4} className="inner_nav">
                            <h2 className="menu_title">Web Hosting & Domain</h2>
                            <ul className="navigation">
                                <li><a href="/web-hosting">Web Hosting</a></li>
                                <li><a href="/dedicated-hosting">Dedicated Hosting</a></li>
                                <li><a href="/linux-hosting">Linux Hosting</a></li>
                                <li><a href="/domain-registration">Domain Registration</a></li>
                                <li><a href="/backorder-domain">Backorder Domain</a></li>
                                <li><a href="/linux-server">Linux Server</a></li>
                                <li><a href="/windows-server">Windows Server</a></li>

                            </ul>
                        </Grid>
                    </Grid>
                </div>
            </div >

            {/* <Hidden mdDown>
                    <Grid item xs={12} md={7}>
                        <Grid className="mainMenu">
                            <List>
                                {props.menus !== undefined ? props.menus.map((item, i) => {
                                    return (
                                        <ListItem key={i}>
                                            <NavLink to={`/${item.slug}`} exact
                                                     className={props.location.pathname === '/' ? item.component === 'HomePage' ? 'active' : '' : ''}
                                                     activeClassName="active">{item.title}</NavLink>
                                        
                                        </ListItem>
                                    )
                                }) : ''}
                            </List>
                        </Grid>
                    </Grid>
                </Hidden> */}
            {/* <Hidden xsDown>
                    <Grid item xs={3} lg={3} sm={4} container alignItems="center"
                          className="justify-md-flex-space-between" justify="flex-end">
                        <Button component={Link} to="/contact-us" className="cBtn cBtnOutline cBtnUppercase">Contact
                            Us</Button>
                        <FormControl className="languageSelector">
                            <Select value={lang} onChange={handleChange} displayEmpty className="languageSelect">
                                <MenuItem value="en"><img src={en} alt="language"/>en</MenuItem>
                                <MenuItem value="br"><img src={br} alt="language"/>br</MenuItem>
                            </Select>
                        </FormControl>
                    </Grid>
                </Hidden> */}
            <Hidden lgUp>
                <Grid item xs={6} sm={2} className="text-right">
                    <Grid className={`humbergur ${showMobile ? 'show' : ''}`}
                        onClick={() => setShowMobile(!showMobile)}>
                        <span></span>
                        <span></span>
                        <span></span>
                    </Grid>
                </Grid>
            </Hidden>
            <MobileMenu showMobile={showMobile} setShowMobile={setShowMobile} />
        </Grid >
    )
};

const mapStateToProps = state => {
    return {
        header: state.header,
        menus: state.header.menu_list,
    }
};

export default connect(mapStateToProps)(withRouter(Header));