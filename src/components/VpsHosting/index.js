import React, { Component } from 'react';
import vpshosting from '../../images/vpshosting/vpshosting.png';
import linuxvps from '../../images/vpshosting/linuxvps.png';
import windowsvps from '../../images/vpshosting/windowsvps.png';
import pic from '../../images/pic.png';
import vpsbackground from '../../images/vpshosting/vpsbackground.png';
import tick from '../../images/tick.png';
import scalability from '../../images/vpshosting/scalability.png';
import security from '../../images/vpshosting/security.png';
import datacenter from '../../images/vpshosting/datacenter.png';
import maximumuptime from '../../images/vpshosting/maximumuptime.png';
import upgradedtechnology from '../../images/vpshosting/upgradedtechnology.png';
import support from '../../images/vpshosting/support.png';
import Flippy, { FrontSide, BackSide } from 'react-flippy';
import cloudserver from '../../images/vpshosting/cloudserver.png';
import strategy from '../../images/strategy.png';
import deployment from '../../images/deployment.png';
import 'react-tabs/style/react-tabs.css';
import Grid from '@material-ui/core/Grid';
import ScrollAnimation from 'react-animate-on-scroll';
import "../../css/animate.min.css";
import { Hidden } from '@material-ui/core';
import './style.scss';
import Products from '../Products';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import { HashLink } from 'react-router-hash-link';


class VpsHosting extends Component {
    render() {
        return (
            <div className="vps_hosting">
                <ScrollAnimation animateIn='bounceInRight' animateOut='bounceOutLeft'>
                    <h1 className="slider_text"><span className="slidertext_color">Price tag is no longer a hindrance! </span> <br /><span className="vps_header_text">Navicosoft offers Complete Root Access and<br /> Full Vistualisation of VPS hosting in <br />the United Kingdom.</span><br />
                        <HashLink smooth to="#buy-now">
                            <button className="btn_digitalmarketting">
                                <a>Check VPS Hosting</a>
                            </button>
                        </HashLink>
                    </h1>
                </ScrollAnimation>
                <div className="digital_marketing_image_section">
                    <img src={vpshosting} className="first_img" alt="vpshosting" loading="lazy" />
                </div>

                <div className="section_select_vps"> 
                    <h2><span class="heading_color">Choose Your Operating</span><br />System With VPS Hosting.</h2>
                    <div className="linux_windows_select">
                        <div className="linux_vps">
                            <h3>Linux VPS Hosting</h3>
                            <img src={linuxvps} className="first_img" alt="linuxvps" loading="lazy" /> 
                            <p>You can benefit from increased performance, stability,and provisioning with fully customised Linux VPS Hosting.</p>

                            <HashLink smooth to="#buy-now">
                                <button className="btn_digitalmarketting">
                                    <a>Learn More Details</a>
                                </button>
                            </HashLink>
                        </div>
                        <div className="windows_vps">
                            <h3>Windows VPS Hosting</h3>
                            <img src={windowsvps} className="first_img" alt="windowsvps" loading="lazy" />
                            <p>You can benefit from increased performance, stability, and provisioning with fully customised Windows VPS Hosting.</p>

                            <HashLink smooth to="#buy-now">
                                <button className="btn_digitalmarketting">
                                    <a>Learn More Details</a>
                                </button>
                            </HashLink>
                        </div>
                    </div>
                </div>

                <div className="dedicated_hosting" id="buy-now">
                    <div className="purchase table_pricing dedi_host" id="buy-now">
                        <div className="container-fluid">
                            <h2 className="web_hosting_h2">Plan of <span>VPS Hosting</span></h2>
                            <ul>

                                <Products
                                    group_id={126}
                                    cycle={'annually'}
                                    imgsrc={pic}
                                    product_title={''}
                                    perprice={'mo'}
                                    action={'dedicatedHostingPlans'}
                                />
                            </ul>

                        </div>
                    </div>
                </div>


                <div className="dedicated_background_section" style={{ backgroundImage: `url(${vpsbackground})` }} alt="windowsvps">
                    <h2 className="sectthreee_heading mobile_stationary"><span className="color_yellow">Our Exceptional </span> <br />VPS Hosting Services </h2>
                    <p className="para_background_linux">We aim to give you a unique online experience. We establish an online reputation for your website that is safe, intelligent, and trustworthy. We are aware that hosting services may become unavailable for a variety of reasons. The network may become unavailable, and backups may become crypto-locked. We've gone through an extensive process to create a virtual private server that delivers the best speed, security, and reliability, while also protecting you from power outages.</p>

                </div>


                <div className="web_development">
                    <div className="rightchoice_section">
                        <h2>Let’s move to <br />VPS Server Hosting</h2> 
                        <p>Although customers are well aware of their hosting requirements, it becomes necessary<br />at times to select the platform that best meets their business's growing demands. It's very difficult to comprehend the <br />complexities of hosting. We can, however, allow you to benefit from our expertise. Simply migrate to a virtual private server (VPS)<br /> when the following conditions exist:</p>
                        <div className="digitalmarketting_section">
                            <div className="tickimg1">
                                <img src={tick} alt="tick" /> 
                                <p className="digitalmkt_txt">Your website traffic is increased above 30%.</p>
                            </div>
                            <div className="tickimg2">
                                <img src={tick} alt="tick" />
                                <p className="digitalmkt_txt">You are interested in running customised operating systems &<br /> applications.</p>
                            </div>
                            <div className="tickimg3">
                                <img src={tick} alt="tick" /> 
                                <p className="digitalmkt_txt">Website is getting less responsive</p>
                            </div>
                            <div className="tickimg3">
                                <img src={tick} alt="tick" /> 
                                <p className="digitalmkt_txt">Your website has rich  media content</p>
                            </div>
                            <div className="tickimg">
                                <img src={tick} alt="tick" /> 
                                <p className="digitalmkt_txt">You want to ensure the security of your website</p>
                            </div>

                        </div>
                    </div>
                </div>


                <div className="smm_agency_section">
                    <div className="inner_section">
                        <div className="smm_agency_text">
                            <h2>Features of our <br />VPS Hosting in United Kingdom</h2>
                            <p>The digital revolution has propelled the growth of Navicosoft. With our customised and cheap VPS hosting services, we help you succeed in your business. We have an extensive knowledge of VPS. It's the right time to take advantage of our high-tech Virtual private Server hosting since we know how to show you in the most professional manner possible to the rest of the industry.
                            </p>
                        </div>
                        <Hidden mdDown>
                            <div className="smm_secondlast_section">
                                <div className="smm_img_text_section">
                                    <img src={scalability} alt="scalability" loading="lazy" />
                                    <div>
                                        <h3>Scalability</h3>
                                        <p>Our VPS Hosting plans are completely customisable. You don't need to stress about your business's demands fluctuating since our hosting plans can always be customised as per your requirements, including storage, bandwidth, and networking speed.</p>
                                    </div>
                                </div>
                                <div className="smm_img_text_section">
                                    <img src={security} alt="security" loading="lazy" />
                                    <div>
                                        <h3>Security</h3>
                                        <p>When it comes to websites, security is one of the most important requirements to have. A website may function properly even at a slow speed, but it cannot function well with inadequate data security measures in place. Our security measures include visitor escorts, biometric scanners and secondary authentication, all of which are implemented with great care.</p>
                                    </div>
                                </div>
                            </div>

                            <div className="smm_secondlast_section">
                                <div className="smm_img_text_section">
                                    <img src={datacenter} alt="datacenter" loading="lazy" />
                                    <div>
                                        <h3>Armored Data Centers</h3>
                                        <p>Navicosoft is committed to providing UK's most affordable VPS hosting. Our data centres are equipped with comprehensive heating, ventilation, and air conditioning systems. Additionally, backup and recovery systems are monitored 24/7/365.</p>
                                    </div>
                                </div>
                                <div className="smm_img_text_section">
                                    <img src={maximumuptime} alt="maximumuptime" loading="lazy" />
                                    <div>
                                        <h3>Maximum uptime</h3>
                                        <p>We provide a data centre that is network-independent. Our team makes it a priority to offer our customers the possible advantage of virtual server hosting. We guarantee 99.99% uptime to make your website accessible at all times.</p>
                                    </div>
                                </div>
                            </div>

                            <div className="smm_secondlast_section">
                                <div className="smm_img_text_section">
                                    <img src={upgradedtechnology} alt="upgradedtechnology" loading="lazy" />
                                    <div>
                                        <h3>Upgraded Technology</h3>
                                        <p>We are very aware of the negative impact downtime may have on your website. In order to deal with these unpredictable conditions, we use the most up-to-date technologies. In order to handle the increased traffic on the website, it contains updated RAM and E5 processors as well as storage technologies.</p>
                                    </div>
                                </div>
                                <div className="smm_img_text_section">
                                    <img src={support} alt="support" loading="lazy" />
                                    <div>
                                        <h3>24/7 Support</h3>
                                        <p>We have a team of IT specialists working with us. These professionals have a thorough knowledge of the business and are available to fulfil the diverse needs of customers around the clock. It allows you to focus on your company rather than spending your time managing and resolving problems with your cheap windows VPS server.</p>
                                    </div>
                                </div>
                            </div>


                        </Hidden>
                        <Hidden lgUp>
                            <div>
                                <div className="smm_img_text_section smm_texttt">
                                    <img src={scalability} alt="scalability" loading="lazy" />
                                    <div>
                                        <h3>Scalability</h3>
                                        <p>Our VPS Hosting plans are completely customisable. You don't need to stress about your business's demands fluctuating since our hosting plans can always be customised as per your requirements, including storage, bandwidth, and networking speed.</p>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div className="smm_img_text_section smm_texttt">
                                    <img src={security} alt="security" loading="lazy" />
                                    <div>
                                        <h3>Security</h3>
                                        <p>When it comes to websites, security is one of the most important requirements to have. A website may function properly even at a slow speed, but it cannot function well with inadequate data security measures in place. Our security measures include visitor escorts, biometric scanners and secondary authentication, all of which are implemented with great care.</p>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div className="smm_img_text_section smm_texttt">
                                    <img src={datacenter} alt="datacenter" loading="lazy" />
                                    <div>
                                        <h3>Armored Data Centers</h3>
                                        <p>Navicosoft is committed to providing UK's most affordable VPS hosting. Our data centres are equipped with comprehensive heating, ventilation, and air conditioning systems. Additionally, backup and recovery systems are monitored 24/7/365.</p>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div className="smm_img_text_section smm_texttt">
                                    <img src={maximumuptime} alt="maximumuptime" loading="lazy" />
                                    <div>
                                        <h3>Maximum uptime</h3>
                                        <p>We provide a data centre that is network-independent. Our team makes it a priority to offer our customers the possible advantage of virtual server hosting. We guarantee 99.99% uptime to make your website accessible at all times.</p>
                                    </div>
                                </div>
                            </div>
                            <div>

                                <div className="smm_img_text_section smm_texttt">
                                    <img src={upgradedtechnology} alt="upgradedtechnology" loading="lazy" />
                                    <div>
                                        <h3>Upgraded Technology</h3>
                                        <p>We are very aware of the negative impact downtime may have on your website. In order to deal with these unpredictable conditions, we use the most up-to-date technologies. In order to handle the increased traffic on the website, it contains updated RAM and E5 processors as well as storage technologies.</p>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div className="smm_img_text_section smm_texttt">
                                    <img src={support} alt="support" loading="lazy" />
                                    <div>
                                        <h3>24/7 Support</h3>
                                        <p>We have a team of IT specialists working with us. These professionals have a thorough knowledge of the business and are available to fulfil the diverse needs of customers around the clock. It allows you to focus on your company rather than spending your time managing and resolving problems with your cheap windows VPS server.</p>
                                    </div>
                                </div>
                            </div>
                            <div>

                            </div>
                        </Hidden>
                    </div>
                </div>

                <div className="web_hosting">
                    <div className="reliable_webhosting">
                        <h2>Why Navicosoft for <br />VPS Hosting in United Kingdom</h2>
                        <Grid container spacing={3} className="flipsection_style">
                            <Grid item xs={12} sm={3}>
                                <Flippy
                                    flipOnHover={true}
                                    flipDirection="horizontal">
                                    <FrontSide>

                                        <p>We are driven by the desire to provide our customers with latest technological solutions.</p>
                                    </FrontSide>
                                    <BackSide>

                                        <p>We are driven by the desire to provide our customers with latest technological solutions.</p>
                                    </BackSide>
                                </Flippy>
                            </Grid>
                            <Grid item xs={12} sm={3}>
                                <Flippy
                                    flipOnHover={true}
                                    flipDirection="horizontal">
                                    <FrontSide>

                                        <p>We secure your website with our enhanced security and reliable connection.</p>
                                    </FrontSide>
                                    <BackSide>

                                        <p>We secure your website with our enhanced security and reliable connection.</p>
                                    </BackSide>
                                </Flippy>
                            </Grid>
                            <Grid item xs={12} sm={3}>
                                <Flippy
                                    flipOnHover={true}
                                    flipDirection="horizontal">
                                    <FrontSide>

                                        <p>We facilitate you with our 24/7/365 technical support.</p>
                                    </FrontSide>
                                    <BackSide>

                                        <p>We facilitate you with our 24/7/365 technical support.</p>
                                    </BackSide>
                                </Flippy>
                            </Grid>
                            <Grid item xs={12} sm={3}>
                                <Flippy
                                    flipOnHover={true}
                                    flipDirection="horizontal">
                                    <FrontSide>

                                        <p>We secure your sensitive data from cyber-attacks.</p>
                                    </FrontSide>
                                    <BackSide>

                                        <p>We secure your sensitive data from cyber-attacks.</p>
                                    </BackSide>
                                </Flippy>
                            </Grid>
                        </Grid>
                    </div>
                </div>

                <div className="confess_section">
                    <div className="confess_text">
                        <h2><span className="heading_color">Server Clustering </span><br />For VPS Hosting</h2>
                        <p>Navicosoft offers latest technology for VPS server hosting and clustering. The clustering has a self-healing feature. This implies that if a server goes down, the virtual machines operating on it will be mounted immediately on another server. Additionally, we provide live website migrations with no downtime. Our SSD storage arrays provide easy clustering of VPS.</p>
                    </div>
                    <div className="confess_img">
                        <img src={cloudserver} alt="cloudserver" />
                    </div>
                </div>
            </div>

        );
    }


}

export default VpsHosting;