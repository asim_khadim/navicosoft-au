import React, { Component } from 'react';


class CloudBackupFAQ extends Component {
    render() {
      return (
            <div className="digitalmarkettingfaq">
               <h2>FAQ’s</h2>
                <div>
                    <h3>What is meant by Cloud Backup?</h3>
                    <p>Cloud backup refers to the process of backing up your files to the cloud. In other terms, it refers to the process of creating a backup copy of your data, programmes, virtual machines, or servers and securely storing them over a remote network. Cloud backup is a fundamental need for many businesses nowadays since the majority of information, business-critical data, and applications are stored on a cloud server.</p>
                </div>
                <div>
                    <h3>Is Cloud storage a safe mode of storing data?</h3>
                    <p>To be sure, it's reasonable to worry if your data is secure in cloud storage. The majority of individuals believe that cloud storage is insecure against cyber-attacks. However, the reality is that data stored in cloud storage is way safer than data stored on a computer's hard disc. Cloud storage provides you with regular security updates, built-in firewalls, data encryption, and various other security measures to safeguard your data.</p>
                </div>
                <div>
                    <h3>How frequently should I do cloud data backup?</h3>
                    <p>It is not a hard and fast rule to do backups on a weekly or monthly basis; it is entirely dependent on your requirements. Indeed, it differs per user. However, it is a recommended habit to do a weekly free cloud backup. If you regularly create and change files, you should do backups even more frequently. We offer continuous backup to ensure that your data and freshly generated, updated, or deleted files are continuously monitored.</p>
                </div>
                <div>
                    <h3>Is my cloud storage accessible to anybody other than me?</h3>
                    <p>We recognise that everyone is worried about their privacy and security. As a result, you can be confident that no one else will have access to your cloud storage or data. Encryption is needed to protect your data from hackers or any cyber-attacks. We use the best encryption available to guarantee the security of your data. Additionally, our data centers in the UK adhere to strict privacy regulations and are well trusted. When you select us for cloud storage, you can be certain that our excellent security procedures will keep you secure from hackers.</p>
                </div>
               
            </div>
      );
    }
  }
  
  export default CloudBackupFAQ;
