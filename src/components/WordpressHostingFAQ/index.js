import React, { Component } from 'react';
import { faqs } from './faqs'
import './style.scss';

class WordpressHostingFAQ extends Component {
    render() {
        return (
            <div className="full_width_background_faq wordpress_hosting">
            <div className="digitalmarkettingfaq businessemailsolutionfaq">
                <h2>FAQ's</h2>
                {faqs.map((faq) => {
                    return (
                        <div key={faq.id}>
                            <h3>{faq.question}</h3>
                            <p>{faq.answer}</p>
                        </div>

                    )
                })}
            </div>
            </div>
        );
    }
}

export default WordpressHostingFAQ;
