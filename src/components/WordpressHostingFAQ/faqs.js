export const faqs = [
    {
        id: '1',
        question: 'Why is WP Hosting Security Important?',
        answer: 'Thousands of automated viruses prowl the internet. These viruses make an effort to compromise the security of your website and reroute visitors to third-party websites. Not only will WordPress Hosting Security secure your WordPress site from harmful attacks, but it will also protect it from third-party redirection. You may benefit from our most reliable managed WordPress hosting in the UK, and let us worry about the security of your website.'
    },
    {
        id: '2',
        question: 'Why should I choose Navicosoft for my WordPress Hosting Provider in the UK?',
        answer: 'Navicosoft has over 10 years of expertise delivering the best WordPress Hosting services. Additionally, we always come up with solutions that keep us high above our competitors. We provide dedicated sessions to guarantee that our customers are satisfied with their WordPress hosting. Additionally, we have developed one of the finest award-winning support centres in the UK to provide 24/7/365 support to our customers across the country.'
    },
    {
        id: '3',
        question: 'Can do anything about my hacked WordPress site? ',
        answer: 'If your website is hacked, but you are not one of our clients, we will still provide our best solutions. Indeed, we will thoroughly clean and reassemble your website. Regardless of the severity of the attack, we will make every effort to backup your website completely.'
    },
    {
        id: '4',
        question: 'How much uptime do you guarantee with your managed WordPress Hosting?',
        answer: 'Navicosoft guarantee an uptime of up to 99% when you get WordPress hosting from Navicosoft. Your website will remain operational throughout. However, we must schedule a time frame for the migration. During that time, you will be unable to access your admin dashboard, but your visitors will continue to view the website.'
    },
    {
        id: '5',
        question: 'Which kind of websites are supported by WordPress Hosting?',
        answer: 'We offer managed WordPress hosting in the UK for WordPress-based websites. If your website is not already running on WordPress, our specialised team will do the necessary conversion. Aside from that, we provide hosting for a wide range of websites of varying sizes, including blogging sites, woocommerce sites, and many more.'
    },
    {
        id: '6',
        question: 'What distinguishes Navicosoft from other hosting providers in the UK?',
        answer: 'Our WordPress hosting services are all one-of-a-kind, both in terms of functionality and pricing approach. Other web hosting companies will host your website on a single server that will host thousands of different websites. If anything goes wrong, they will not provide you with comprehensive technical assistance. As your most acceptable hosting partners, we make sure to offer you technical service 24/7/365, as well as constant monitoring to guarantee that your website is up and running properly.'
    },
];