import ReactHtmlParser from "react-html-parser";
import { products } from "./products";
import React from "react";
import { Link } from 'react-router-dom';
import Button from "@material-ui/core/Button";
function Products({
  product_id,
  cycle,
  imgsrc,
  product_title,
  perprice,
  classes,
  btn_link, 
  action,
  description,
  group_id
}) {

  const web_hosting_home_page_pkgs = products.map((product, i) => {
    if (parseInt(product.pid) === parseInt(product_id)) {
      return (
        <li className={classes}>
          <div className="table_sec">
            <div>
              {" "}
              <img alt="web hsoting" src={imgsrc} />
            </div>
            <div className="table_details">
              <div className="purchase-description">{product_title}</div>
              <div className="purchase-price">
                ${perprice === 'mo' ? product.pricing.AUD.monthly : parseFloat(product.pricing.AUD.annually / 12).toFixed(2)}/mo
              </div>
            </div>
          </div>
          <div className="text_table">
            <p>{description}</p>
            <Link to={btn_link}>
              <button type="button" className="btn_table">
                <a>Choose Plan</a>
              </button>
            </Link>
          </div>
        </li>
      );
    }
  });

  const shared_hosting_pkgs = products.map((product, i) => {
    if (parseInt(product.gid) === parseInt(group_id)) {
      return (

        <li className={classes}>
          <div className="table_sec">
            <div> <img alt={product_title} src={imgsrc} alt="Navicosoft Packages" /></div>
            <div className="table_details">
              <div className="purchase-description">{product.name}</div>
              <div className="purchase-price">
                ${parseFloat(product.pricing.AUD.annually / 12).toFixed(2)}/{perprice}</div>

            </div>
          </div>
          {/* <div className="text_table">
            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam</p>
          </div> */}
          <div>
            <ul className="table_content">
              {ReactHtmlParser(product.description)}
            </ul>
          </div>
          <button onClick={event => window.location.href = 'https://register.navicosoft.com/cart.php?a=add&currency=4&pid=' + product.pid} className="btn_table" >
            <a>  Choose Plan </a>
            {/* <a href={'https://register.navicosoft.com/cart.php?a=add&currency=4&pid=' + product.pid}>  Choose Plan </a> */}
          </button>
        </li >
      )
    }
  });

  const dedicated_hosting_pkgs = products.map((product, i) => {
    if (parseInt(product.gid) === parseInt(group_id)) {
      return (

        <li className={classes}>
          <div className="table_sec">
            <div> <img alt={product_title} src={imgsrc} alt="Navicosoft Packages" /></div>
            <div className="table_details">
              <div className="purchase-description">{product.name}</div>
              <div className="purchase-price">
                ${product.pricing.AUD.monthly}/{perprice}</div>
            </div>
          </div>
          {/* <div className="text_table">
            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam</p>
          </div> */}
          <div>
            <ul className="table_content">
              {ReactHtmlParser(product.description)}
            </ul>
          </div>
          <button onClick={event => window.location.href = 'https://register.navicosoft.com/cart.php?a=add&currency=4&pid=' + product.pid} className="btn_table" >
            <a>  Choose Plan </a>
          </button>
        </li>
      )
    }
  });

  if (action === "homePage") {
    return (<React.Fragment>
      {web_hosting_home_page_pkgs}
    </React.Fragment>);
  }
  if (action === "sharedHostingPlans") {
    return (<React.Fragment>
      {shared_hosting_pkgs}
    </React.Fragment>);
  }

  if (action === "dedicatedHostingPlans") {
    return (<React.Fragment>
      {dedicated_hosting_pkgs}
    </React.Fragment>);
  }
}

export default Products;
