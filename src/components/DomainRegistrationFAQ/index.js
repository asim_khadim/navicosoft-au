import React, { Component } from 'react';

import './style.scss';

class DomainRegistrationFAQ extends Component {
    render() {
        return (
            <div className="digitalmarkettingfaq">
                <h2>Your Questions? Our Answers </h2>
                <div>
                    <h3>How do I register a domain name?</h3>
                    <p>Firstly, you must choose a domain name extension such as .com, .co.uk, or .com.au from among the many available options. Remember that this must be linked to your services, so always be sure to choose the one that best fits the image you want to project when people think of your company. When doing a domain search, certain recommended extensions, such as.club and.org, are always available to you. In the search bar at the top of this page, you may enter your ideal domain name to find it.<br /><br />
                        A domain with the “.co.uk” extension is required if your company is located in the UK since it is the most frequent and most commonly used top-level domain (TLD) extension. It also plays a vital role in the growth of your brand on a local level and promotes your company as a UK-based enterprise. Furthermore, the addition of various new domain extensions, such as “.london” and “.wales,” can help you draw attention to your targeted audience in a particular city.<br /><br />
                        It is necessary to choose a domain name once you have selected an extension for your website.<br /><br />
                        Prior to beginning the domain registration procedure, it is highly recommended that your domain name should be concise, simple to remember, and most importantly, relates to your business services or products. This is something that makes it simple to find and tough to forget your domain name.<br /><br />
                        Once you are sure about your domain name, you can browse it in our domain search bar above to see all available options. If your required domain is available, you can purchase it immediately; otherwise, we will show you a list of all relevant domains for your business niche.
                        </p>
                </div>
                <div>
                    <h3>How long am I allowed to register a domain name?</h3>
                    <p>Registration of domain names is determined by the registrar, which establishes specific regulations for specific extension types. Typically, domain registrations last between one and ten years. You can browse all domain TLDs and obtain detailed information about any extension that best suits you.<br /><br />
                        Most of the domain registration in the UK extension (TLDs such as.co.uk, .org.uk, and .uk) has registration periods of two years.
                        </p>
                </div>
                <div>
                    <h3>Is it possible to register domain name with Navicosoft and host is elsewhere?</h3>
                    <p>Yes, you just need to set up web forwarding, which you can accomplish by updating DNS records or configure quickly if you are utilising our DNS's rapid setup option.</p>
                </div>
                <div>
                    <h3>How long does it take to register a domain name?</h3>
                    <p>The majority of the time, the domain registration procedure is finished in a matter of seconds. You receive a verification email at the start of the process; once everything is being completed, you will get a confirmation email.<br /><br />
                    Before your website becomes online and accessible through the internet, it is necessary to change the global DNS. These databases enable various machines to lookup an IP address based on its web address. Once this is complete, your website and associated files will be accessible to visitors through the internet.
                    </p>
                </div>
                <div>
                    <h3>Is it possible to transfer a domain name from one company to another?</h3>
                    <p>If you are using our free basic DNS service, you must modify the A record. On the administration page, go to the DNS Settings section and add records by clicking the menu and selecting "Add record" from the drop-down menu.<br /><br />
                        This requires that you know the IP address of the service or website to which you want to redirect traffic. It may be located using the domain name or IP address in the majority of search engines. Otherwise, this information should be accessible from the hosting company for your website. To determine your website's IP address, see their tutorials or documentation.<br /><br />
                        Check to verify that your server or website is ready to receive the forwarding request. You must update your nameservers to point to the nameservers of the sites to which you want to redirect traffic.
                        </p>
                </div>

            </div>
        );
    }
}

export default DomainRegistrationFAQ;
