import React from 'react';
import styled from 'styled-components';
import './style.scss';
import { Controller, Scene } from 'react-scrollmagic';
import { Hidden } from '@material-ui/core';

const StickyStyled = styled.div`
  .section {
    height: 100vh;
  }
  
  .sticky {
    color: white;
    width: 100%;
    & div {
    //   padding: 30px;
    }
  }
  
//   .blue {
//     background-color: blue;
//   }
`;

const Sticky = () => (
  <StickyStyled>
    <Hidden mdDown>
      <div className="section_two_background">
    <div className="full_section">
        <div className="top_section">
        <h2 className="heeader_first">LET'S TALK BUSINESS</h2>
        <p className="para_first">Digital marketing agency that< br /> guarantee lead generation for your business</p>
        </div>
        <div className="scroller_section">
            <div className="left_section">
            <Controller>
            <Scene pin={true} duration={1750} enabled={true}>
                    <div className="intro--extralarge"><div className="text">We are an experienced team of digital marketing experts in Britain with having a track record of business generation through the best digital marketing and web development services.</div></div>
                </Scene>
            </Controller>
            </div>
            <div className="right_section">
            <Controller>
                <Scene duration={100} pin={true} enabled={true} reverse={true} offset={500} classToggle='fade_color'>
                    <div className="sticky">
                      <div>
                        <p className="text-white large-title">Web Hosting</p>
                          <p className="para">We are a UK-based web hosting company that offers you the best customised web hosting packages with managed cloud services. Give a try to our affordable web hosting packages with secure lightning speed SSD based servers with 24/7 technical support.</p>
                         
                      </div>
                    </div>
                </Scene>
                <Scene duration={100} pin={true} offset={500} reverse={true} classToggle='fade_color'>
                    <div className="sticky"><div>
                    <p className="text-white large-title">SEO</p>
                         <p className="para">We are expert enough to ensure that your business is ranked on top of search engine by optimizing your website and increasing its search ranking using our advanced SEO techniques. We offer the most cost-effective yet organic way to increase your website trafficking.</p>
                            
                        </div></div>
                </Scene> 
                <Scene duration={100} pin={true} offset={500}>
                    <div className="sticky blue"><div>
                    <p className="text-white large-title">PPC</p>
                         <p className="para">Don’t stress about finding clients; they’ll come to you! Our expert team conduct extensive research into your target audience in every region and create well-structured campaigns for you. This will nail down your higher ROI and more potential clients. </p>
                       
                        </div></div>
                </Scene>
                <Scene duration={100} pin={true} offset={500}>
                    <div className="sticky blue"><div>
                    <p className="text-white large-title">Social Media Marketing </p>
                         <p className="para">Voice out to clients! Our SMM specialists ensure to make your business noticeable on all social media platforms. We will make sure that that your social profile is prominent enough to generate sales and leads. We use all social media channels quite efficiently for advertising your business.</p>
                       
                        </div></div>
                </Scene>
                <Scene duration={100} pin={true} offset={500}>
                    <div className="sticky blue"><div>
                    <p className="text-white large-title">Web Design </p>
                         <p className="para">Our talented team of UI and UX designers knows the arts and trends of British businesses. We make your website look professional by creating well-versed content with highly responsive and user friendly design.</p>
                       
                        </div></div>
                </Scene>
                <Scene duration={100} pin={true} offset={500}>
                    <div className="sticky blue"><div>
                    <p className="text-white large-title">Web Development </p>
                         <p className="para">We believe in developing an engaging website for every visitor so that you never miss out on orders. Our best web developers make your website stays ahead of your competitors. We always use up-to-date technology to code your website.</p>
                       
                        </div></div>
                </Scene>
                <Scene duration={100} pin={true} offset={500}>
                    <div className="sticky blue"><div>
                    <p className="text-white large-title">Google Ads </p>
                         <p className="para">Navicosoft values every single penny; therefore, our experts will build campaigns and strategies to gain high output with less investment. We track every single keyword on your website until you get results. </p>
                       
                        </div></div>
                </Scene>
                </Controller>
            </div>
          </div>
        </div>
        </div>
        </Hidden>
        <Hidden lgUp>
           <div item xs={12} sm={12}>
               <h2 className="title_h2">LET'S TALK BUSINESS</h2>
               <p className="title_para">Digital marketing agency that<br /> guarantee lead generation for your business</p>
               <p className="title_para">Heading: We are an experienced team of digital marketing experts in Britain with having a track record of business generation through the best digital marketing and web development services.</p>
                <div className="row">
                  <div className="col">
                    <div className="tabs">
                      <div className="tab">
                        <input type="radio" id="rd1" name="rd" />
                        <label className="tab-label" for="rd1">Web Hosting</label>
                        <div className="tab-content">
                          <p>We are a UK-based web hosting company that offers you the best customised web hosting packages with managed cloud services. Give a try to our affordable web hosting packages with secure lightning speed SSD based servers with 24/7 technical support.</p>
                        </div>
                      </div>
                      <div className="tab">
                        <input type="radio" id="rd2" name="rd" />
                        <label className="tab-label" for="rd2">SEO</label>
                        <div className="tab-content">
                          <p>We are expert enough to ensure that your business is ranked on top of search engine by optimizing your website and increasing its search ranking using our advanced SEO techniques. We offer the most cost-effective yet organic way to increase your website trafficking.</p>
                        </div>
                      </div>
                      <div className="tab">
                        <input type="radio" id="rd3" name="rd" />
                        <label className="tab-label" for="rd3">PPC</label>
                        <div className="tab-content">
                          <p>Don’t stress about finding clients; they’ll come to you! Our expert team conduct extensive research into your target audience in every region and create well-structured campaigns for you. This will nail down your higher ROI and more potential clients. </p>
                        </div>
                      </div>
                      <div className="tab">
                        <input type="radio" id="rd4" name="rd" />
                        <label className="tab-label" for="rd4">Social Media Marketing</label>
                        <div className="tab-content">
                          <p>Voice out to clients! Our SMM specialists ensure to make your business noticeable on all social media platforms. We will make sure that that your social profile is prominent enough to generate sales and leads. We use all social media channels quite efficiently for advertising your business</p>
                        </div>
                      </div>
                      <div className="tab">
                        <input type="radio" id="rd5" name="rd" />
                        <label className="tab-label" for="rd5">Web Design</label>
                        <div className="tab-content">
                          <p>Our talented team of UI and UX designers knows the arts and trends of British businesses. We make your website look professional by creating well-versed content with highly responsive and user friendly design.</p>
                        </div>
                      </div>
                      <div className="tab">
                        <input type="radio" id="rd6" name="rd" />
                        <label className="tab-label" for="rd6">Web Development</label>
                        <div className="tab-content">
                          <p>We believe in developing an engaging website for every visitor so that you never miss out on orders. Our best web developers make your website stays ahead of your competitors. We always use up-to-date technology to code your website.</p>
                        </div>
                      </div>
                      <div className="tab">
                        <input type="radio" id="rd7" name="rd" />
                        <label className="tab-label" for="rd7">Google Ads</label>
                        <div className="tab-content">
                          <p>Navicosoft values every single penny; therefore, our experts will build campaigns and strategies to gain high output with less investment. We track every single keyword on your website until you get results. </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
        </Hidden>
    
  </StickyStyled>
);

export default Sticky;