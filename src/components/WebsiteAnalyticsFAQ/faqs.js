export const faqs = [
    {
        id: '1',
        question: 'What is Google Analytics and how can you integrate it into your website?',
        answer: 'Google website analytics is undoubtedly the most useable web analytics product in the world to monitor website traffic. To put it simply, it gives all of the information about visitors to your website, including their origin and behaviour, in order to track their presence on your website. In addition, it offers more complex and valuable information to help you develop your company at the advanced level.'
    },
    {
        id: '2',
        question: 'What are the benefits of using web analytics tools to improve your business?',
        answer: 'Google website analytics, in conjunction with a web analytics tool, assists you in improving your website and generating more income by tracking the actions and areas of interest of your visitors. When it comes to online marketing, Google Analytics and web analytics tools should be at the heart of your strategies since they provide result-driven data and statistics for all your website pages.'
    },
    {
        id: '3',
        question: 'Can Google Analytics squarespace assist me in making financial savings?',
        answer: 'The answer is yes, provided that it is appropriately utilised! In order to save money, it is necessary to identify the marketing channels while interpreting the data inside Google website analytics. If your website is listed on a third-party channel, you may be paying thousands of pounds. We will assist you in determining how much traffic you are receiving from a particular link and whether or not you should discontinue paying money on that connection. In addition, we will provide you with free website analytics.'
    },
    {
        id: '4',
        question: 'What data do I get access to when I integrate Google Analytics into my website?',
        answer: 'When you integrate Google Analytics into your website, you get access to all pertinent data about client visits and detailed insight into every element of your site performance. Google web analytics reports are generally classified into four types: audience reports, behaviour reports, acquisition reports, and conversion reports. Therefore, you can be confident that Google Analytics will provide you with all of this essential information.'
    },
]