import React, { Component } from 'react';

import './style.scss';

class WebDevelopmentFAQ extends Component {
    render() {
      return (
            <div className="digitalmarkettingfaq">
               <h2>FAQ’s</h2>
                <div>
                    <h3>How long do we take to develop a complete functional website?</h3>
                    <p>Our expert team of developers begin working on your project immediately upon confirmation. We offer you custom-built websites at the most affordable price and in the shortest amount of time feasible. Our Google-certified developers are dedicated to completing the task promptly. Therefore, do not be concerned about the timetable, as we are always available to assist you and fulfil even the most stringent deadlines!</p>
                </div>
                <div>
                    <h3>Do you offer website renovation services?</h3>
                    <p>We are constantly developing new design patterns to suit your demands and specifications. Additionally, we redesign outdated websites in such a manner that they make a lasting impression on visitors. Before creating any website, we guarantee that appropriate prototypes are provided. After you have approved the mockups, we will begin designing the website. Additionally, we deliver documents to improve our communication with you!</p>
                </div>
                <div>
                    <h3>What is the main difference between static and dynamic website?</h3>
                    <p>Static web pages are those that remain unchanged throughout time. To make a modification, you must create a new page. Dynamic websites, on the other hand, are diametrically opposed. They may be modified at any moment throughout the loading process. Additionally, they often store and retrieve data from an existing database. Due to technological advancements, dynamic websites have become more useful as well as demanding.</p>
                </div>
                <div>
                    <h3>How many pages should I have for my website? </h3>
                    <p>Well, it is entirely up to you. We operate in accordance with your specifications and demands. If you need a few pages for your website, we can offer them to you. However, if you need a large number of pages, we will get them done. We can create pages for your website ranging from one to an infinite number. Additionally, you may contact us later to request additional pages.</p>
                </div>
                <div>
                    <h3>Do you provide technical support?</h3>
                    <p>Navicosoft is committed to providing you with the highest quality service possible. To guarantee that you get prompt assistance if you run into a technical issue, our courteous technical support team is available 24/7. If you choose us for your web development or web hosting, we guarantee that we will be available 24/7/365. We are always available to resolve any problems with your site relating to downtime, loading speed or anything else.</p>
                </div>
                <div>
                    <h3>How do you charge for my website development?</h3>
                    <p>This is the most common question from our new customers. In fact, the price of a website varies depending on its features. Our charges increase with more features you would like to add. We always charge the lowest pricing for websites, so you don't have to worry about that. Due to our affordable rates, we are one of the UK's most cost-efficient website development companies. We also provide an option of paying in several stages as it helps our customers avoid becoming overburdened.</p>
                </div>
               
               
            </div>
      );
    }
  }
  
  export default WebDevelopmentFAQ;
