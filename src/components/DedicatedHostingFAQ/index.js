import React, { Component } from 'react';

import './style.scss';

class DedicatedHostingFAQ extends Component {
    render() {
      return (
          <div className="digitalmarkettingfaq_background">
            <div className="digitalmarkettingfaq webdesign_faq">
               <h2>FAQs</h2>
                <div>
                    <h3>What is a cheap dedicated server?</h3>
                    <p>A dedicated server is primarily like a computer that is connected to a network. You have complete control over all of the resources on the dedicated server. It is not necessary for you to share it with anybody else. Dedicated servers are extremely configurable, allowing you to tailor them to your specific requirements. You are granted complete server access, allowing you to handle all your available resources.</p>
                </div>
                <div>
                    <h3>Why should I go for Navicosoft to get a dedicated server in the United Kingdom?</h3>
                    <p>Navicosoft has built a reputation in the IT industry by providing top-quality dedicated hosting services since 2008. We have assisted hundreds of individuals and businesses in bringing their projects online while providing them 24/7/365 assistance. We make every single effort to help you grow economically and technically.</p>
                </div>
                <div>
                    <h3>Why should I select a dedicated server over a shared server?</h3>
                    <p>Using a dedicated server, you have complete command over how your operations are managed. Furthermore, you may adjust the performance of your server following the demands of your most demanding applications as well as your performance needs. When using a shared server, you must share the server with other users, which poses several security and performance concerns.</p>
                </div>
                <div>
                    <h3>Is it possible to access cheap dedicated server using RDP and SSH?</h3>
                    <p>Yes it is. You will be able to control all your administrative operations using a dedicated server. When you purchase a cheap dedicated server from us, we make certain that you have full remote access to your server. Aside from that, we make certain to provide you with root access as well as SSH access. You will undoubtedly be able to handle each procedure with ease!</p>
                </div>
                <div>
                    <h3>Are automatic backups included in the price of dedicated server hosting?</h3>
                    <p>When it comes to dedicated server hosting, we certainly offer backups. Whatever your preference is for automatic backups or taking manual backups, it is entirely up to you! We also make certain to provide you with a one-click backup-restore feature, allowing you to restore your data in accordance with your time management requirements.</p>
                </div>
                <div>
                    <h3>What is the uptime reliability of Navicosoft’s dedicated servers?</h3>
                    <p>Navicosoft is concerned about your company and the significance of internet business in your life. We guarantee 99.99% uptime, which is the highest level in the industry. Our dedicated servers are specifically built to offer the highest possible uptime while experiencing the least amount of downtime. Furthermore, we provide dedicated server hosting with all kinds of Linux and Windows configurations available.</p>
                </div>
               
            </div>
            </div>
      );
    }
  }
  
  export default DedicatedHostingFAQ;
