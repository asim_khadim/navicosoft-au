export const faqs = [
    {
        id: '1',
        question: 'What is meant by Business Stationery?',
        answer: 'Business stationery refers to the design of letterheads, brochures, booklets, logos, flyers, etc. The distinctive stationery design will assist your consumers in recognising and identifying your company and brand as a result of an appealing stationery appearance.'
    },
    {
        id: '2',
        question: 'What does Navicosoft provide with Business Stationery Design in the UK?',
        answer: 'Navicosoft design team is always available for your help. We guarantee the most contemporary, original, and creative artwork. We also guarantee to supply everything you need for printing or production. We will upload your corporate business stationery designs in the format you choose, which includes CMYK, PDF, and EPS files. We also provide you with web preview files like JPEG and PNG.'
    },
    {
        id: '3',
        question: 'Do I need to provide any design for my company business stationery?',
        answer: 'To be sure, we provide you full power to make all of your own decisions. If you already have an appropriate design for your company  business stationery, that is OK. However, it is also fine if you do not have any. We are here to assist you throughout the process with a dedicated team. Navicosoft has extensive expertise in designing corporate stationery, business stationery, logos, and marketing material.'
    },
    {
        id: '4',
        question: 'How much does Navicosoft charge for stationery design?',
        answer: 'Even though we provide a broad range of design services, our packages are competitive and reasonable. Additionally, we provide customisation to suit your specific requirements. Additionally, our team constantly guarantees that you are pleased before finalising anything. We are always here to welcome you!'
    },
    {
        id: '5',
        question: 'How many design revisions am I permitted to make?',
        answer: 'Once you have received a design from Navicosoft, you may request multiple revisions. The revision count varies per package, but we are here to assist you in iterating the design till you are happy with it. Once you have entrusted our designers with your design idea, we make every effort to exceed your expectations. However, if you are dissatisfied, we allow for further rounds until the required results are obtained.'
    },
    {
        id: '6',
        question: 'Can you provide me with design ideas?',
        answer: 'Why not! We are well aware of the customer perceptions about a brand. Therefore, to keep you ahead of your competitors in creating appealing business stationery for your business, we provide you with an extensive selection of suitable designs. We offer a variety of design concepts to showcase precise artwork. As a result, when you commission us to create your company stationery, you can expect to get an abundance of design concepts.'
    }
]
