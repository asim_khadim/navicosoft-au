// Complete React Code 
import React, { useEffect } from 'react'; 
import './style.scss';
import Particles from 'react-particles-js'; 
import {Grid} from "@material-ui/core";
import { Container } from '@material-ui/core';
import { Component } from 'react';

class Particles1 extends Component { 

  constructor(props) {
    super(props);
    this.state = { matches: window.matchMedia("(min-width: 768px)").matches };
  }

  componentDidMount() {
    const handler = e => this.setState({matches: e.matches});
    window.matchMedia("(min-width: 768px)").addListener(handler);
  }
  
  render() { 
    return (
      <div >
      {this.state.matches && (<div className="Particles"> 
      <div className="home-slide-content red">
    <div className="word-container">
      <div className="word-wrapper word-wrapper-elle">
        <div className="word elle"><span className="hide">
            <div className="hide-right">We u</div>
          </span> <span className="hide">p</span> <span className="hide">
            <div className="hide-left">lift</div>
          </span></div>
      </div>
      <div className="word-wrapper word-wrapper-aime">
        <div className="word aime"><span className="hide">
            <div className="hide-right">bril</div>
          </span> <span className="hide">l</span> <span className="hide">
            <div className="hide-left">iancy</div>
          </span></div>
      </div>
    </div>

  {/* <div className="word-container">
      <div className="word-wrapper word-wrapper-elle">
        <div className="word elle"><span className="hide">
            
          </span> <span className="hide">We </span> <span className="hide">
            <div className="hide-left"> uplift</div>
          </span></div>
      </div>
      <div className="word-wrapper word-wrapper-aime">
        <div className="word aime"><span className="hide">
            <div className="hide-right">brillian</div>
          </span> <span className="hide">cy</span> <span className="hide">
          </span></div>
      </div>
    </div> */}
  </div>
  <Particles
      params={{ "particles": { "number": { "value": 300 }, "color": { "value": "#000000" }, "shape": { "type": "circle", "stroke": { "width": 0, "color": "#000000" }}, "polygon": { "nb_sides": 5 }, "opacity": { "value": 0.5, "random": false, "anim": { "enable":
  false, "speed": 1, "opacity_min": 0, "sync": false } }, "size": { "value": 2, "random": false, "anim": { "enable": false, "speed": 10, "size_min": 0, "sync": false } }, "line_linked": { "enable": false, "distance": 50, "color": "#ffffff", "opacity": 0.4,
  "width": 1 }, "move": { "enable": true, "speed": 6, "direction": "none", "random": false, "straight": false, "out_mode": "out", "bounce": false, "attract": { "enable": false, "rotateX": 600, "rotateY": 1200 } } }, "interactivity": { "detect_on": "canvas",
  "events": { "onhover": { "enable": true, "mode": "repulse" }, "onclick": { "enable": true, "mode": "push" }, "resize": true }, "modes": { "grab": { "distance": 400, "line_linked": { "opacity": 1 } }, "bubble": { "distance": 400, "size": 40, "duration":
  2, "opacity": 8, "speed": 3 }, "repulse": { "distance": 79.92007992007991, "duration": 0.4 }, "push": { "particles_nb": 4 }, "remove": { "particles_nb": 2 } } },
    }} />
    </div>)}
      {!this.state.matches && (<div className="Particles mobile_particles"> 
      <div className="home-slide-content red">
    <div className="word-container-mobile">
      <div className="word_count">
          We Uplift Brilliancy
      </div>
    </div>

  
  </div>
  <Particles height={window.outerHeight}
      params={{ "particles": { "number": { "value": 100 }, "color": { "value": "#f7941e" }, "shape": { "type": "circle", "stroke": { "width": 0, "color": "#f7941e" }}, "polygon": { "nb_sides": 5 }, "opacity": { "value": 1, "random": false, "anim": { "enable":
  false, "speed": 1, "opacity_min": 0, "sync": false } }, "size": { "value": 2, "random": false, "anim": { "enable": false, "speed": 10, "size_min": 0, "sync": false } }, "line_linked": { "enable": false, "distance": 50, "color": "#ffffff", "opacity": 0.4,
  "width": 1 }, "move": { "enable": true, "speed": 6, "direction": "none", "random": false, "straight": false, "out_mode": "out", "bounce": false, "attract": { "enable": false, "rotateX": 600, "rotateY": 1200 } } }, "interactivity": { "detect_on": "canvas",
  "events": { "onhover": { "enable": true, "mode": "repulse" }, "onclick": { "enable": true, "mode": "push" }, "resize": true }, "modes": { "grab": { "distance": 400, "line_linked": { "opacity": 1 } }, "bubble": { "distance": 400, "size": 40, "duration":
  2, "opacity": 8, "speed": 3 }, "repulse": { "distance": 79.92007992007991, "duration": 0.4 }, "push": { "particles_nb": 4 }, "remove": { "particles_nb": 2 } } },
    }} />
    </div>)}
      </div>


     
    
  ); 
  }
} 

export default Particles1; 
