import React, { Component } from 'react';
import cloudbackup from '../../images/cloudbackup/cloudbackup.png';
import 'react-tabs/style/react-tabs.css';
import customplan from '../../images/cloudbackup/customplan.png';
import ultrafastbackup from '../../images/cloudbackup/ultrafastbackup.png';
import dedicatedaccountmanager from '../../images/cloudbackup/dedicatedaccountmanager.png';
import support from '../../images/cloudbackup/247support.png';
import ultrafastdashboard from '../../images/cloudbackup/ultrafastdashboard.png';
import tick from '../../images/tick.png';
import cloudimage from '../../images/cloudbackup/cloudimage.png';
import whynavicosectionimg2 from '../../images/whynavicosectionimg2.png';
import whynavicosectionimg3 from '../../images/whynavicosectionimg3.png';
import Grid from '@material-ui/core/Grid';
import ScrollAnimation from 'react-animate-on-scroll';
import "../../css/animate.min.css";
import './style.scss';
import { Hidden } from '@material-ui/core';
import { Link } from "react-router-dom";

class CloudBackUp extends Component {
    render() {
        return (
            <div className="dedicated_hosting linuxhosting cloud_backup">
                <ScrollAnimation animateIn='bounceInRight' animateOut='bounceOutLeft'>
                    <h1 className="slider_text">Scale your Cloud Backup <br /><span className="slidertext_color">Solutions With Us</span><br />
                        
                        <Link to="contact-us"><button className="btn_digitalmarketting" ><a>Get A Quote</a></button> </Link>
                    </h1>
                </ScrollAnimation>
                <div className="digital_marketing_image_section">
                    <img src={cloudbackup} className="first_img" alt="cloudbackup" loading="lazy" />
                </div>

                <div className="backorder_services_why">
                    <h2 className="sectthreee_heading">Our Most Reliable  <br /><span className="underline_heading">Cloud Backup Solution Got Your Back!</span></h2>
                    <p className="linux_para">Since 2008, Navicosoft has provided safe cloud backup solutions for small and medium-sized companies and large corporations. We are one of the most reliable cloud data backup solutions in London. Contact us now for more information. Backup and disaster recovery, cloud storage, and cloud data backup are just a few of the services that distinguish us from the competition. Moreover, our managed cloud backup solutions assist your companies in protecting their data from a wide range of security risks and vulnerabilities. We guarantee to combine reliable and easy backup and disaster recovery solutions with unwavering support with our monthly on-demand approach.<br /><br />On a predetermined timetable, we make a copy of your data and transfer it to our safe UK data centers, which are protected by industry-standard firewalls and other measures. We provide an easy-to-use, automatic, and reasonably priced solution for backing up your important data. Once your specified period has passed, we will encrypt and compress your data before sending it to one of our data. In addition, we provide cloud backup solutions ideal for companies that need data backup via a secure storage network.</p>
                </div>


                <div className="windows_server">
                <div className="backorder_services_why">
                    <h2 className="sectthreee_heading">Our Cloud Backup  <br />Solutions for Storing Media</h2>
                    <p className="text_ccenter_align">We at Navicosoft offer the most reliable cloud backup solutions for your company's data. We are confident in<br /> providing solutions ranging from basic cloud data backup to complete virtual boot and failover recovery with our<br /> advanced security features. We ensure your data protection by storing backup copies of your data in the most secure<br /> and technologically sophisticated data centers. We are aware that lost data and system failures may cause your<br /> company's operations to be halted. Our well-thought-out contingency plan protects your company's critical<br /> data as well as its documents and other files. Get our free cloud backup service and<br /> take advantage of the following benefits:</p>
                    <div className="box_section">
                        <div className="smm_box_text">
                            <div className="smm_arrow _text"> 
                                <p>The encryption key for customer data, <br />ensuring that only they have access to it</p>
                            </div>
                        </div>
                        <div className="smm_box_text">
                            <div className="smm_arrow _text">
                                <p>Restore chosen <br />files immediately.</p>
                            </div>
                        </div>
                        <div className="smm_box_text">
                            <div className="smm_arrow _text">  
                                <p>Data is encrypted and stored in <br />two-tiered secure data centers</p>
                            </div>
                        </div>
                    </div>
                    <div className="box_section below_sec">
                        <div className="smm_box_text">
                            <div className="smm_arrow _text"> 
                                <p>On the client side, military-grade <br />256-bit AES encryption is used</p>
                            </div>
                        </div>
                        <div className="smm_box_text">
                            <div className="smm_arrow _text">
                                <p>On the client side, deduplication <br />is applied to all backup sets.</p>
                            </div>
                        </div>
                    </div>
                </div>
                </div>

                <div className="vps_hosting">
                <div className="smm_agency_section">
                    <div className="inner_section">
                        <div className="smm_agency_text"> 
                            <h2>We Are Australia's Most Reliable <br /><span class="slidertext_color">Source for Cloud Backup Solutions</span></h2>
                            <p>Our Windows Server 2016 get you a physical server which is completely isolated from the rest of the network. Using blazing-fast Windows VPS servers, solid-state storage, and full root access, we enable you to manage an infinite amount of traffic on your website. Our cheap Windows VPS with SSD storage are built to withstand the needs of the most demanding resources.</p>
                        </div>
                        <Hidden mdDown>
                            <div className="smm_secondlast_section">
                                <div className="smm_img_text_section">
                                    <img src={customplan} alt="cloudbackup" loading="lazy" />
                                    <div>
                                        <h3>Custom Plans</h3>
                                        <p>Our cloud storage and cloud data backup solutions can be customised to meet your specific needs. You can now specify the amount of storage space that is necessary, along with the number of devices that will be used. Furthermore, you have the option of scheduling a cloud data backup at a certain time. </p>
                                    </div>
                                </div>
                                <div className="smm_img_text_section">
                                    <img src={ultrafastbackup} alt="cloudbackup" loading="lazy" />
                                    <div>
                                        <h3>Ultra-fast backup <br />and disaster recovery</h3>
                                        <p>We keep your downtime to a minimum by using the most efficient cloud backup options. Only one click is required to restore a file or the whole system to a new or previously used device or storage media type. It is our mission to be your backup partners, ensuring that you have access to storage and backup at all times.</p>
                                    </div>
                                </div>
                            </div>

                            <div className="smm_secondlast_section">
                                <div className="smm_img_text_section">
                                    <img src={ultrafastdashboard} alt="cloudbackup" loading="lazy" />
                                    <div>
                                        <h3>User-friendly <br />Dashboard</h3>
                                        <p>In order for you to spend less time maintaining the security and other aspects of your company data, we've made it simpler. You have complete control over the storage limit, the addition of new devices, and the editing of backup schedules.</p>
                                    </div>
                                </div>
                                <div className="smm_img_text_section">
                                    <img src={dedicatedaccountmanager} alt="cloudbackup" loading="lazy" />
                                    <div>
                                        <h3>Dedicated <br />Account Manager</h3>
                                        <p>Our personal cloud backup solutions allow you to benefit from our high-tech relationship as soon as you sign up for them. You can be absolutely at ease knowing that your data is being protected by our reliable data backup solutions and dedicated account manager.</p>
                                    </div>
                                </div>
                            </div>

                            <div className="smm_secondlast_section ">
                                <div className="smm_img_text_section">
                                    <img src={support} alt="cloudbackup" loading="lazy" />
                                    <div>
                                        <h3>24/7 Support</h3>
                                        <p>We are determined to meeting all of your technical assistance requirements as efficiently and effectively as possible. We are very much focused about the security of your data and cloud storage. Our support staff is always ready to go above and above to serve you better than the competition. So don't let any technological difficulties get the best of you since we are available 24/7/365.</p>
                                    </div>
                                </div>
                            </div>


                        </Hidden>
                        <Hidden lgUp>
                            <div>
                                <div className="smm_img_text_section smm_texttt">
                                    <img src={customplan} alt="cloudbackup" loading="lazy" />
                                    <div>
                                        <h3>Custom Plans</h3>
                                        <p>Our cloud storage and cloud data backup solutions can be customised to meet your specific needs. You can now specify the amount of storage space that is necessary, along with the number of devices that will be used. Furthermore, you have the option of scheduling a cloud data backup at a certain time.</p>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div className="smm_img_text_section smm_texttt">
                                    <img src={ultrafastbackup} alt="cloudbackup" loading="lazy" />
                                    <div>
                                        <h3>Ultra-fast backup <br />and disaster recovery</h3>
                                        <p>We keep your downtime to a minimum by using the most efficient cloud backup options. Only one click is required to restore a file or the whole system to a new or previously used device or storage media type. It is our mission to be your backup partners, ensuring that you have access to storage and backup at all times.</p>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div className="smm_img_text_section smm_texttt">
                                    <img src={ultrafastdashboard} alt="cloudbackup" loading="lazy" />
                                    <div>
                                        <h3>User-friendly <br />Dashboard</h3>
                                        <p>In order for you to spend less time maintaining the security and other aspects of your company data, we've made it simpler. You have complete control over the storage limit, the addition of new devices, and the editing of backup schedules.</p>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div className="smm_img_text_section smm_texttt">
                                    <img src={dedicatedaccountmanager} alt="cloudbackup" loading="lazy" />
                                    <div>
                                        <h3>Dedicated <br />Account Manager</h3>
                                        <p>Our personal cloud backup solutions allow you to benefit from our high-tech relationship as soon as you sign up for them. You can be absolutely at ease knowing that your data is being protected by our reliable data backup solutions and dedicated account manager.</p>
                                    </div>
                                </div>
                            </div>
                            <div>

                                <div className="smm_img_text_section smm_texttt">
                                    <img src={support} alt="cloudbackup" loading="lazy" />
                                    <div>
                                        <h3>24/7 Support</h3>
                                        <p>We are determined to meeting all of your technical assistance requirements as efficiently and effectively as possible. We are very much focused about the security of your data and cloud storage. Our support staff is always ready to go above and above to serve you better than the competition. So don't let any technological difficulties get the best of you since we are available 24/7/365.</p>
                                    </div>
                                </div>
                            </div>
                            <div>

                            </div>
                        </Hidden>
                    </div>
                </div>
                </div>

                <div className="google_ad">
                <div className="dedicated_background_section">
                
                    <div className="google_ad_service">
                        <div className="left_text">
                        <h2 className="sectthreee_heading mobile_stationary">Control your data <br />with our Cloud <br />Backup Solutions</h2>
                            <p className="para_background_linux">Navicosoft backup your essential files and data cost-effectively using cloud storage. We provide cloud backup solutions for small businesses and big corporations, allowing them to have full coverage and complete control over their data. In addition, it is possible to handle all types of data security inside a single process when using a hybrid workflow approach.< br /><br />
                            Additionally, with cloud backup options, obtaining more reliable backup solutions than RAID is now a no-brainer. You have complete control over backups of all storage components, including drives, files, folders, and virtual machines. Additionally, we do not restrict any number of file versions or the length of time a file may be stored. We offer the most reliable cloud storage system in London, and we are constantly improving and adding new features.</p>
                            
                        </div>
                        <div className="right_image">
                            <img src={cloudimage} className="first_img" alt="cloudbackup" loading="lazy" />
                        </div>
                    </div>
                </div>
                </div>

                <div className="windows_server background_white">
                <div className="backorder_services_why">  
                    <h2 className="sectthreee_heading">Get our Cloud Backup <br />Solutions & Say <span class="slidertext_color">No to Data <br />Deletion!</span></h2>
                    <p className="text_ccenter_align">Data deletions that occur suddenly, as well as malware assaults, may have a devastating impact <br />on your company's operations. Cyber-attacks on a daily basis have the potential to harm your <br />data, steal or modify your sensitive information. In the event of malicious activity being <br />detected, our active security system with cloud storage will restore the data to their most recent <br />clean backup. We keep your downtime to a minimum by providing the quickest data recovery <br />service. All of your data and files are accessible with a simple click. Recover the whole system of <br />your old computer or the entire system of your new machine from the cloud storage service.</p>
                </div>
                </div>


                <div className="cloud_backup_solutions">
                    <h2>Why Navicosoft For <br />Cloud Backup Solutions?</h2>
                    <div className="text_right_left_under">
                        <div className="check_text"><i class="far fa-check-circle"></i><p className="text">Navicosoft is a company that provides solution-oriented<br /> services delivered by technological professionals.</p></div>
                        <div className="check_text"><i class="far fa-check-circle"></i><p className="text">Our experts, who have access to a secure cloud storage<br /> facility, are capable of meeting demands for data of any size.</p></div>
                    </div>
                    <div className="text_right_left_under">
                        <div className="check_text"><i class="far fa-check-circle"></i><p className="text">Our mission is to save you money by eliminating the need<br /> for inefficient hard drives and replace them with reliable cloud storage.</p></div>
                        <div className="check_text"><i class="far fa-check-circle"></i><p className="text">We guarantee to protect your business<br /> in the lowest possible cost.</p></div>
                    </div>
                    <div className="text_right_left_under">
                        <div className="check_text"><i class="far fa-check-circle"></i><p className="text">We provide military-grade software security<br /> together with round-the-clock technical assistance.</p></div>
                        <div className="check_text"><i class="far fa-check-circle"></i><p className="text">We protect your data by using<br /> enterprise-level encryption.</p></div>
                    </div>
                </div>

            </div>
        );
    }
}

export default CloudBackUp;
