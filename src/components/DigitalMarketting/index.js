import React, { Component } from 'react';
import digitalmarketingimg from '../../images/digitalmarketingimg.png';
import socialmediamarketing from '../../images/socialmediamarketing.png';
import emailmarketing from '../../images/emailmarketing.png';
import seo from '../../images/seo.png';
import ppc from '../../images/ppc.png';
import AppealingVisuals from '../../images/AppealingVisuals.png';
import contentwritting from '../../images/contentwritting.png';
import sem from '../../images/sem.png';
import tick from '../../images/tick.png';
import Contactusform from '../ContactUsForm';
import { HashLink } from 'react-router-hash-link';

import './style.scss';

class digitalmarketing extends Component {
    render() {
        return (
            <div className="container_dm">
                <h1 className="slider_text"><span className="slidertext_color">  
                Let’s digitally conquer <br />the world </span>with your customised  <br />digital solution in London.<br />
                    <HashLink smooth to="#work-with-us">
                        <button className="btn_digitalmarketting">
                            <a>Work WIth Us</a>
                        </button>
                    </HashLink>
                </h1>
                <div className="digital_marketing_image_section">
                    <img src={digitalmarketingimg} className="first_img" alt="digitalmarketingimg" loading="lazy" />
                </div>
                <div className="section_one">
                    <div className="firsttextsec">  
                        <h2>Most <span className="text_color"> Trusted</span> <br />Digital Marketing Agency</h2>
                        <p>Navicosoft is a renowned online marketing and digital marketing agency based in the United Kingdom. We are here to fly above the competition and offer the most satisfactory service possible, thanks to our high-spirited and enthusiastic team! As an advanced digital marketing agency in London, we guarantee that technology and design are seamlessly integrated. We are continuously developing to provide you with unmatched digital marketing services delivered with honesty, dedication, and consistency. We have come with the goal of achieving the following:</p>
                        <div className="font_text"><i className="far fa-check-circle"></i><p>Define your brand in the best way possible!</p></div>
                        <div className="font_text"><i className="far fa-check-circle"></i><p>Digitalise your brand with innovative marketing solutions.</p></div>

                    </div>
                    <hr />
                    <div className="firsttextsec secondd">
                        <h2>Digital Marketing<br />Innovation in <span className="text_color">London</span></h2>
                        <p>In the UK, developing a well-structured yet creative marketing strategy is no longer a time-consuming endeavour. Navicosoft, a digital marketing agency, has consistently come up with innovative ideas. You don't need to go elsewhere for a digital marketing agency since we are your next-door brand builders and storytellers in London. We ensure to add value to your company as part of our goal to see your business thrive worldwide. We do each task most appropriate for you. Furthermore, we believe in bringing earnings right to your doorstep!</p>

                    </div>
                    <hr />
                    <div className="firsttextsec secondd">
                        <h2>Intelligent Online <br />Marketing to <span className="text_color">Promote Your Brand!</span></h2>
                        <p>Do not only imagine yourself enjoying the peaceful pleasure of outpacing your rivals. Realise your dream because we know how to make your advertising successful and position you at the forefront of attention you alone deserve! Digital marketing in the UK will be head and shoulders above the competition. We have the professional knowledge to elevate your brand to new heights, reaching the shores of the sky and placing your website on the top of search engines. We guarantee that we work on all platforms, including social media, search engines, Google advertising, Search Engine Marketing, and a variety of others.</p>

                    </div>
                    <hr />
                </div>

                <div className="section_two_background">
                <div className="section_two">
                    <div className="secondtextsec">
                        <h2>Our Digital<br />Marketing Services</h2>
                        <p>Our goal is not only to increase the visibility of your digital presence but also to make your online presence seem more attractive and inviting to visitors. Eventually, your prospects will become leads as a result of your engaging web presence. Because we are the best with complicated online marketing services, Navicosoft guarantees that you will have no hassle and no worry.</p>
                    </div>
                    <div className="imgandtextsec">
                        <div className="lefttextsec">
                            <h2>Social Media Marketing</h2>
                            <p>We think that having a long-lasting web existence should never be considered a should-do. Navicosoft is a cutting-edge digital marketing agency in London that focuses on increasing brand recognition as well as generating leads for its clients. We connect you with the world on social media platforms such as Facebook, Instagram, Twitter, and Linkedin, and we provide you with the ideal digital environment!</p>
                        </div>
                        <div className="rightimgsec"><img src={socialmediamarketing} className="righttext_img" alt="socialmediamarketing" /></div>
                    </div>

                    <div className="imgandtextsec">
                        <div className="lefttextsec">
                            <h2>Email Marketing</h2>
                            <p>Our team in the UK is highly competent and expert in email marketing. We make it a point to provide our customers with efficient email marketing campaigns. No matter what your budget is, our cost-effective but result-driven campaigns can communicate effectively with your prospects, offer your services at the most convenient moment, and produce results that exceed your expectations.</p>
                        </div>
                        <div className="rightimgsec"><img src={emailmarketing} className="righttext_img" alt="emailmarketing" /></div>
                    </div>

                    <div className="imgandtextsec">
                        <div className="lefttextsec">
                            <h2>SEO</h2>
                            <p>We are here to help you get the best out of search engine traffic with the help of our SEO professional’s team. We think that revenue optimization should take precedence over conversion optimization. Using a systematic approach to search engine optimization campaigns guarantees that our clients get the best possible return on their investment, quantifiable outcomes, and monthly updates on the work completed.</p>
                        </div>
                        <div className="rightimgsec"><img src={seo} className="righttext_img" alt="seo" /></div>
                    </div>
                    <div className="imgandtextsec">
                        <div className="lefttextsec">
                            <h2>PPC</h2>
                            <p>Do you wish to earn income as soon as possible? Not to worry, we have the first and best Google Adwords as well as Pay-per-click services on the market. If you want to have consumers by tomorrow, then we are the best place in Melbourne to acquire Digital marketing services. Get in touch now to learn about our Digital marketing services. We guarantee to provide the best return on investment (ROI), conversion monitoring, real-time data, and a campaign that fits within your budget.</p>
                        </div>
                        <div className="rightimgsec"><img src={ppc} className="righttext_img" alt="ppc" /></div>
                    </div>
                    <div className="imgandtextsec">
                        <div className="lefttextsec">
                            <h2>Appealing Visuals</h2>
                            <p>A website's purpose is not just to establish a social presence but also to capture visitors' attention via eye-catching graphics and responsiveness. As a leading digital marketing company in London, we distinguish your brand and website by using unique graphics, a user-friendly interface, and many leads to engage the audience and create a massive number of sales for you.</p>
                        </div>
                        <div className="rightimgsec"><img src={AppealingVisuals} className="righttext_img" alt="AppealingVisuals" /></div>
                    </div>
                    <div className="imgandtextsec">
                        <div className="lefttextsec">
                            <h2>Content Writing</h2>
                            <p>High-quality content is the most valuable asset for achieving maximum sales results across all platforms. Navicosoft is a competent digital marketing agency in London that consistently develops innovative marketing campaigns that inspire, educate, and amuse your audience. We promise that your website content will convey a mission statement to your clients.</p>
                        </div>
                        <div className="rightimgsec"><img src={contentwritting} className="righttext_img" alt="contentwritting" /></div>
                    </div>
                    <div className="imgandtextsec">
                        <div className="lefttextsec">
                            <h2>SEM</h2>
                            <p>Our search engine marketing strategies are well-planned and precisely implemented. As a result, we guarantee that we will increase your sales and leads at the lowest possible cost using search engine marketing. Additionally, we create Google Shopping campaigns to drive direct sales of your goods from search results. Further, we effectively promote and market your brand.</p>
                        </div>
                        <div className="rightimgsec"><img src={sem} className="righttext_img" alt="sem" /></div>
                    </div>
                </div>
                </div>

                <div className="sectionthree"></div>
                <h2 className="sectthree_heading">Why NavicoSoft as your<br />Digital Marketing Agency in United Kingdom?</h2>
                <div className="digit_width_custom">
                <div className="digitalmarketting_section">
                    <div className="tickimg1">
                        <img src={tick} alt="tick" />
                        <p className="digitalmkt_txt">We are here with over a decade of expertise to ensure that everything is done properly!</p>
                    </div>
                    <div className="tickimg2">
                        <img src={tick} alt="tick" />
                        <p className="digitalmkt_txt">Navicosoft offers affordable pricing to make your social presence should be both strong and economical.</p>
                    </div>
                    <div className="tickimg3">
                        <img src={tick} alt="tick" />
                        <p className="digitalmkt_txt">Our expert support team is always available to assist you and provide the best possible service.</p>
                    </div>
                    <div className="tickimg">
                        <img src={tick} alt="tick" />
                        <p className="digitalmkt_txt">We promote your brand's quality across all social media platforms and welcome the most creative opportunities that arise as a result.</p>
                    </div>
                </div>
                </div>
                <div id="work-with-us">
                    <Contactusform page_name='Digital Marketting' />
                </div>
            </div>
        );
    }
}

export default digitalmarketing;
