import React, { Component } from 'react';
import webdevelopment from '../../images/webdevelopment.png';
import webdevelopmentlaptop from '../../images/webdevelopmentlaptop.png';
import cutomwebdevimg from '../../images/cutomwebdevimg.png';
import webdevimg from '../../images/webdevimg.png';
import tick from '../../images/tick.png';
import 'react-tabs/style/react-tabs.css';
import Grid from '@material-ui/core/Grid';
import ScrollAnimation from 'react-animate-on-scroll';
import "../../css/animate.min.css";
import { Hidden } from '@material-ui/core';
import './style.scss';
import wellorganized from '../../images/wellorganized.png';
import seofriendly from '../../images/seofriendly.png';
import ecomwebsite from '../../images/ecomwebsite.png';
import cms from '../../images/cms.png';
import appealinglandingpage from '../../images/appealinglandingpage.png';
import techsup from '../../images/techsup.png';
import incrementaldev from '../../images/incrementaldev.png';
import { Link } from 'react-router-dom'

class WebDevelopment extends Component {
    render() {
        return (
            <div className="web_development">
                <ScrollAnimation animateIn='bounceInRight' animateOut='bounceOutLeft'>
                    <h1 className="slider_text">We Bring <span className="slidertext_color">Dynamic, Responsive <br />and Creative Web  <br />Development Solutions! </span><br />

                        <Link to="contact-us"><button className="btn_digitalmarketting" ><a>Transfer Hosting</a></button> </Link>
                    </h1>
                </ScrollAnimation>
                <div className="digital_marketing_image_section">
                    <img src={webdevelopment} className="first_img" alt="web development" loading="lazy" />
                </div>
                <div className="development_solutions">  
                    <h2>Make a Great First Impression with <br />Web Development Solution <br /><span class="text_color">in London</span></h2>
                    <div className="text_dev">
                        <div className="text_section_left">
                            <p>Don't be disappointed by the slow growth of your e-commerce business and sales rate. Navicosoft understands how to conduct effective web development and create state-of-the-art websites for you, emphasising performance, stability and security as the primary considerations. We always use the latest techniques of developing your digital brands and generating high-quality leads.<br /><br />
                                Navicosoft works towards your website as a unique self-identity. We are renowned for developing a high-quality website for all our clients in London. So make sure you're prepared to captivate your audience with your appealing website since we:
                                </p>
                            <div class="font_text"><i class="far fa-check-circle"></i><p>We create a vision-based websites.</p></div>
                            <div class="font_text"><i class="far fa-check-circle"></i><p>We create innovative, unique and meaningful web solutions.</p></div>
                        </div>
                        <ScrollAnimation animateIn='bounceInRight' animateOnce={true}>
                            <div className="right_img_text">
                                <img src={webdevelopmentlaptop} className="first_img" alt="web development" loading="lazy" />
                            </div>
                        </ScrollAnimation>
                    </div>
                </div>

                <div className="custom_web_development">
                    <h2>Outstanding Tech Partners For <br />Custom Web Development Solutions!</h2>
                    <p>Navicosoft is a leading web design and development company in London. Our team of experienced web designers,<br /> developers, and digital strategists provides customers with quantifiable results. We've grown to become one of the most<br /> reputable web development companies in the UK. We have been inspiring the digital world since 2008 to empower your online identity. <br />Since 2008, our customer-centric strategy has been a cornerstone of the business.<br /> Our success is all about our client's satisfaction.</p>
                    <ScrollAnimation animateIn='bounceInDown' animateOnce={true}>
                        <div className="custom_img_webdev">
                            <img src={cutomwebdevimg} className="first_img" alt="web development" loading="lazy" />
                        </div>
                    </ScrollAnimation>
                </div>

                <ScrollAnimation animateIn='bounceInUp' animateOnce={true}>
                    <div className="innovative_webdev">
                        <h2 className="sectthreee_heading">We are Advanced & Modern <br />UK <span className="underline_heading"> Web Development Company</span></h2>
                        <div className="textandimg_sec">
                            <div className="text_webdevv">
                                <p>As a renowned web design company in London, we guarantee that you get contemporary and responsive web design solutions. Additionally, being a web design and development company, we understand how to attract the appropriate audience and create a website that generates leads. So take the first step toward dominating your online company with us, and we'll provide you with effective web solutions, including website development, that will ultimately propel you to the top of the online world!</p>
                            </div>
                            <div className="image_webdevv">
                                <img src={webdevimg} className="first_img" alt="web development" loading="lazy" />
                            </div>
                        </div>
                        <div className="innovative_webdev_btn">
                            <Link to="/contact-us"><button className="btn_digitalmarketting" ><a>Let’s Start Project</a></button></Link>
                        </div>
                    </div>
                </ScrollAnimation>

                <div className="dark_section_background">
                    <div className="section_two">
                        <div className="secondtextsec">
                            <h2>Latest Web Development Solutions<br />  with Preferential Services</h2>
                            <p>Navicosoft integrates all necessary components to create an effective and engaging website for your business. Additionally, we are committed to bringing your imaginative thoughts to life via visual delights. Moreover, we also understand the value of CRM and analytics monitoring. Our objective is to offer a comprehensive, robust, and reliable interface with a variety of CRM systems, including Mailchimp and Hubspot.</p>
                        </div>
                        <div className="imgandtextsec">
                            <div className="lefttextsec">
                                <h2>SEO-friendly Website Development</h2>
                                <p>Any website's SEO ranking should be the top priority. We are committed to developing websites that are both customer-centric and responsive. Our objective is not to create aesthetically attractive websites but to create websites that are simple to browse and, with proper SEO work and easy to rank on Google.</p>
                            </div>
                            <div className="rightimgsec"><img src={seofriendly} className="righttext_img" alt="web development" /></div>
                        </div>

                        <div className="imgandtextsec">
                            <div className="lefttextsec">
                                <h2>Ecommerce Websites</h2>
                                <p>We are motivated to provide reliable solutions! We are a UK web design and development company based in London, and we provide complete eCommerce web development solutions to businesses worldwide. If you're looking to create an online product selling store, we can serve as your reliable web development partner. Our web development team has the right knowledge and expertise to create websites of all complexities.</p>
                            </div>
                            <div className="rightimgsec"><img src={ecomwebsite} className="righttext_img" alt="web development" /></div>
                        </div>

                        <div className="imgandtextsec">
                            <div className="lefttextsec">
                                <h2>Content Management Systems</h2>
                                <p>Navicosoft provides you with full control over the management of your website. With an easy-to-use interface, you can either create new content, edit existing content descriptions, or remove any section from your website. You can convert your blogging website into an income source as there is an increasing trend of free blogging platforms. We can also integrate Joomla, Drupal, Magento, and WordPress content management systems into your website.</p>
                            </div>
                            <div className="rightimgsec"><img src={cms} className="righttext_img" alt="web development" /></div>
                        </div>
                        <div className="imgandtextsec">
                            <div className="lefttextsec">
                                <h2>Appealing Landing Pages</h2>
                                <p>Our web development solutions are the most cost-effective and affordable in London, allowing you to launch your business campaigns at the lowest possible cost. As a UK web development company, Navicosoft assists its customers in the creation of landing pages that are conversion-friendly. When you work with our team, you can be certain that you will have high-performing websites that are simple to integrate as well as useful.</p>
                            </div>
                            <div className="rightimgsec"><img src={appealinglandingpage} className="righttext_img" alt="web development" /></div>
                        </div>
                        <div className="imgandtextsec">
                            <div className="lefttextsec">
                                <h2>Technical Support</h2>
                                <p>We devise a plan for providing the best possible service to you. We have a 24/7 active support team that is only focused on providing our customers with rock-solid assistance. Our technical support department not only listens to your problems but resolve them in the quickest time possible.</p>
                            </div>
                            <div className="rightimgsec"><img src={techsup} className="righttext_img" alt="web development" /></div>
                        </div>
                        <div className="imgandtextsec">
                            <div className="lefttextsec">
                                <h2>Incremental Development</h2>
                                <p>Navicosoft believes that providing services is a continual means of assisting and caring for customers. We guarantee that we work incrementally throughout the web development and design process. This implies that before proceeding forward, we shall ascertain your satisfaction with the current discipline of work. We guarantee that the entire process of web development and design is as easy and simple as possible!</p>
                            </div>
                            <div className="rightimgsec"><img src={incrementaldev} className="righttext_img" alt="web development" /></div>
                        </div>
                    </div>
                </div>

                <div className="wellorganized_section">
                    <h2>Our Organized <span class="text_color">Web Development<br /> Services</span> Will Ensure Your Satisfaction!</h2>
                    <div className="textandimg_sec">
                        <div className="image_webdevv">
                            <img src={wellorganized} className="first_img" alt="web development" loading="lazy" />
                        </div>
                        <div className="text_webdevv">
                            <p>At Navicosoft, we strive to make your digital life simpler and more pleasant. We make it a point to work with cutting-edge technology and marketing solutions. Our company's strategy is intended to guarantee that you get both high-quality leads and a positive return on investment.<br /><br />
                                Additionally, as a full-service technology provider, we personalise our solutions in a manner that differentiates us from the crowd of other businesses. Today, we are one of London's most trusted business development partners. With over a decade of expertise, we are here to strive for our clients' pleasure by providing outcomes that exceed expectations!
                                </p>
                        </div>

                    </div>
                </div>

                <div className="rightchoice_section">
                    <h2>Why Navicosoft is The <br />Right Choice For Web Development<br /> Services in London?</h2>
                    <div className="digitalmarketting_section">
                        <div className="tickimg1">
                            <img src={tick} alt="web development" />
                            <p className="digitalmkt_txt">We create your website from scratch using latest design technology.</p>
                        </div>
                        <div className="tickimg2">
                            <img src={tick} alt="web development" />
                            <p className="digitalmkt_txt">Our friendly team of developers makes you feel easy all the way.</p>
                        </div>
                        <div className="tickimg3">
                            <img src={tick} alt="web development" />
                            <p className="digitalmkt_txt">We aim to create simple and user-friendly websites.</p>
                        </div>
                        <div className="tickimg3">
                            <img src={tick} alt="web development" />
                            <p className="digitalmkt_txt">We place high premium on client’s satisfaction.</p>
                        </div>
                        <div className="tickimg">
                            <img src={tick} alt="web development" />
                            <p className="digitalmkt_txt">We have a solution to all your digital problems.</p>
                        </div>

                    </div>
                </div>
            </div>

        );
    }
}

export default WebDevelopment;