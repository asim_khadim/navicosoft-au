import React, { Component } from 'react';
import google from '../../images/google/google.png';
import googlead from '../../images/google/googlead.png';
import displayad from '../../images/google/displayad.png';
import internationalad from '../../images/google/internationalad.png';
import shopping from '../../images/google/shopping.png';
import keyword from '../../images/google/keyword.png';
import analysis from '../../images/google/analysis.png';
import copycreation from '../../images/google/copycreation.png';
import detailreporting from '../../images/google/detailreporting.png';
import tick from '../../images/tick.png';
import ScrollAnimation from 'react-animate-on-scroll';
import "../../css/animate.min.css";
import 'react-tabs/style/react-tabs.css';
import { HashLink } from 'react-router-hash-link';

import './style.scss';
import Contactusform from '../ContactUsForm';

class GoogleAd extends Component {
    render() {
        return (
            <div className="google_ad">
                <ScrollAnimation animateIn='bounceInRight' animateOut='bounceOutLeft'> 
                    <h1 className="slider_text">Grow Your Business Digitally<br />With The Right Google <br /><span className="slidertext_color">Get the Ads Partner</span><br />
                        <HashLink smooth to="#contact">
                        <button className="btn_digitalmarketting">
                        <a>Talk with our google ads management experts</a>
                        </button>
                        </HashLink>
                    </h1>
                </ScrollAnimation>
                <div className="digital_marketing_image_section">
                    <img src={google} className="first_img" alt="google" loading="lazy" />
                </div>

                <div className="web_development">
                    <div className="development_solutions">
                        <h2>Discover Best of the <br /><span className="text_color">Best Google ads Campaign with a</span><br /> Premier adwords Agency Today!</h2>
                        <div className="text_dev">
                            <div className="text_section_left">
                                <p>Isn't it true that you want to present yourself to the whole world? So relax and let Navicosoft, as the premier Google Ads agency provider, take care of your digital presence via Google Ads. In order to meet your digital requirements, we offer the digital solutions you need. Aside from that, we handle millions of dollars in Google advertising each year to maximize your search conversions. As your Google Ads agency, we help you promote your business by providing you with the finest end-to-end solutions for advertising on Google Ads, marketing campaigns, and Google Shopping Ad campaigns. This capacity to comprehend Google ads and Google's never-ending strategy improvements distinguishes us from our competitors. So let us be your Google ad manager and leave the rest to us since we will do the following for you:</p>
                                <div className="font_text"><i className="far fa-check-circle"></i><p>Premium services because we are a google-certified ad agency. </p></div>
                                <div className="font_text"><i className="far fa-check-circle"></i><p>Weekly meetings and monthly reporting.</p></div>
                                <div className="font_text"><i className="far fa-check-circle"></i><p>Develop result-oriented Google Ads tactics to increase sales.</p></div>
                            </div>

                        </div>
                    </div>
                </div>

                <div className="one_stop_background ">
                <div className="dedicated_background_section">
                    <h2 className="sectthreee_heading mobile_stationary">Be on the top of <br /><span className="slidertext_color">the World with our Google Ads Services!</span></h2>
                    <div className="google_ad_service">
                        <div className="left_text">
                            <p className="para_background_linux">We are a full-service advertising agency that offers not only the best Google Ads solutions but also a full-service advertising package. We are confident with our expertise to assist you, whether you are looking to start a new Google AdWords campaign or modify an existing one. The services of a Google advertising firm with a Google ads expert are just a click away. Bring more amazing services to light as we embark on this incredible adventure!</p>
                            <h3 className="headingthree">PPC ads</h3>
                            <p className="para_background_linux">Want to know an interesting fact? Companies who engage in pay-per-click (PPC) advertisements get an average return on investment (ROI) of $8 for every dollar invested. Our pay-per-click (PPC) advertising strategy may work wonders in increasing your sales and leads. With us as your AdWords agency, you can create a precise plan based on your budget to generate more income, leads, and sales for your company's website.</p>
                        </div>
                        <div className="right_image">
                            <img src={googlead} className="first_img" alt="googlead" loading="lazy" />
                        </div>
                    </div>
                    {/* <button className="btn_digitalmarketting btn_google" ><a href="#">Learn More About PPC services</a></button> */}
                </div>
                </div>



                <div className="section_two">
                    <div className="imgandtextsec">
                        <div className="lefttextsec">
                            <h2>Display Advertisement</h2>
                            <p>Thanks to our Google Ads expert, we are able to get the greatest return on investment. We can develop unique designs that are both attractive and striking for your marketing campaigns. As your AdWords agency, we also plan full management to keep track of your clicks, conversions, and fraud activities to ensure that you get the most out of your campaign. Contact us now to learn more.</p>
                            {/* <button className="btn_digitalmarketting googlead_ad" ><a href="#">Get a custom disply advertising compaign</a></button> */}
                        </div>
                        <div className="rightimgsec"><img src={displayad} className="righttext_img" alt="displayad" /></div>
                    </div>

                    <div className="imgandtextsec">
                        <div className="lefttextsec">
                            <h2>International ad Campaigns</h2>
                            <p>We will assign a Google Ad Manager to handle your foreign ad campaigns on a full-time basis. Besides that, our copywriters and designers produce ad content and eye-catching graphics for each section of your target audience across the world. Because we have something extremely interesting for you, we want to reach your valued customer and create more leads and sales.</p>
                            {/* <button className="btn_digitalmarketting googlead_ad" ><a href="#">advertise Internationally</a></button> */}
                        </div>
                        <div className="rightimgsec"><img src={internationalad} className="righttext_img" alt="internationalad" /></div>
                    </div>

                    <div className="imgandtextsec">
                        <div className="lefttextsec">
                            <h2>Google Shopping</h2>
                            <p>Is your eCommerce shop struggling to attract more customers? Are you looking for ways to improve your website's visibility? We have the most creative method for generating extra income via Google Shopping Ads. Using our Google ad expert, you can make full use of Google shopping ads to increase sales and accomplish more business objectives.</p>
                            {/* <button className="btn_digitalmarketting googlead_ad" ><a href="#">start your google shopping ad compaign</a></button> */}
                        </div>
                        <div className="rightimgsec"><img src={shopping} className="righttext_img" alt="shopping" /></div>
                    </div>

                </div>


                <div className="backorder_flow"> 
                    <h2 className="sectthreee_heading">Efforts behind your Successful<br /><span className="purple_color">Google Ads Campaigns</span></h2>
                    <p className="text_google_center">Navicosoft is not a business that makes empty promises; instead, it strives to exceed expectations. We maintain your AdWords<br /> account, focusing on the most relevant and highly rated keywords to attract a large number of customers to your website. In <br />addition, we provide result-driven pay-per-click campaigns that use the most sophisticated tools and well-planned strategies<br /> to help you achieve your goal of cost per sale. On the other hand, our in-house staff with industry-specific<br /> expertise differentiates us as a business worth working with!</p>
                    <div className="flow_order_width">
                    <div className="flow_order">
                        <div className="search_order">
                            <div className="circle_search">
                                <img src={keyword} alt="keyword"/>
                            </div>
                            <h3>Keyword Research</h3>
                            <p>Finding the most competitive keyword is critical to executing a successful AdWords campaign. We do this research using our most powerful tools to determine which keywords are most relevant to your company.</p>
                        </div>

                        <div className="search_order">
                            <div className="circle_search">
                                <img src={analysis} alt="analysis"/>
                            </div>
                            <h3>Competitor Analysis</h3>
                            <p>Analysing your competitor's marketing may be very beneficial. We study their bids, spending, and landing page optimisation in order to have a better understanding.</p>
                        </div>

                        <div className="search_order">
                            <div className="circle_search">
                                <img src={copycreation} alt="copycreation"/>
                            </div>
                            <h3>Campaign & ad <br />Copy Creation</h3>
                            <p>Once we've completed the preceding stages successfully, we'll create a Google Ads campaign and ad copy. We ensure that numerous ad copies are created to maximise your return on investment (ROI).</p>
                        </div>

                        <div className="search_order">
                            <div className="circle_search">
                                <img src={detailreporting} alt="detailreporting"/>
                            </div>
                            <h3>Detailed Reporting</h3>
                            <p>We are more concerned with your company and investments than with anything else. As a result, we schedule monthly meetings and weekly reports for you to keep you informed of progress.</p>
                        </div>
                    </div>
                    </div>

                </div>
                <div id="contact">
                <Contactusform page_name='Google Ad' />
                </div>
                <div className="sectionthree"></div>
                <h2 className="sectthree_heading">Why Navicosoft for your <br /><span className="purple_color">Google ads Management?</span></h2>
                <div className="digitalmarketting_section">
                    <div className="tickimg1">
                        <img src={tick} alt="tick"/>
                        <p className="digitalmkt_txt">We provide a comprehensive solution to your business's requirements.</p>
                    </div>
                    <div className="tickimg2">
                        <img src={tick} alt="tick"/>
                        <p className="digitalmkt_txt">We are on top of the game with over 12 years of Google ads experience.</p>
                    </div>
                    <div className="tickimg3">
                        <img src={tick} alt="tick"/>
                        <p className="digitalmkt_txt">Our AdWords agency has a proven track record of success.</p>
                    </div>
                    <div className="tickimg">
                        <img src={tick} alt="tick"/>
                        <p className="digitalmkt_txt">We provide your agency with a competitive advantage by producing the most effective advertising campaign strategies.</p>
                    </div>
                </div>
            </div>
        );
    }
}

export default GoogleAd;
