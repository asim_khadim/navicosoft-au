import React, { Component } from 'react';
import businessstationary from '../../images/businessstationary.png';
import designelegence from '../../images/designelegence.png';
import businesscard from '../../images/businesscard.png';
import describe from '../../images/describe.png';
import designparetner from '../../images/designparetner.png';
import getyourdesign from '../../images/getyourdesign.png';
import buisnesscardsdesign from '../../images/buisnesscardsdesign.png';
import logodesignstat from '../../images/logodesignstat.png';
import brochuredesign from '../../images/brochuredesign.png';
import letterheads from '../../images/letterheads.png';
import tick from '../../images/tick.png';
import ScrollAnimation from 'react-animate-on-scroll';
import "../../css/animate.min.css";
import 'react-tabs/style/react-tabs.css';
import { Link } from "react-router-dom";
import ReactPlayer from "react-player";

import './style.scss';

class BusinessStationary extends Component {
    render() {
        return (
            <div className="businessstationary">
                <ScrollAnimation animateIn='bounceInRight' animateOut='bounceOutLeft'> 
                    <h1 className="slider_text">Let’s showcase your  <span className="slidertext_color"><br />business with our unique <br />business stationary <br />Design ideas</span><br />
                      
                        <Link to="contact-us"><button className="btn_digitalmarketting" ><a>Enquire Now</a></button> </Link>
                    </h1>
                </ScrollAnimation>
                <div className="digital_marketing_image_section">
                    {/* <img src={businessstationary} className="first_img" alt="businessstationary" loading="lazy" /> */}
                    <div className='player-wrapper'>
                        <ReactPlayer
                        className='react-player'
                        url='https://www.youtube.com/watch?v=fyj0gQwYaew'
                        width='100%'
                        height='200%'
                        loop= 'true'
                        controls= 'true'
                        playing= 'true'
                        />

                    </div>
                </div>

                <div className="creativity_section"> 
                    <h2 className="sectthreee_heading">Give your business a <br />creative look with business <br /><span className="underline_heading">stationary design</span></h2>
                    <div className="text_sectionn">
                        <p>Take a look at our Company Stationery design services and see how Navicosoft's Stationery designs will help you complete your UK business branding. We have an expert team of business stationery designers for you. Therefore, if you need to redesign your business stationery design, feel free to entrust us with your project. We understand the little details that make a significant difference in your business development and the design kit that captures the viewer's attention.</p>
                        <p>When it comes to your business development and growth, the most critical factor is how your customers perceive you. Our business stationery design in London infuses your interactive business events with additional inspiration and originality. We are much more than a stationery and printing business. We recognise that attractive business stationery with a logo is a critical need for business stationery printing. Therefore, commission stationery designs from us and get something really lovely, as we are here to provide the following:</p>
                    </div>
                    <div className="creativityy_section">
                        <div className="box_section">
                            <div className="smm_box_text">
                                <div className="smm_arrow _text">
                                    <p>Dedicated <br />Professional Designers!</p>
                                </div>
                            </div>
                            <div className="smm_box_text">
                                <div className="smm_arrow _text">
                                    <p>Custom & corporate <br />stationery business designs.</p>
                                </div>
                            </div>
                            <div className="smm_box_text">
                                <div className="smm_arrow _text">
                                    <p>Creative but<br />Affordable printing</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="dark_section_background">
                <div className="section_two">
                    <div className="secondtextsec">
                        <h2>Rejuvenating Business <br />Stationery Design in the UK</h2>
                        <p>Whether you require business card printing, letterhead printing, or any other type of business stationery design, we are the right people to call. Our aim is to offer you a consistent brand management approach, as well as suitable business stationery that includes a logo that is compatible with your brand's identity. Starting with basic brochures and progressing to stunning stationery design services, we are here to help you get notoriety in the London market.<br /><br />
                            Additionally, we create customised stationery designs for businesses using appealing colours and themes. We guarantee that your brand's value is preserved and that its presence is felt. Our business stationery design process incorporates creativity, inspiration, and an adrenaline rush! We provide you with amazing design ideas that will help you showcase your brand and services. Navicosoft is committed to the efficiency and aesthetic appeal of each design it produces!
                            </p>
                    </div>
                    <div className="imgandtextsec">
                        <div className="lefttextsec">
                            <h2>Business Stationery <br />Design with Outclass Look: </h2>
                            <p>We provide your company with fresh business stationery and logo designs. Whether you are starting a new company or already have one, we guarantee that you have everything covered. Additionally, we are here to provide practical visual communication solutions that create lasting memories!</p>
                        </div>
                        <div className="rightimgsec"><img src={designelegence} className="righttext_img" alt="businessstationary" /></div>
                    </div>

                    <div className="imgandtextsec">
                        <div className="lefttextsec">
                            <h2>Business Cards</h2>
                            <p>Navicosoft understands that marketing has a massive impact on your branding; therefore, we will play our part to take your business to new heights. If you are concerned about your business outlook, drop us some descriptions, and we will create designs for you.</p>
                        </div>
                        <div className="rightimgsec"><img src={businesscard} className="righttext_img" alt="businesscard" /></div>
                    </div>
                </div>
                </div>

                <div className="businessstationary_process">
                    <h2>Our Simplified Ways <span class="heading_color">of Creating Your <br />Business </span>Stationary Design</h2>
                    <p>Prepare to wow your clients with this one-of-a-kind business stationery design. Our designers have years<br /> of expertise, which will eventually provide you an advantage over your business competitors. We value <br />your company and are thus delighted to provide you with high-quality service and creative solutions.</p>
                    <div className="process_table_business">
                        <div className="process_table_three">
                            <div className="square_describe">
                                <img src={describe} alt="businesscard" />
                            </div>
                            <h3>Describe</h3>
                            <p>We begin the design process after collecting the necessary data from you. Simply explain your company, services, and requirements, and let us to handle the rest.</p>
                        </div>
                        <div className="process_table_three">
                            <div className="square_describe">
                                <img src={designparetner} alt="businesscard" />
                            </div>
                            <h3>Your Design Partner</h3>
                            <p>Afterward, we link you with one of our expert designers who can help you bring your dream idea to life. We make certain that you feel at ease while communicating with our team.</p>
                        </div>
                        <div className="process_table_three">
                            <div className="square_describe">
                                <img src={getyourdesign} alt="businesscard" />
                            </div>
                            <h3>Get Your Design</h3>
                            <p>You create the vision; we bring it to life. After we've finalised and approved the design, we'll give you the files, which you can then use as graphics.</p>
                        </div>
                    </div>
                    <div className="btn_table_stationary">

                        <Link to="contact-us"><button className="btn_digitalmarketting" ><a>Enquire Now</a></button> </Link>
                    </div>
                </div>

                <div className="business_background">
                <div className="section_two business_background_purple">
                    <div className="secondtextsec">
                        <h2>Business Stationery &<br /> Corporate Stationery Design Services</h2>
                        <p>Navicosoft deals with a variety of business stationery designs that range from logo design to business cards to letterheads and pamphlets. We have the most creative team of designers who have the skills to provide top-quality work. Get started today! Build your brand with the highest-quality designs for your company in the UK, and communicate with your target audience!</p>
                    </div>
                    <div className="imgandtextsec">
                        <div className="lefttextsec">
                            <h2>Business Card Design</h2>
                            <p>We ensure that your clients remember you forever! Navicosoft is well aware that the uninteresting cards will ultimately end up in the waste bin. We design your business cards in such a manner that they stand out and capture the attention of everyone who sees them. Stand out from the crowd now and allow your personality to be reflected in your business cards that are both distinctive and beautiful.</p>
                        </div>
                        <div className="rightimgsec"><img src={buisnesscardsdesign} className="righttext_img" alt="Business Stationary" /></div>
                    </div>

                    <div className="imgandtextsec">
                        <div className="lefttextsec">
                            <h2>Logo Design</h2>
                            <p>We completely understand the importance of a creative and decent logo design for your brand recognition. Therefore, we guarantee that your company is projected effectively through the best logo design. We are in this industry for over a decade, providing business stationery and logo designs to our clients across the globe. We are one of the UK's top logo designers, renowned for our uniqueness and profound ideas!</p>
                        </div>
                        <div className="rightimgsec"><img src={logodesignstat} className="righttext_img" alt="Business Stationary" /></div>
                    </div>

                    <div className="imgandtextsec">
                        <div className="lefttextsec">
                            <h2>Brochure Design</h2>
                            <p>Professionally created brochures will help the world learn about your company. Our competitive and experienced design team will assist you in proudly communicating the narrative of your business to your customers. We specialise in two-sided brochures, tri-fold brochures, quad-fold brochures, single-sided leaflets, and double-sided leaflets with innovative and creative designs.</p>
                        </div>
                        <div className="rightimgsec"><img src={brochuredesign} className="righttext_img" alt="Business Stationary" /></div>
                    </div>
                    

                    <div className="imgandtextsec">
                        <div className="lefttextsec">
                            <h2>Letterheads</h2>
                            <p>We design letterheads for you that is both coherent and professional. Make a lasting impression on your clients in an elegant manner! Our designers have over a decade of expertise and are well aware of design trends and tastes. Therefore, take advantage of our services immediately, as we are here to assist you in streamlining your business communication.</p>
                        </div>
                        <div className="rightimgsec"><img src={letterheads} className="righttext_img" alt="Business Stationary" /></div>
                    </div>
                </div>
                </div>

                <div className="sectionthree"></div>
                <h2 className="sectthreee_heading mobile_stationary">Why Navicosoft is the Right <br />Choice for Business Stationery <br /><span className="slidertext_color">Design in UK</span></h2>
                <p className="center_text_stationary">"First impression is the last impression." Based on this rule, it is crucial to hire the right agency for the first impression of<br /> your business as it will last forever. We are here to make sure that your first business impression should be jaw-dropping for your competitors.<br />  Navicosoft can provide you a competitive advantage over your competitors in London, as we are a well-crafted design agency<br /> that works towards creating the ideal business stationery design for your business.</p>
                <div className="digitalmarketting_section">
                    <div className="tickimg1">
                        <img src={tick} alt="tick" />
                        <p className="digitalmkt_txt">We have a specialised team for technical assistance available to assist you 24 hours a day!</p>
                    </div>
                    <div className="tickimg2">
                        <img src={tick} alt="tick" />

                        <p className="digitalmkt_txt">Our main objective is to provide the highest-quality designs quickly and cost-efficiently. </p>
                    </div>
                    <div className="tickimg3">
                        <img src={tick} alt="tick" />

                        <p className="digitalmkt_txt">We provide a variety of hand crafted design templates for your selection. </p>
                    </div>
                    <div className="tickimg">
                        <img src={tick} alt="tick" />

                        <p className="digitalmkt_txt">We make certain that our artworks and designs generate immediate results.</p>
                    </div>
                </div>
                <div className="center_text_business">
                    <p ><a href="contact-us" className="click_here_text">Click here</a> to talk to Navicosoft’s experts today!</p>
                </div>



            </div>
        );
    }
}

export default BusinessStationary;
