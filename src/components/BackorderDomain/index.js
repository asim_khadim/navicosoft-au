import React, { Component } from 'react';
import backorderdomain from '../../images/backorderdomain.png';
import search from '../../images/search.png';
import arrow1 from '../../images/arrow1.png';
import arrow2 from '../../images/arrow2.png';
import order from '../../images/order.png';
import process from '../../images/process.png';
import reserve from '../../images/reserve.png';
import 'react-tabs/style/react-tabs.css';
import Grid from '@material-ui/core/Grid';
import ScrollAnimation from 'react-animate-on-scroll';
import "../../css/animate.min.css";
import './style.scss';
import { HashLink } from 'react-router-hash-link';

class BackorderDomain extends Component {
    render() {
      return (
            <div className="backorder_domain">
                <ScrollAnimation animateIn='bounceInRight' animateOut='bounceOutLeft'>
                <h1 className="slider_text"> Navicosoft Has Perfect <br /><span className="slidertext_color"> Domain & Back Order <br />For You!</span><br />
                    <HashLink smooth to="#search">
                        <button className="btn_digitalmarketting">
                        <a>Search Your Domain</a>
                        </button>
                        </HashLink>
                </h1>
                </ScrollAnimation> 
                <div className="backgroundimg_regis" style={{ backgroundImage: `url(${backorderdomain})` } } id="search" alt="navicosoft backorder domain">
                    <h3>Backorder your Domain to excel your<br />business and brand globally!</h3>
                 <form className="domain_search_form" action="https://register.navicosoft.com/domainchecker.php" >
                    <input type="hidden" name="token" value="eec641d7cf9cbd6175e6d159332fd5659548444f" />
                   <input type="text" placeholder="WWW.." name="domain" required/>
                   <button type="search"><i class="fas fa-search"></i>Search</button>
                 </form>
                 <p>Navicosoft introduces the backorder domain service, which makes obtaining a domain as simple as it could possibly get! We allow you to win a chance to be first in line if you previously owned a specific domain. Our domain backorder service covers domain monitoring in addition to tracking and registering the domain for one year following registration.</p>
                </div>

                <div className="backorder_flow">
                    <h2 className="sectthreee_heading">Simple Steps To  <br />to <span className="underline_heading">Backorder Your Domain</span></h2>
                    <div className="flow_order">
                        <div className="search_order">
                            <div className="circle_search">
                                <img src={search} alt="navicosoft backorder domain"/>
                            </div>
                            <h3>Search</h3>
                            <p>Browse your desired <br />domain name using our <br />domain name checker</p>
                        </div>

                        <div className="search_order">
                            <div className="circle_search">
                                <img src={order} alt="navicosoft backorder domain"/>
                            </div>
                            <h3>Order</h3>  
                            <p>Now, pick the backorder <br />domain name you've<br /> always desired.</p>
                        </div>

                        <div className="search_order">
                            <div className="circle_search">
                                <img src={process} alt="navicosoft backorder domain"/>
                            </div>
                            <h3>Process</h3>
                            <p>Our highly efficient <br />backorder domain service <br />system will monitor the <br />domain to make it <br />available for you.</p>
                        </div>

                        <div className="search_order">
                            <div className="circle_search">
                                <img src={reserve} alt="navicosoft backorder domain"/>
                            </div>
                            <h3>Reserve</h3>
                            <p>When the domain name <br />becomes available, we will <br />make every effort to <br /> obtain it for you <br />as soon as possible.</p>
                        </div>
                    </div>
                </div>
            
                <div className="smm_dark_background">        
                <div className="smm_dark_section">
                        <div className="smm_first_row">
                            <h2>Enjoy Domain  <br />Backorder with us!</h2>
                            <p>We developed our domain backorder service system utilising a variety of low-latency servers to ensure that it operates efficiently. They are strategically situated with numerous registry connections, allowing them to offer the greatest success rate possible when backordering expired domain names. Every day, tens of thousands of domain names expire. We will keep an eye on the domain you want to buy and will pick it up for you as soon as it becomes available.</p>
                        </div>
                    <hr/>
                        <div className="smm_smam"> 
                            <h2>Let us backorder your Domain<br />so you don't miss out!</h2>
                            <p>Are you aware that your ideal domain could also be someone else's? We will notify you immediately if another party requests the same domain. It is then auctioned. It depends on you to decide whether to bid or pass. Additionally, thousands of names expire or are removed each year due to various reasons. We guarantee that such domains will be available to you by backordering expired domains.<br /><br />
                            Once you get our domain backorder service, you will have complete control over the process. To ensure that you always know if a domain name is accessible or not, we are here to offer complete transparency. You may also log into your client area and keep track of the backordered domains.
                            </p>
                        </div>
                </div>
                </div>   

                <div className="backorder_services_why">
                    <h2 className="sectthreee_heading">Why Navicosoft for <br />Domain <span className="underline_heading">Backorder Service?</span></h2>
                    <div className="box_section">
                        <div className="smm_box_text">
                            <div className="smm_arrow _text">
                                <p>Your privacy and security<br /> are our topmost priority.</p>
                            </div>
                        </div>
                        <div className="smm_box_text">
                            <div className="smm_arrow _text">
                                <p>Navicosoft will provide you the perfect <br />domain name to grow your business.</p>
                            </div>
                        </div>
                        <div className="smm_box_text">
                            <div className="smm_arrow _text"> 
                                <p>We aim to keep your domain & <br />website safe and secure.</p>
                            </div>
                        </div>
                    </div>
                    <div className="box_section below_sec">
                        <div className="smm_box_text">
                            <div className="smm_arrow _text">
                                <p>Our effective tracking system will<br /> track and backorder your desired domains.</p>
                            </div>
                        </div>
                        {/* <div className="smm_box_text">
                            <div className="smm_arrow _text">
                                <p>We let you find the domain name <br />& backorder it with efficient tracking</p>
                            </div>
                        </div> */}
                    </div>
                </div>

            </div>
      );
    }
  }
  
  export default BackorderDomain;
