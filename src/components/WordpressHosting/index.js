import React, { Component } from 'react';
import wordpresshosting from '../../images/wordpresshosting/wordpresshosting.png';
import pic from '../../images/pic.png';
import imgg2 from '../../images/imgg2.png';
import imgg3 from '../../images/imgg3.png';
import wordpressvector from '../../images/wordpresshosting/wordpressvector.png';
import wordpressbackground from '../../images/wordpresshosting/wordpressbackground.png';
import tick from '../../images/wordpresshosting/tick.png';
import data from '../../images/data.png';
import ftpssh from '../../images/ftpssh.png';
import clock from '../../images/clock.png';
import dos from '../../images/dos.png';
import protection from '../../images/protection.png';
import monitoring from '../../images/monitoring.png';
import oneclickapps from '../../images/oneclickapps.png';
import backups from '../../images/backups.png';
import gurantee from '../../images/gurantee.png';
import webhsotingtick from '../../images/webhsotingtick.png';
import domainregi from '../../images/domainregi.png';
import domaintransfer from '../../images/domaintransfer.png';
import webhostcpanel from '../../images/webhostcpanel.png';
import technologysupport from '../../images/technologysupport.png';
import sslcertificate from '../../images/sslcertificate.png';
import emailssetups from '../../images/emailssetups.png';
import unlimiteddomainandwebsites from '../../images/unlimiteddomainandwebsites.png';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
import Grid from '@material-ui/core/Grid';
import ScrollAnimation from 'react-animate-on-scroll';
import "../../css/animate.min.css";
import { Hidden } from '@material-ui/core';
import { Link } from "react-router-dom"
import Flippy, { FrontSide, BackSide } from 'react-flippy';
import './style.scss';
import Products from '../Products';
import { HashLink } from 'react-router-hash-link';

class WordpressHosting extends Component {
    render() {
        return (
            <div className="web_hosting wordpress_hosting">
                <ScrollAnimation animateIn='bounceInRight' animateOut='bounceOutLeft'>   
                    <h1 className="slider_text"><span className="slidertext_color">We Boost </span>Websites<br />with Cheap Managed<br /> WordPress Hosting.<br />
                        <HashLink smooth to="#orderNow">
                            <button className="btn_digitalmarketting">
                                <a>Buy WordPress Hosting</a>
                            </button>
                        </HashLink>
                    </h1>
                </ScrollAnimation>
                <div className="digital_marketing_image_section">
                    <img src={wordpresshosting} className="first_img" alt="wordpresshosting" loading="lazy" />
                </div>

                <div className="enhance_background_width">
                    <div className="enhance_wordpress_hosting_Section">
                        <div className="enhace_wp">
                            <h2>Advanced & Powerful <br /><span className="purple_color">Wordpress Hosting</span></h2>
                            <p>Don't you get frustrated by the occasional downtown bashing of your website? Navicosoft offers UK's best platform for managed WordPress hosting. We facilitate our client's with the most affordable but feature-rich WordPress Hosting available. Moreover, our magnificent technological platform also offers you the best managed WordPress hosting in the UK, as well as refined workflow tools. You may immediately begin building, launching, and managing your site with us as your hosting partners.</p>
                        </div>
                        <div className="section_right_left">
                            <div className="left_text_section">
                                <p>Navicosoft's values are the foundation of our success. Our passion for WordPress Hosting is why we are one of the best managed WordPress hosting companies in the UK. Our enthusiasm for innovating technology can bring you whatever you need! Make the most of your WordPress Hosting Platform with:</p>
                                <div class="font_text"><i class="far fa-check-circle"></i><p>Up-to-date security measures to keep you safe.</p></div>
                                <div class="font_text"><i class="far fa-check-circle"></i><p>Simple and intuitive setup with a single click.</p></div>
                                <div class="font_text"><i class="far fa-check-circle"></i><p>Management of several WordPress websites.</p></div>
                            </div>
                            <div className="right_image_section">
                                <img src={wordpressvector} className="first_img" alt="wordpress hosting" loading="lazy" />
                            </div>
                        </div>
                    </div>

                </div>


                <div className="dedicated_hosting wordpress_hosting_table" id="orderNow">
                    <div className="purchase table_pricing dedi_host" id="buy-now">
                        <div className="container-fluid">
                            <h2 className="web_hosting_h2" >Our <span>WordPress Hosting</span> Packages</h2>
                            <ul id="orderNow">
                                <Products
                                    group_id={135}
                                    imgsrc={pic}
                                    perprice={'mo'}
                                    action={'sharedHostingPlans'}
                                />
                            </ul>

                        </div>
                    </div>
                </div>

                <div className="enhance_background_width"> 
                    <div className="dedicated_background_section" style={{ backgroundImage: `url(${wordpressbackground})` }} alt="windowsvps">
                        <h2 className="sectthreee_heading mobile_stationary">We are<span className="purple_color"> WordPress Hosting Experts</span></h2>
                        <p className="para_background_linux">Unlike other hosting companies in London, we are a group of highly skilled professionals here to serve you. Our experienced team of IT specialists knows all the ins and outs of WordPress hosting, as well as the process of installation. Moreover, our advanced network of technical team members is trusted by the most well-known businesses in the UK, ranging from startups to enterprise-level organisations. </p>

                        <h3>Our Framework For WordPress Hosting</h3>
                        <p className="para_background_linux">We assist you in getting the greatest performance from both the software and hardware side of WordPress hosting. Our experienced team of developers have been offering managed WordPress Hosting in the UK for over a decade. Our best managed WordPress hosting comes with the newest versions for the greatest user experience.</p>
                    </div>
                </div>

                <div className="reliable_webhosting">
                    <h2>Our Services with WordPress Hosting</h2>
                    <p>Navicosoft is the cornerstone of Managed WordPress Hosting; we take care of all the technical<br /> details associated with operating your website on free WordPress Hosting. Launch your website<br /> with our fully featured WordPress hosting since that is ultimately what you will treasure!</p>
                    <Grid container spacing={3} className="flipsection_style">
                        <Grid item xs={12} sm={4}>
                            <Flippy
                                flipOnHover={true}
                                flipDirection="horizontal">
                                <FrontSide>
                                    <h3>Focused Security</h3>
                                    <p>There should be no justification for a website's lack of security. Navicosoft is well aware of the negative consequences of a hacked website! We will ensure that your website is cleaned and will make recommendations on how to protect it.</p>
                                </FrontSide>
                                <BackSide>
                                    <h3>Focused Security</h3>
                                    <p>There should be no justification for a website's lack of security. Navicosoft is well aware of the negative consequences of a hacked website! We will ensure that your website is cleaned and will make recommendations on how to protect it.</p>
                                </BackSide>
                            </Flippy>
                        </Grid>
                        <Grid item xs={12} sm={4}>
                            <Flippy
                                flipOnHover={true}
                                flipDirection="horizontal">
                                <FrontSide>
                                    <h3>Periodic Backups</h3>
                                    <p>In the UK, Navicosoft is your ideal backup partner. We offer 24/7 data backup. No need for clumsy backup plugins; a simple click can restore your data.</p>
                                </FrontSide>
                                <BackSide>
                                    <h3>Periodic Backups</h3>
                                    <p>In the UK, Navicosoft is your ideal backup partner. We offer 24/7 data backup. No need for clumsy backup plugins; a simple click can restore your data.</p>
                                </BackSide>
                            </Flippy>
                        </Grid>
                        <Grid item xs={12} sm={4}>
                            <Flippy
                                flipOnHover={true}
                                flipDirection="horizontal">
                                <FrontSide>
                                    <h3>Speedy Websites</h3>
                                    <p>We ensure that your web pages load at lightning speed. Our proprietary caching technology was created exclusively to accelerate your WordPress sites. Smart refresh ensures that your site is highly cached.</p>
                                </FrontSide>
                                <BackSide>
                                    <h3>Speedy Websites</h3>
                                    <p>We ensure that your web pages load at lightning speed. Our proprietary caching technology was created exclusively to accelerate your WordPress sites. Smart refresh ensures that your site is highly cached.</p>
                                </BackSide>
                            </Flippy>
                        </Grid>
                    </Grid>
                    <Grid container spacing={3} className="flipsection_style">
                        <Grid item xs={12} sm={4}>
                            <Flippy
                                flipOnHover={true}
                                flipDirection="horizontal">
                                <FrontSide>
                                    <h3>SSH Gateway</h3>
                                    <p>We provide full command-line access with our WordPress hosting. With SSH access, you can easily manage all of your WordPress Hosting sites, as well as perform administrative duties.</p>
                                </FrontSide>
                                <BackSide>
                                    <h3>SSH Gateway</h3>
                                    <p>We provide full command-line access with our WordPress hosting. With SSH access, you can easily manage all of your WordPress Hosting sites, as well as perform administrative duties.</p>
                                </BackSide>
                            </Flippy>
                        </Grid>
                        <Grid item xs={12} sm={4}>
                            <Flippy
                                flipOnHover={true}
                                flipDirection="horizontal">
                                <FrontSide>
                                    <h3>Exceptional Support</h3>
                                    <p>Navicosoft also helps you when it comes to installation and maintenance! Indeed, we are here to offer the UK's best WordPress Hosting with 24/7 technical assistance to everyone.</p>
                                </FrontSide>
                                <BackSide>
                                    <h3>Exceptional Support</h3>
                                    <p>Navicosoft also helps you when it comes to installation and maintenance! Indeed, we are here to offer the UK's best WordPress Hosting with 24/7 technical assistance to everyone.</p>
                                </BackSide>
                            </Flippy>
                        </Grid>

                    </Grid>
                </div>

                <div className="">
                    <div className="data_center_wordpresshosting">
                        <h2>United Kingdom <span className="purple_color">Data Centers</span></h2>
                        <p>Navicosoft believes in establishing a trustworthy relationship with our clients in order to anticipate and fulfil<br /> their requirements. Our UK data centres are sufficiently equipped to meet your network latency and data sovereignty<br /> issues. Additionally, our network is interoperable with the majority of CDN providers, such as MaxCDN and Cloudflare. We<br /> employ the best developers for our data centres, guaranteeing that we can address problems such as:</p>
                        <div className="cards_wordpresshosting">
                            <div className="one_card">
                                <img src={tick} className="first_img" alt="wordpress hosting" loading="lazy" />
                                <p>Speed optimisation issue</p>
                            </div>
                            <div className="one_card">
                                <img src={tick} className="first_img" alt="wordpress hosting" loading="lazy" />
                                <p>Database connection errors.</p>
                            </div>
                            <div className="one_card">
                                <img src={tick} className="first_img" alt="wordpress hosting" loading="lazy" />
                                <p>Plugins & themes updation.</p>
                            </div>
                            <div className="one_card">
                                <img src={tick} className="first_img" alt="wordpress hosting" loading="lazy" />
                                <p>Security breaches correction</p>
                            </div>
                            <div className="one_card">
                                <img src={tick} className="first_img" alt="wordpress hosting" loading="lazy" />
                                <p>Woocommerce support.</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="businessemailsolution wordpress_hosting_manage">
                    <div className="secure_business_email">
                        <h2 className="sectthreee_heading">Why Navicosoft For <br /> <span className="yellow_color">Manage WordPress Hosting</span></h2>
                        <div className="secure_full">
                            <div className="secure_rows">
                                <p>We provide WordPress hosting with real-time malware detection.</p>
                            </div>
                            <div className="secure_rows">
                                <p>Navicosoft offers the most affordable but effective WordPress Hosting in the UK.</p>
                            </div>
                            <div className="secure_rows">
                                <p>With wp hosting, you get a full-featured dashboard to manage operations easily.</p>
                            </div>
                            <div className="secure_rows">
                                <p>Navicosoft believes in providing 24/7 technical assistance.</p>
                            </div>
                            <div className="secure_rows">
                                <p>Our principle is to please customers and provide great customer service.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default WordpressHosting;
