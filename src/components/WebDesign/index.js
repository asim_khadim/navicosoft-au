import React, { Component } from 'react';
import Popup from 'reactjs-popup';
import 'reactjs-popup/dist/index.css';
import webdesign from '../../images/webdesign.png';
import backgroundcomputer from '../../images/backgroundcomputer.png';
import carrecovery from '../../images/carrecovery.png';
import ahk from '../../images/ahk.png';
import lab from '../../images/lab.png';
import orhna from '../../images/orhna.png';
import alsa from '../../images/alsa.png';
import neelampower from '../../images/neelampower.png';
import designergy from '../../images/designergy.png';
import sadi from '../../images/sadi.png';
import design from '../../images/design.png';
import devep from '../../images/devep.png';
import strategy from '../../images/strategy.png';
import deployment from '../../images/deployment.png';
import 'react-tabs/style/react-tabs.css';
import Grid from '@material-ui/core/Grid';
import ScrollAnimation from 'react-animate-on-scroll';
import "../../css/animate.min.css";
import { Hidden } from '@material-ui/core';
import './style.scss';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import { Link } from "react-router-dom";
import Contactusform from '../ContactUsForm'; s

class WebDesign extends Component {
    render() {
        return (
            <div className="web_design">
                <ScrollAnimation animateIn='bounceInRight' animateOut='bounceOutLeft'>
                    <h1 className="slider_text">Develop your demanded <br /><span className="slidertext_color">software application with <br /> our stunning website design</span><br />

                        <Link to="contact-us"><button className="btn_digitalmarketting" ><a>Work WIth Us</a></button> </Link>
                    </h1>
                </ScrollAnimation>
                <div className="digital_marketing_image_section">
                    <img src={webdesign} className="first_img" alt="navicosoft" loading="lazy" />
                </div>

                <div className="leading_section">
                    <div className="left_section">
                        <h2><span className="title_section_h2">London’s </span> Leading <br /><span className="title_section_h2">Website Design</span><br /> and Development<br /> Company</h2>
                    </div>
                    <div className="right_section">
                        <p>Navicosoft is a London-based website design and development company that serves clients across the United Kingdom. We are an experienced team of digital strategists, website designers, and web developers who are all specialists in their fields of expertise. We empower our clients by delivering the most outstanding result-driven solutions. As a result of our work with millions of customers over the years, we have established ourselves as one of the UK's most reliable and trustworthy website design and development companies.</p>
                        <button className="btn_digitalmarketting" ><a href="#">Instagramable marketing</a></button>
                    </div>

                </div>

                <div className="design_experiance" style={{ backgroundImage: `url(${backgroundcomputer})` }} alt="navicosoft">
                    <div className="text_experiance"> 
                        <h2>Your best <br />website experience<br />is our priority!</h2>
                        <p>Navicosoft promotes interactive working vibes while working on your projects. Our primary goal is to create something outstanding for the clients that meets their needs; we are also concerned with our own development. Because of our decades of expertise, we continue to place a high value on our customers' ideas and help them achieve their business objectives.</p>
                    </div>
                </div>
                <div className="numbering_section">
                    <div className="first_col line">
                        <h2>16+</h2>
                        <p>years of experience</p>
                    </div>
                    <div className="first_col line">
                        <h2>100%</h2>
                        <p>On-time Delivery</p>
                    </div>
                    <div className="first_col line2">
                        <h2>10,000+</h2>
                        <p>Happy Clients </p>
                    </div>
                </div>

                <div className="feature_projects">
                    <h2 className="heading_feature">Featured Projects</h2>
                    <div className="feature_images">
                        <img src={carrecovery} alt="navicosoft" />
                        <img src={ahk} alt="navicosoft" />
                        <img src={lab} alt="navicosoft" />
                        <img src={alsa} alt="navicosoft" />
                    </div>
                    <div className="feature_images">
                        <img src={designergy} alt="navicosoft" />
                        <img src={orhna} alt="navicosoft" />
                        <img src={neelampower} alt="navicosoft" />
                        <img src={sadi} alt="navicosoft" />
                    </div>
                </div>

                <div className="best_webdesign">
                    <h2>What features make us the <br /><span className="slidertext_color">best Website Design Agency in the UK</span></h2>
                    <p>Navicosoft is the most trusted website design company in the UK, based in London. We are well-known for providing<br /> results – producing the most up-to-date, effective, and responsive website designs that turn small businesses into<br /> large corporations. The company has a dedicated team of web developers committed to providing creative energy <br />to small and large-sized companies in London and across the UK.</p>
                    <p className="bold_text">Our web design team works in the following manner:</p>
                </div>
                <div className="web_hosting ">
                    <Tabs defaultIndex={1}>
                        <TabList>
                            <Tab>
                                <p>Strategy</p>
                            </Tab>
                            <Tab>
                                <p>Design</p>
                            </Tab>
                            <Tab>
                                <p>Development</p>
                            </Tab>
                            <Tab>
                                <p>Deployment</p>
                            </Tab>
                        </TabList>

                        <TabPanel>
                            <div className="panel-content">
                                <div className="imgandtext_tab">
                                    <img src={strategy} className="" alt="navicosoft" loading="lazy" />
                                    <p>We initiate your project with proper planning and make strategies. Our team conducts extensive research on your business category, target market, existing marketing locations, and, most importantly, your ideas.<br /><br />
                                    Our experts collect detailed information about all the nitty-gritty of your business and map out your project's execution.
                                    </p>
                                </div>
                            </div>
                        </TabPanel>
                        <TabPanel>
                            <div className="panel-content">
                                <div className="imgandtext_tab">
                                    <img src={design} className="" alt="navicosoft" loading="lazy" />
                                    <p>After thoroughly researching your business and the most recent web design trends, we use our skills to create a stunning website for you.<br /><br />We emphasize creating a visually appealing website with an eye-catching design to provide an optimal user experience.</p>
                                </div>
                            </div>
                        </TabPanel>
                        <TabPanel>
                            <div className="panel-content">
                                <div className="imgandtext_tab">
                                    <img src={devep} className="" alt="navicosoft" loading="lazy" />
                                    <p>Our first goal is to create a website design that provides a pleasant user experience. Your website should be fast, simple to use, and especially easy to edit by the user. <br /><br />
                                        We utilise popular, custom, built-in, and trustworthy plugins, as well as simple frameworks to keep your website running as quickly as possible.
                                        </p>
                                </div>
                            </div>
                        </TabPanel>
                        <TabPanel>
                            <div className="panel-content">
                                <div className="imgandtext_tab">
                                    <img src={deployment} className="" alt="navicosoft" loading="lazy" />
                                    <p>Following completion of the strategy, planning, design, and development phases, it is time to launch your new website so that the whole world may view it and interact with it.<br /><br />
                                        Allow us to host your website with us on one of our lightning-fast web hosting servers and see unstoppable growth of your online business.
                                        </p>
                                </div>
                            </div>
                        </TabPanel>
                    </Tabs>
                </div>

                <div className="pocket_friendly_background">
                    <div className="pocket_friendly_design_plan">  
                        <h2 className="design_plan_h2">Our Budget-Friendly<br /><span class="slidertext_color">Website Design Packages </span>Are Specifically Created For You!</h2>
                        <p>Navicosoft provides website design packages to suit the needs of any business. There is no need to worry<br /> about whether you need a standard website or a complex online store. We are always available to flourish your business!</p>
                        <Grid container spacing={3} className="design_plan_table">
                            <Grid item xs={12} sm={4}>
                                <div className="design_table">
                                    <h3>Professional</h3>
                                    <p>Are you searching for a functional, attractive, and well-designed website to help you break into the online world? Our primary professional website plan is just the right package for you.<br /><br />
                                        We build a quick and responsive website that uses a versatile website CMS to generate web page layouts; thus, you get more control over your website.
                                        <br /><br /> </p>
                                    <Popup trigger={<button className="btn_digitalmarketting" ><a> Get Quote</a></button>} position="center center" modal >
                                        <div id="contact">
                                            <Contactusform page_name='WebDesign' />
                                        </div>
                                    </Popup>
                                </div>
                            </Grid>
                            <Grid item xs={12} sm={4}>
                                <div className="design_table">
                                    <h3>Premium</h3>
                                    <p>Do you want to take your online presence to the next level with a visually appealing website design? Our Premium plan provides you with an attractive, conversion-focused, fully responsive website created on our most advanced website design layout system.<br /><br />
                                        Give a hard time to your competitors and demonstrate to your customers that you are the best in the business!
                                        <br /><br /> </p>
                                    <Popup trigger={<button className="btn_digitalmarketting" ><a> Get Quote</a></button>} position="center center" modal >
                                        <div id="contact">
                                            <Contactusform page_name='WebDesign' />
                                        </div>
                                    </Popup>
                                </div>
                            </Grid>
                            <Grid item xs={12} sm={4}>
                                <div className="design_table">
                                    <h3>Enterprise</h3>
                                    <p>Are you into something extraordinary for your business? A visually appealing website that has been meticulously crafted down to the pixel.<br />
                                        With our experience in animations and parallax video background effects, our team of web designers will wow your clients!<br />
                                        With comprehensive functionality available under one roof, there is nothing we cannot do.
                                        <br /><br /></p>

                                    <Popup trigger={<button className="btn_digitalmarketting" ><a> Get Quote</a></button>} position="center center" modal >
                                        <div id="contact">
                                            <Contactusform page_name='WebDesign' />
                                        </div>
                                    </Popup>
                                </div>
                            </Grid>

                        </Grid>
                    </div>
                </div>

                <div className="project_business"> 
                    <h2>Do You <span className="purple_color">Have a Project/Business</span> <br />You'd Like to Discuss?</h2>
                    <p class="bold_text">Let’s schedule a consultancy meeting with us.</p>
                    <p>Get sound business advice and a customised marketing strategy from our specialists that you may use as you want.<br />
                        We have years of experience in website design, SEO, and social media marketing. Allow us to take you through all of these options<br /> and how they may ultimately benefit your company.
                        </p>
                    <div className="btns_align"><Link to="contact-us"><button className="btn_digitalmarketting" ><a>Start a Project</a></button> </Link> <a href="tel:+443333355418"><button className="purple_btn_digitalmarketting" ><a><i class="fa fa-phone"></i>+44 3333 355 418</a></button> </a></div>

                </div>
            </div>

        );
    }


}

export default WebDesign;