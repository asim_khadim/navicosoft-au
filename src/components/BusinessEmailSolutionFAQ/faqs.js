export const faqs = [
    {
        id: '1',
        question: 'What is email hosting?',
        answer: 'Email hosting is another service offered by Navicosoft, which is similar to web hosting services. A custom domain-based email account is managed via this tool. Email hosting is provided by most businesses that rent out space on email servers to their clients and take care of the sending and receiving emails on behalf of those customers. Unlike other email hosting services, we provide much more than just storage space and delivery services. To increase efficiency, there is a broad range of partnerships and feature integrations available.'
    },
    {
        id: '2',
        question: 'How to get business email address?',
        answer: 'To get a business email plan, you must first register a domain name with a legitimate domain name registrar. After that, you should look for the most appropriate email hosting package to match your requirements. To get started, just choose an email hosting plan and let us know you want it activated. It is really that simple.'
    },
    {
        id: '3',
        question: 'What are the advantages of Email Hosting?',
        answer: 'If you want your companys communications to be managed by high-quality email servers, you can obtain our best email hosting services. Navicosoft offers high-quality hosting services that provide you more freedom and control than the competition using a sophisticated interface. Moreover, our email hosting services offer 24/7 assistance along with a slew of features that make it easy to connect with your customers.'
    },
    {
        id: '4',
        question: 'How to setup my business email? ',
        answer: 'The procedure of setting up a business email with Navicosoft is simple since we offer the fastest way of configuring your DNS records and mailbox. In order to set up your company emails, you must first configure your domain name and then buy a business email address. After that, you must either complete the procedures necessary to set up your DNS record or request that our technical support staff complete the process for you. Create your email account, and you are finished.'
    }
];