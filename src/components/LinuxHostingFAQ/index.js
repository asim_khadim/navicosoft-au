import React, { Component } from 'react';

import './style.scss';

class LinuxHostingFAQ extends Component {
    render() {
      return (
        <div className="digitalmarkettingfaq_background">
            <div className="digitalmarkettingfaq webdesign_faq">
               <h2>FAQs</h2>
                <div>
                    <h3>What is Linus Shared Hosting?</h3>
                    <p>There are two distinct kinds of shared hosting options. The first is Windows shared hosting, while the second is Linux shared hosting. You may host Magento, Drupal, Joomla, and WordPress, as well as other similar websites and applications, on Linux Hosting. You will be provided with a control panel via which you can manage your website and related email addresses. Cpanel is easy-to-use control panel that offers the most basic functions and requirements</p>
                </div>
                <div>
                    <h3>What is the primary difference between Linux hosting and Windows hosting?</h3>
                    <p>Linux hosting is a kind of hosting in which you are provided with a cpanel control panel via which you can handle all aspects of your website. On-click installation of Joomla, Magento, Drupal, WordPress, and other programmes is available with Linux hosting. Whereas Windows hosting allows you to host PHP, ASP.Net apps, crystal reports, and MSSQL databases. With Windows shared hosting, you get a Plesk control panel.</p>
                </div>
                <div>
                    <h3>Is it possible to get a free trial of Linux Hosting from Navicosoft?</h3>
                    <p>Navicosoft is happy to provide you with a Linux hosting trial. Before everything else, we place a high priority on client satisfaction. So, if you're unsure about your requirements or the quality or performance of our hosting, you may sign up for a free 3 day trial. You may proceed to make the payment after you've decided on our hosting package.</p>
                </div>
                <div>
                    <h3>Which payment options is Navicosoft accepting?</h3>
                    <p>We are committed to make our clients as relaxed as possible. We always work towards offering reliable and easy solution to our customers. As a result, one of our efforts to put you at ease is to provide you with a variety of payment options from which to select. The following ways of payment are available:
                     <ul className="faq_payment_method_linux">   
                        <li>Paypal</li>
                        <li>Bank transfer</li>
                        <li>BTC</li>
                        </ul>
                        </p>
                </div>
                
               
            </div>
            </div>
      );
    }
  }
  
  export default LinuxHostingFAQ;
