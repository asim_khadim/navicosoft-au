import React, { Component } from 'react';
import Products from "../Products";
import pic from '../../images/pic.png';
import imgg2 from '../../images/imgg2.png';
import imgg3 from '../../images/imgg3.png';
import './style.scss';

class Table extends Component {
  render() {
    return (

      <div className="purchase btn_btn" id="buy-now">
        <div className="container">
          <h2 className="h2_second">HOSTING PACKAGES</h2>
          <p className="para_sec">Let’s feel the iconic tech experience with our feature-packed yet affordable hosting packages. <br />We bring endless possibilities with phenomenal web hosting along your digital way!</p>
          <ul>
            <Products
              product_id={721}
              cycle={'annually'}
              imgsrc={pic}
              product_title={'Web Hosting'}
              perprice={'yr'}
              classes={''}
              btn_link={'/linux-hosting'}
              action={'homePage'}
              description={'Make the smart choice, get the merging web hosting with free domain, unlimited web space, and the speed you deserve!'}
            />


            <Products
              product_id={103}
              cycle={'monthly'}
              imgsrc={imgg2}
              product_title={'Dedicated Hosting'}
              perprice={'mo'}
              classes={'mid_table'}
              btn_link={'/dedicated-hosting'}
              action={'homePage'}
              description={'Go on, and grab the dedicated hosting, the powerhouse with unmetered bandwidth, powerful hardware, and incredible uptime!'}
            />

            <Products
              product_id={662}
              cycle={'monthly'}
              imgsrc={imgg3}
              product_title={'VPS Hosting'}
              perprice={'mo'}
              classes={''}
              btn_link={'/vps-hosting'}
              action={'homePage'}
              description={'Enjoy the increased power, flexibility, and uptime in an affordable manner with complete root access & full virtualization.'}
            />
          </ul>

        </div>
      </div>
    );
  }
}

export default Table;
