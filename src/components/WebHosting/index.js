import React, { Component } from 'react';
import webhosting from '../../images/webhosting.png';
import pic from '../../images/pic.png';
import imgg2 from '../../images/imgg2.png';
import imgg3 from '../../images/imgg3.png';
import cpanel from '../../images/cpanel.png';
import mail from '../../images/mail.png';
import techsupport from '../../images/techsupport.png';
import data from '../../images/data.png';
import ftpssh from '../../images/ftpssh.png';
import clock from '../../images/clock.png';
import dos from '../../images/dos.png';
import protection from '../../images/protection.png';
import monitoring from '../../images/monitoring.png';
import oneclickapps from '../../images/oneclickapps.png';
import backups from '../../images/backups.png';
import gurantee from '../../images/gurantee.png';
import webhsotingtick from '../../images/webhsotingtick.png';
import domainregi from '../../images/domainregi.png';
import domaintransfer from '../../images/domaintransfer.png';
import webhostcpanel from '../../images/webhostcpanel.png';
import technologysupport from '../../images/technologysupport.png';
import sslcertificate from '../../images/sslcertificate.png';
import emailssetups from '../../images/emailssetups.png';
import unlimiteddomainandwebsites from '../../images/unlimiteddomainandwebsites.png';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
import Grid from '@material-ui/core/Grid';
import ScrollAnimation from 'react-animate-on-scroll';
import "../../css/animate.min.css";
import { Hidden } from '@material-ui/core';
import { Link } from "react-router-dom"
import Flippy, { FrontSide, BackSide } from 'react-flippy';
import './style.scss';
import Products from '../Products';
import { HashLink } from 'react-router-hash-link';

class WebHosting extends Component {
    render() {
        return (
            <div className="web_hosting">
                <ScrollAnimation animateIn='bounceInRight' animateOut='bounceOutLeft'>
                    <h1 className="slider_text"><span className="slidertext_color">We, </span>being the only fastest web  <br /> hosting platform in the United<br /><span className="slidertext_color"> Kingdom, ensure that your businesses run at lightning speed.</span><br />
                       
                        <HashLink smooth to="#orderNow">
                            <button className="btn_digitalmarketting">
                                <a>Order Now</a>
                            </button>
                        </HashLink>
                    </h1>
                </ScrollAnimation>
                <div className="digital_marketing_image_section">
                    <img src={webhosting} className="first_img" alt="webhosting" loading="lazy" />
                </div>

                <div className="web_hosting_palns" id="orderNow">
                    <div className="purchase table_pricing dedi_host" id="buy-now">
                        <div className="container-fluid">
                            <h2 className="web_hosting_h2" ><span>Choose The Best</span> Web Hosting Plan</h2>
                            <ul id="orderNow">
                            <Products
                                product_id={721}
                                cycle={'annually'}
                                imgsrc={pic}
                                product_title={'Web Hosting'}
                                perprice={'yr'}
                                classes={''}
                                btn_link={'/linux-hosting'}
                                action={'homePage'}
                                description={'Make the wise option and sign up for the best web hosting package that includes a free domain, unlimited web space, and the speed you deserve!'}
                                />


                                <Products
                                            product_id={103}
                                            cycle={'monthly'}
                                            imgsrc={imgg2}
                                            product_title={'Dedicated Hosting'}
                                            perprice={'mo'}
                                            classes={'mid_table'}
                                            btn_link={'/dedicated-hosting'}
                                            action={'homePage'}
                                            description={'Proceed to purchase dedicated hosting, a powerhouse with unlimited bandwidth, strong hardware, and an unmatched uptime.'}
                                            />

                                <Products
                                            product_id={662}
                                            cycle={'monthly'}
                                            imgsrc={imgg3}
                                            product_title={'VPS Hosting'}
                                            perprice={'mo'}
                                            classes={''}
                                            btn_link={'/vps-hosting'}
                                            action={'homePage'}
                                            description={'With complete root access and full virtualization, you may enjoy enhanced power, flexibility, and uptime at a reasonable price.'}
                                            />
                            </ul>

                        </div>
                    </div>
                </div>
                <div className="technical_specification">  
                    <h2 className="web_hosting_h2"><span>Each Web Hosting</span> Plan <br /> Includes Technical Specifications</h2>
                    <Grid container spacing={3} className="grid_technical">
                        <Grid item xs={12} sm={4}>
                            <div className="technical_content">
                                <img src={cpanel} className="" alt="cpanel" loading="lazy" />
                                <p>Cpanel</p>
                            </div>
                        </Grid>
                        <Grid item xs={12} sm={4}>
                            <div className="technical_content">
                                <img src={mail} className="" alt="mail" loading="lazy" />
                                <p>Email Protection</p>
                            </div>
                        </Grid>
                        <Grid item xs={12} sm={4}>
                            <div className="technical_content">
                                <img src={techsupport} className="" alt="techsupport" loading="lazy" />
                                <p>24/7 Tech Support</p>
                            </div>
                        </Grid>
                    </Grid>
                    <Grid className="grid_technical" container spacing={3}>
                        <Grid item xs={12} sm={4} >
                            <div className="technical_content">
                                <img src={data} className="" alt="data" loading="lazy" />
                                <p>Unlimited Data</p>
                            </div>
                        </Grid>
                        <Grid item xs={12} sm={4}>
                            <div className="technical_content">
                                <img src={ftpssh} className="" alt="ftpssh" loading="lazy" />
                                <p>FTP & SSH Access</p>
                            </div>
                        </Grid>
                        <Grid item xs={12} sm={4}>
                            <div className="technical_content">
                                <img src={clock} className="" alt="clock" loading="lazy" />
                                <p>99.9% Uptime Guarantee</p>
                            </div>
                        </Grid>
                    </Grid>
                    <Grid className="grid_technical" container spacing={3}>
                        <Grid item xs={12} sm={4} >
                            <div className="technical_content">
                                <img src={dos} className="" alt="dos" loading="lazy" />
                                <p>DDOs Protection</p>
                            </div>
                        </Grid>
                        <Grid item xs={12} sm={4}>
                            <div className="technical_content">
                                <img src={protection} className="" alt="protection" loading="lazy" />
                                <p>Multi Code Support</p>
                            </div>
                        </Grid>
                        <Grid item xs={12} sm={4}>
                            <div className="technical_content">
                                <img src={monitoring} className="" alt="monitoring" loading="lazy" />
                                <p>24/7 Site Monitoring</p>
                            </div>
                        </Grid>
                    </Grid>
                    <Grid className="grid_technical" container spacing={3}>
                        <Grid item xs={12} sm={4} >
                            <div className="technical_content">
                                <img src={oneclickapps} className="" alt="oneclickapps" loading="lazy" />
                                <p>One Click Apps</p>
                            </div>
                        </Grid>
                        <Grid item xs={12} sm={4}>
                            <div className="technical_content">
                                <img src={backups} className="" alt="backups" loading="lazy" />
                                <p>File Backups</p>
                            </div>
                        </Grid>
                        <Grid item xs={12} sm={4}>
                            <div className="technical_content lastimg_gurantee">
                                <img src={gurantee} className="" alt="gurantee" loading="lazy" />
                                <p>60 Day Money Back</p>
                            </div>
                        </Grid>
                    </Grid>
                </div>

                <div className="main result_focus_section">
                    <div className="first"> 
                        <h2>Obtain Exceptional<br />Business Results </h2>
                        <p>Navicosoft's web hosting servers are designed at the corporate level for scalability and performance in<br /> order to provide the quickest website speed possible while maintaining complete security and<br /> dependability. Make your website online now with our lightning-fast web hosting services that<br /> guarantee up to 100% uptime and zero spamming. Because we make certain of it.</p>
                    </div>
                    <div className="second"></div>
                    <div className="digital_section">
                        <div className="img1">
                            <img src={webhsotingtick} alt="webhsotingtick" />  
                            <p className="digital_txt">With our free website <br />builder, you can create an<br /> ideal professional website. </p>
                        </div>
                        <div className="img2">
                            <img src={webhsotingtick} alt="webhsotingtick" />
                            <p className="digital_txt">Install WordPress a with <br />single click</p>
                        </div>
                        <div className="img3">
                            <img src={webhsotingtick} alt="webhsotingtick" /> 
                            <p className="digital_txt">Our amazing and high- <br />performance servers  <br />provide the fastest website<br /> speed possible.</p>
                        </div>
                        <div className="img4">
                            <img src={webhsotingtick} alt="webhsotingtick" />
                            <p className="digital_txt">Host your website and get <br />unlimited free  <br />professional emails.</p>
                        </div>
                    </div>
                </div>

                <div className="webhosting_features"> 
                    <h2 className="sectthreee_heading">We Provide <span className="underline_heading">The Following Features: </span></h2>
                    <p className="webhsoting_featurepara">Whether it's assisting you with selecting a domain name, setting cloud email addresses for your company, or<br /> guiding you through our website builder, our technical staff is here to assist you at any time.</p>
                    <Tabs defaultIndex={3}>
                        <TabList>
                            <Tab>
                                <p>Free Domain Registration</p>
                            </Tab>
                            <Tab>
                                <p>Free Domain Transfer</p>
                            </Tab>
                            <Tab>
                                <p>Web Hosting Control Panel</p>
                            </Tab>
                            <Tab>
                                <p>24/7/365 Technical Support</p>
                            </Tab>
                            <Tab>
                                <p>Free SSL Certificate</p>
                            </Tab>
                            <Tab>
                                <p>Simple Emails Setups</p>
                            </Tab>
                            <Tab>
                                <p>Unlimited Domains & Websites</p>
                            </Tab>
                        </TabList>

                        <TabPanel>
                            <div className="panel-content">
                                <img src={domainregi} className="" alt="domainregi" loading="lazy" /> 
                                <p>With each of our web hosting plans, you'll get a free domain name.<br /><br />Check the availability of your chosen domain here and reserve it immediately!</p>
                            </div>
                        </TabPanel>
                        <TabPanel>
                            <div className="panel-content">
                                <img src={domaintransfer} className="" alt="domaintransfer" loading="lazy" /> 
                                <p>Have you previously registered a domain name elsewhere and want to transfer it?<br /><br />Transfer it to Navicosoft for free and we'll host your website at lightning speed on our strong servers with a guaranteed 0% downtime.</p>
                            </div>
                        </TabPanel>
                        <TabPanel>
                            <div className="panel-content">
                                <img src={webhostcpanel} className="" alt="webhostcpanel" loading="lazy" />
                                <p>Take complete control of your web hosting control panel and simply manage your website from any web portal.</p>
                            </div>
                        </TabPanel>
                        <TabPanel>
                            <div className="panel-content">
                                <img src={technologysupport} className="" alt="technologysupport" loading="lazy" />
                                <p>We provide all types of web hosting services and provide 24/7 chat, ticket, and phone assistance. <br /><br />Whenever you need technical assistance, our staff is here to assist you.</p>
                            </div>
                        </TabPanel>
                        <TabPanel>
                            <div className="panel-content">
                                <img src={sslcertificate} className="" alt="sslcertificate" loading="lazy" />
                                <p>A safe website, we think, has a positive effect on visitors. <br /><br />As a result, we provide free SSL certificates with all of our web hosting services. <br /><br />Purchase the package and protect your website.</p>
                            </div>
                        </TabPanel>
                        <TabPanel>
                            <div className="panel-content">
                                <img src={emailssetups} className="" alt="emailssetups" loading="lazy" />
                                <p>With our web hosting plans, you can easily set up your professional emails. <br /><br />After selecting an appropriate plan, go into your web hosting control panel, navigate to the email account page, and create a new email.</p>
                            </div>
                        </TabPanel>
                        <TabPanel>
                            <div className="panel-content">
                                <img src={unlimiteddomainandwebsites} className="" alt="unlimiteddomainandwebsites" loading="lazy" />
                                <p>You may now host an unlimited number of websites and domains with a single hosting subscription.  <br /><br />Purchase an appropriate website hosting plan to begin enjoying it immediately!</p>
                            </div>
                        </TabPanel>
                    </Tabs>
                    <div className="hosting_packages_btn">
                        <button className="btn_digitalmarketting" ><HashLink smooth to="#orderNow">Check Hosting packages</HashLink></button>
                    </div>
                </div>

                <div className="reliable_webhosting">
                    <h2>Why Choose Navicosoft as the Most <br /> Reliable Web Hosting Company in the United Kingdom?</h2>
                    <p>You'll get all you need to ensure your online presence's success</p>
                    <Grid container spacing={3} className="flipsection_style">
                        <Grid item xs={12} sm={3}>
                            <Flippy
                                flipOnHover={true}
                                flipDirection="horizontal">
                                <FrontSide>
                                    <h3>One-click<br />installation</h3>
                                    <p>With one-click installation, you may install up to 100+ applications for free, including WordPress, Joomla, and a variety of other business tools.</p>
                                </FrontSide>
                                <BackSide>
                                    <h3>One-click<br />installation</h3>
                                    <p>With one-click installation, you may install up to 100+ applications for free, including WordPress, Joomla, and a variety of other business tools.</p>
                                </BackSide>
                            </Flippy>
                        </Grid>
                        <Grid item xs={12} sm={3}>
                            <Flippy
                                flipOnHover={true}
                                flipDirection="horizontal">
                                <FrontSide>
                                    <h3>Easy Control<br />Panel</h3>
                                    <p>Utilise our simple-to-use control panels like as Plesk and Cpanel to manage all of your services directly from your Navicosoft client area.</p>
                                </FrontSide>
                                <BackSide>
                                    <h3>Easy Control<br />Panel</h3>
                                    <p>Utilise our simple-to-use control panels like as Plesk and Cpanel to manage all of your services directly from your Navicosoft client area.</p>
                                </BackSide>
                            </Flippy>
                        </Grid>
                        <Grid item xs={12} sm={3}>
                            <Flippy
                                flipOnHover={true}
                                flipDirection="horizontal">
                                <FrontSide>
                                    <h3>Fast Servers </h3>
                                    <p>With our premium high blazing speed servers, your website's loading time will be much slower than that of your competitors, which ensures a 500% performance increase.</p>
                                </FrontSide>
                                <BackSide>
                                    <h3>Fast Servers </h3>
                                    <p>With our premium high blazing speed servers, your website's loading time will be much slower than that of your competitors, which ensures a 500% performance increase.</p>
                                </BackSide>
                            </Flippy>
                        </Grid>
                        <Grid item xs={12} sm={3}>
                            <Flippy
                                flipOnHover={true}
                                flipDirection="horizontal">
                                <FrontSide>
                                    <h3>Security</h3>
                                    <p>With our premium Greenlock SSL certificate, you can protect your website's critical data from hackers. Install it on your website to increase the strength of your domain privacy protection.</p>
                                </FrontSide>
                                <BackSide>
                                    <h3>Security</h3>
                                    <p>With our premium Greenlock SSL certificate, you can protect your website's critical data from hackers. Install it on your website to increase the strength of your domain privacy protection.</p>
                                </BackSide>
                            </Flippy>
                        </Grid>
                    </Grid>
                </div>
            </div>
        );
    }
}

export default WebHosting;
