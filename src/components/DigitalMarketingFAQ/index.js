import React, { Component } from 'react';

import './style.scss';

class DigitalMarketingFAQ extends Component {
    render() {
      return (
            <div className="digitalmarkettingfaq">
               <h2>FAQ's</h2>
                <div>
                    <h3>What is our role as digital marketing agency in the United Kingdom?</h3>
                    <p>We assist companies in achieving their objectives and identifying prospective customers. Our services include SEM, social media marketing, pay-per-click advertising, search engine optimization, and other digital marketing services are available via us. We begin by doing in-depth research on your brand and then targeting the appropriate audience. Later on, we provide our customers with performance monitoring, plan development, as well as evaluating and auditing their marketing efforts.</p>
                </div>
                <div>
                    <h3>Do we provide the cheapest digital marketing services?</h3>
                    <p>It might not be possible to provide you with the cheapest digital marketing services available in the market, but we guarantee that we will place a strong emphasis on delivering results that are of high quality. Many other digital marketing companies in Melbourne offer cheap rates, but in the end, you will get poor results and a low conversion rate as a consequence of their efforts. As a result, we set an essential objective for you: to be successful while generating the greatest possible return on your investment.</p>
                </div>
                <div>
                    <h3>What kind of businesses do we work with?</h3>
                    <p>We deal with a diverse variety of companies of varying sizes and types. Our services are available to both big and small businesses, and we do not discriminate in our work. Aside from that, we will put up a specialised team of digital marketing experts to fulfil your online marketing needs. We are here to help you contact your prospective customers via passionate and effective marketing initiatives, whether you are a tiny business seeking growth opportunities or a well-established organisation.</p>
                </div>
                <div>
                    <h3>How can we differentiate our SEO services from the competition?</h3>
                    <p>We begin by doing an analysis of your search engine optimisation (SEO) requirements. Our expert team is committed to doing a thorough study of your company and marketing requirements to position you above your competitors. We optimise your campaign by selecting the most appropriate keywords. Additionally, we structure your website and choose which channels to optimise off-page. While rating your website may take some time, but it will be genuinely organic. What distinguishes us from the competition are the long-term outcomes we assist you in achieving.</p>
                </div>
                <div>
                    <h3>What is unique about our social media marketing approach?</h3>
                    <p>We consider your social media marketing campaign to be our most important duty. As a digital marketing agency in London with decades of experience and expertise, we have an in-depth knowledge of trends and SMM strategies. We do complete research to get a thorough understanding of your brand. Studying your competitors, business category, and target audience are important considerations. Following the identification of these factors, we launch a social media campaign to generate leads and accurate interaction.</p>
                </div>
                <div>
                    <h3>How effective are our PPC campaigns?</h3>
                    <p>When you entrust us with your PPC campaign, we, as a cutting-edge digital marketing agency in London, ensure that you get dedicated assistance and maintenance. We don't just put up a PPC campaign and letting it run on its own. Rather, we follow and monitor the campaigns in real-time to verify that it is running well. Each campaign is carefully analysed to get the most value from a keyword and ad placement while also guaranteeing that the campaign reaches the intended demographic.</p>
                </div>
            </div>
      );
    }
  }
  
  export default DigitalMarketingFAQ;
