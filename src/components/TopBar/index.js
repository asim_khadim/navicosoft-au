import React, { Component } from 'react';
import cart from '../../images/cart.png';
import { Link, NavLink, withRouter } from "react-router-dom";
import 'react-tabs/style/react-tabs.css';
import Grid from '@material-ui/core/Grid';
import ScrollAnimation from 'react-animate-on-scroll';
import "../../css/animate.min.css";
import { Hidden } from '@material-ui/core';
import './style.scss';


class TopBar extends Component {
    render() {
      return (
            <div className="top_bar">
               <div className="phone"><i className="fas fa-phone-alt"></i> +44 3333 355 418</div> <span className="line_topbar">|</span>
               <div className="registration_sign">
               <Link to={{ pathname: "https://register.navicosoft.com/register.php?_ga=2.122641026.883677110.1622438902-1006891373.1600335468" }} target="_blank" ><a >Sign Up </a></Link>
               <Link to={{ pathname: "https://register.navicosoft.com/clientarea.php?_ga=2.122641026.883677110.1622438902-1006891373.1600335468" }} target="_blank" ><a >/ Log In</a></Link>
               </div>
               <div > <img src={cart} className="cart" alt="navicosoft cart" loading="lazy" /></div>
            </div>

      );
    }
    
    
  }
  
  export default TopBar;