import React, { Component } from 'react';
import logodesign from '../../images/logodesign/logodesign.png';
import plant from '../../images/logodesign/plant.png';
import logodesign2d from '../../images/logodesign/logodesign2d.png';
import logodesign3d from '../../images/logodesign/logodesign3d.png';
import brandlogodesign from '../../images/logodesign/brandlogodesign.png';
import logodesignanimated from '../../images/logodesign/logodesignanimated.png';
import search from '../../images/logodesign/search.png';
import idealize from '../../images/logodesign/idealize.png';
import create from '../../images/logodesign/create.png';
import launch from '../../images/logodesign/launch.png';
import ScrollAnimation from 'react-animate-on-scroll';
import "../../css/animate.min.css";
import 'react-tabs/style/react-tabs.css';
import { Link } from "react-router-dom";

import './style.scss';

class LogoDesign extends Component {
    render() {
        return (
            <div className="logo_design">
                <ScrollAnimation animateIn='bounceInRight' animateOut='bounceOutLeft'>
                    <h1 className="slider_text">Enhance your brand's <br /> recognition with a <span className="slidertext_color">dazzling image <br />and a flurry of vibrant colours  <br />in your business logo design.</span><br />

                        <Link to="contact-us"><button className="btn_digitalmarketting" ><a>Get Started</a></button> </Link>
                    </h1>
                </ScrollAnimation>
                <div className="digital_marketing_image_section">
                    <img src={logodesign} className="first_img" alt="logodesign" loading="lazy" />
                </div>

                <div className="dedicated_background_section" style={{ backgroundImage: `url(${plant})` }} alt="logodesign"> 
                    <h2 className="sectthreee_heading mobile_stationary">Business Logo Design Lets<br /><span className="slidertext_color">you Communicate <br />Elegantly</span></h2>
                    <p className="para_background_linux">Are you searching for a business logo design that is both professional and cheap in Australia? With Navicosoft, you can have it all! Thousands of companies trust us as one of Australia's top logo designers. We know how to design an elegant brand logo. Create a compelling business identity by understanding your company's ideas and branding visuals. We are here to:</p>
                    <div className="font_text"><i className="far fa-check-circle"></i><p><strong>Bring your brand's & company's logo to life with creative embellishment ideas.</strong></p></div>
                    <div className="font_text"><i className="far fa-check-circle"></i><p><strong>Design with a burning desire to engage your prospects via visual inventiveness and originality.</strong></p></div>
                </div>

                <div className="dark_section_background">
                <div className="smm_dark_section">
                    <div className="smm_first_row">
                        <h2>Innovative Business Logo <br />Design Featuring Amazing Ideas!</h2>
                        <p>At Navicosoft, our goal is to offer you a mesmerising business logo, product logo, and company logo for your business identity. Additionally, we provide a range of services to help your company flourish. We understand what it takes to propel your brand forward. Therefore, start marketing your business to the globe now with a UK-made logo company. We are here to provide you with an outstanding and distinctive CBA logo design for your company. Additionally, we provide a new method of publicising the correct route with a more sophisticated company logo design.</p>
                    </div>
                    <hr />
                    <div className="smm_smam"> 
                        <h2>Simplified but creative business logo <br />design – The Perfect Experience</h2>
                        <p>Navicosoft understands your concerns about graphic designs. We facilitate you with the best designers to help you communicate yourself to the rest of the world. With a unique CBA logo, you may put yourself on the road to success in advertising. Rather than settling for a generic brand logo creator online with a clunky template-based Business logo design, we encourage you to create your vision using original images and illustrations. With Navicosoft, you can be confident in your ability to collaborate with expert designers to create affordable UK-made logos.</p>
                    </div>
                </div>
                </div>

                <div className="linux_server">
                    <div className="section_two">
                        <div className="secondtextsec"> 
                            <h2>Our Business Logo Design Services<br />Can Bring Your Ideas to Life</h2>
                            <p>We are well aware that your business logo serves as your worldwide brand recognition. That's why we offer the appropriate approach to project your company's requirements. We can create a primary company logo or a whole range of branding for you. We also provide you a fantastic brand logo that sets you apart from the competitors.</p>
                        </div>
                        <div className="imgandtextsec">
                            <div className="lefttextsec">
                                <h2>2D Logo Design</h2>
                                <p>Navicsocost has tremendous success in logo designing bringing new concepts to the table for company logo design. We all know that graphics are more easily recalled than written text. So, in order to build timeless logos that connect with the precise thoughts behind your business, we allow you to decorate your brand logo with embellishing concepts. It doesn't matter whether you want a basic 2D logo design or any complex variants; we are always the best spot, to begin with!</p>
                            </div>
                            <div className="rightimgsec"><img src={logodesign2d} className="righttext_img" alt="logodesign" /></div>
                        </div>

                        <div className="imgandtextsec">
                            <div className="lefttextsec">
                                <h2>3D Logo Design</h2>
                                <p>We understand that a company's success is directly related to its brand's perception. As a result, we prioritise logo design as the very first stage in the branding process. Each logo design we produce is unique and customised to your specifications. We develop 3D logo designs that add a touch of realism to your brand's look.</p>
                            </div>
                            <div className="rightimgsec"><img src={logodesign3d} className="righttext_img" alt="logodesign" /></div>
                        </div>

                        <div className="imgandtextsec">
                            <div className="lefttextsec">
                                <h2>Animated Logo Design</h2>
                                <p>Our animated live logo offers your business the ability to communicate. We provide the optimal push for your business's success through Animated Logo design. As moving images are one of the rising trends these days, we are here to transform your companies' static marks into engaging animated logos. Because a logo is the beating heart of every company, we guarantee that a vibrant connection is created to convey your brand's narrative!</p>
                            </div>
                            <div className="rightimgsec"><img src={logodesignanimated} className="righttext_img" alt="logodesign" /></div>
                        </div>

                    </div>
                </div>

                <div className="brand_section_background">
                <div className="brand_Section">
                    <h2>Get a company logo design that <br />reflects your brand's energy!</h2>
                    <p>Navicosoft is one of the main visual communication of your brand through CBA logo designs. Since 2008, we are slaying with a passion for creating stunning logo designs. As a logo maker online in Australia, our first & foremost duty is to get extreme elegance for your business with the stuff that shines!</p>
                    <div className="brand_vitality" style={{ backgroundImage: `url(${brandlogodesign})` }} alt="logodesign">
                        <div className="branding_text"><p>We understand that each symbol has a unique narrative to tell, which is why we place a premium on creating pleasant impressions with your prospects. Our logo designs are not exclusive to a certain kind of company; they vary according to the type of business. We can create a logo for any type of company, including the following:</p></div>

                    </div>
                </div>
                </div>


                <div className="branding_solution_logo_design"> 
                    <h2 className="sectthreee_heading mobile_stationary">Our Intelligent Approach to Your <br /><span className="slidertext_color">Branding Solutions with <br />Business Logo Design</span></h2>
                    <p className="para_background_linux">We strive to offer you a perfect logo design for your brand in a thorough and systematic approach. We aim for your complete happiness and will not stop working until you are delighted with the design. We provide your company with a fresh design viewpoint via the creation of intuitive but cheap logo designs. Navicosoft is dedicated to delivering amazing graphics for wonderful people like you!</p>
                    <div className="branding_steps">
                        <div className="left_branding">
                            <div><img src={search} className="righttext_img" alt="search" /></div>
                            <div className="text_logo_branding">
                                <h2>Search</h2>
                                <p>We have a team of expert logo designers that do in-depth research into industry standards and practices on our behalf. The concepts will be developed in the same manner; we guarantee it. Our thorough research routine leaves us with a fresh viewpoint on how to offer you the finest Business Logo Design possible.</p>
                            </div>
                        </div>
                        <div className="left_branding">
                            <div><img src={idealize} className="righttext_img" alt="idealize" /></div>
                            <div className="text_logo_branding">
                                <h2>Idealize</h2>
                                <p>After doing a thorough analysis of the needs and industry standards, we offer several concepts for the brand and business logo. These thoughts and ideas are given to you in order for you to choose one that best suits your tastes. The completed idea is then utilised in the creation process.</p>
                            </div>
                        </div>
                    </div>
                    <div className="branding_steps">
                        <div className="left_branding">
                            <div><img src={create} className="righttext_img" alt="create" /></div>
                            <div className="text_logo_branding">
                                <h2>Create</h2>
                                <p>This stage will include the creation of a draft of the final idea. It will assist you in gaining an understanding of the final product. It will also assist the client in determining whether or not the logo is appropriate for the situation. It will, however, be fully implemented if the client determines that modifications are necessary.</p>
                            </div>
                        </div>
                        <div className="left_branding">
                            <div><img src={launch} className="righttext_img" alt="launch" /></div>
                            <div className="text_logo_branding">
                                <h2>Launch</h2>
                                <p>Following acceptance of the final draft, we proceed to completion with all necessary changes. We guarantee that your Business Logo Design is entirely customised and results-oriented. We are committed to providing you satisfactory services and will continue to do so until you are completely pleased!</p>
                            </div>
                        </div>
                    </div>

                </div>

                <div className="branding_element_background">
                <div className="why_navico_branding_element"> 
                    <h2 className="sectthreee_heading mobile_stationary">Choose Navicosoft for your brand <br /><span className="slidertext_color">dominance with our Business  <br />Logo Design?</span></h2>
                    <p className="para_background_linux">As expert logo designers, we can create a UK-made logo that will convey your brand's inspiration and creativity to the rest of the globe while also representing your company's values. We make certain to use the most effective methods and approaches in order to offer you logos that are visually stunning!</p>
                    <div className="font_text"><i className="far fa-check-circle"></i><p>We provide completely original business logo designs because we love uniqueness.</p></div>
                    <div className="font_text"><i className="far fa-check-circle"></i><p>Our expert logo designers make you excel in competition with the creation of compelling logo designs.</p></div>
                    <div className="font_text"><i className="far fa-check-circle"></i><p>We guarantee that your logo design is entirely yours and that you retain full copyright.</p></div>
                    <div className="font_text"><i className="far fa-check-circle"></i><p>Navicosoft offers a variety of affordable graphic design solutions to meet your every requirement.</p></div>
                    <div className="font_text"><i className="far fa-check-circle"></i><p>Our designers are highly talented and trained to increase your brand appearance.</p></div>

                </div>
                </div>

            </div>
        );
    }
}

export default LogoDesign;
